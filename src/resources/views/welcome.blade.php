@extends('layouts.page')

@php
    /**
      * @var  Place[] $places
      */
        use App\Models\Place;
        $title = trans('seo.welcome.title');
        $description = trans('seo.welcome.description');
        $keywords = trans('seo.welcome.keywords');
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('main_class', 'home')

@section('content')
    <h1>Abau.ru - сайт бесплатных объявлений</h1>

    <p>Вы можете бесплатно подать объявление на нашем сайте, для начала использования выберите город:</p>

    <ul class="home__city-list">
        @foreach($places as $place)
            <li>
                <a href="{{route('ad.search', ['place' => $place->code])}}">{{ $place->name_nominative }}</a>
            </li>
        @endforeach
    </ul>
@endsection
