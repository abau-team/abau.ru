@if(isset($breadcrumbs))

        <ul class="breadcrumb__container container" itemscope="" itemtype="http://schema.org/BreadcrumbList">
            <li class="breadcrumb__link" itemprop="itemListElement" itemscope=""
                itemtype="http://schema.org/ListItem">
                <a href="{{route('home')}}" itemprop="item"><span itemprop="name">{{__('Home')}}</span></a>
                <meta itemprop="position" content="1">
            </li>
            @foreach($breadcrumbs as $key => $item)
                <li class="breadcrumb__link" itemprop="itemListElement" itemscope=""
                    itemtype="http://schema.org/ListItem">
                    <a href="{{!empty($item['url']) ? $item['url'] : URL::current()}}" itemprop="item">
                        <span itemprop="name">{{$item['text']}}</span>
                    </a>
                    <meta itemprop="position" content="{{$key + 2}}">
                </li>
            @endforeach
        </ul>
@endif