@extends('layouts.master')

@section('main')
    <div class="content page__content">
        <main class="main main_full container @yield('main_class')">
            @yield('content')
        </main>
    </div>
@endsection