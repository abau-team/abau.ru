@push('js')
    <script type="text/javascript">
        var reformalOptions = {
            project_id: 982839,
            show_tab: false,
            project_host: "abau.reformal.ru"
        };

        (function () {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.async = true;
            script.src = ('https:' === document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
            document.getElementsByTagName('head')[0].appendChild(script);
        })();
    </script>
    <noscript>
        <a href="http://reformal.ru">
            <img alt="Отзывы и предложения" src="http://media.reformal.ru/reformal.png"/>
        </a>
        <a href="http://abau.reformal.ru">
            Oтзывы и предложения для Abau.ru
        </a>
    </noscript>
@endpush

<footer class="footer page__footer">
    <ul class="menu footer__menu container">
        <li>
            <a href="{{ Url::to('/') }}">
                {{ __('Home') }}
            </a>
        </li>
        <li>
            <a href="{{ route('term') }}">
                {{ __('Rules') }}
            </a>
        </li>
        <li>
            <a href="http://abau.reformal.ru" onclick="Reformal.widgetOpen();return false;"
               onmouseover="Reformal.widgetPreload();">
                {{ __('Reviews and suggestions') }}
            </a>
        </li>
    </ul>
    <div class="copyright footer__copyright">
        © 2019<?= date('Y') != "2019" ? " - " . date('Y') : "" ?>&nbsp;<a href="{{Url::to('/')}}">Abau.ru</a>
    </div>
</footer>