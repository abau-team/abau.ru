@extends('layouts.master')

@section('main')
    <div class="content page__content page__content_center">
        <main class="main main_single">
            @yield('content')
        </main>
    </div>
@endsection