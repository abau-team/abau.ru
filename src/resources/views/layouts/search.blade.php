@extends('layouts.master')

@section('main')
    <div class="content page__content">
        <main class="main main_full ad-search container @yield('main_class')">
            <div class="ad-search__result">
                @yield('content')
            </div>
            <div class="ad-search__form">
                @yield('form')
            </div>
        </main>
    </div>
@endsection