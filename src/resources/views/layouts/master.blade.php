<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js">
@yield('beginHtml')
<head>
    @section('beginHead')
        @env('production')
        <!-- Google Tag Manager -->
        <script>
            (function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                let f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l !== 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-PT2KJJR');
        </script>
        <!-- End Google Tag Manager -->
        @endenv
    @show

    <meta charset="UTF-8">

    {{-- Просим IE переключиться в последний режим --}}
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    {{-- Убираем возможность масштабировать --}}
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Запрещаем распознавать номера телефонов и адреса --}}
    <meta name="format-detection" content="telephone=no"/>
    <meta name="format-detection" content="address=no"/>

    <!-- Set Page Informs Tags -->
    <title>@yield('title_prefix', '')@yield('title')@yield('title_postfix', ' - Abau.ru')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="copyright" content="Abau.ru">
    <link rel="canonical" href="{{URL::current()}}"/>
    <!-- End Set Page Informs Tags -->


    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Set Favicons Tags -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/icons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('/icons/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('/icons/safari-pinned-tab.svg')}}" color="#e00707">
    <link rel="shortcut icon" href="{{asset('/icons/favicon.ico')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="{{asset('/icons/browserconfig.xml')}}">
    <meta name="theme-color" content="#ffffff">
    <!-- End Set Favicons Tags -->

    @section('endHead')

        <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" async></script>
        <link rel="stylesheet" href="{{mix('css/main.css')}}">
        @stack('css')

    @show

</head>
<body>

<div id="abau" class="page" itemscope itemtype="http://schema.org/WebPage">
    <meta itemprop="mainEntityOfPage" content="{{Request::fullUrl()}}" property="mainEntityOfPage">

    @section('beginBody')
        @env('production')
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PT2KJJR"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->
        @endenv
    @show

    @include('layouts.header')

    @yield('main')

    @include('layouts.footer')

</div>

@section('endBody')

    <script src="{{mix('js/main.js')}}" async></script>

    @stack('js')
@show

</body>@yield('endHtml')

</html>@yield('endDocument')