@php
    $menu = [
        //['label' => __('Work'), 'url' => route('ad.search.work')],
        ['label' => __('Vacancies'), 'url' => route('ad.searchCategory', ['category' => 'vacancies'])],
        ['label' => __('Resumes'), 'url' => route('ad.searchCategory', ['category' => 'resumes'])],
    ];
@endphp

<header class="header page__header">
    <div class="line header__first-line">
        <ul class="menu header__nav-menu">
            @each('partials.menu-item', $menu, 'item')
        </ul>

        <ul class="menu profile-menu">
            @if(\Illuminate\Support\Facades\Auth::check())
                <li><a href="{{ route('ad.my') }}">{{__('My Ads')}}</a></li>

                <li>
                    <a href="{{ route('logout') }}">{{__('Logout (:email)', ['email' => isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email])}}</a>
                </li>
            @else
                <li><a href="{{route('login')}}"><img alt="Login" src="{{ asset('/images/person.svg') }}">
                        <span>{{__('Login and Register')}}</span></a></li>
            @endif
        </ul>
    </div>
    <div class="line header__second-line">
        <div class="container">
            <a class="burger" href="{{ route('ad.my') }}">
                <div class="icon icon-burger"></div>
            </a>

            <a class="logo" href="{{Url::to('/')}}">
                <img alt="Abau Logo" src="{{asset('/images/logo.svg')}}">
            </a>
            <a class="btn-add" href="{{ route('ad.create.form') }}">
                <span class="icon icon-add"></span>
                <span class="btn btn-green">{{__('Post Ad')}}</span>
            </a>
        </div>
    </div>
</header>
<section class="breadcrumb">
    @if(Request::path() != '/')
        @include('partials.breadcrumbs')
    @endif
</section>

@if ($message = Session::get('success'))

    <section class="alert alert-success container" role="alert">

        <strong>{{ $message }}</strong>

    </section>

@endif


@if ($message = Session::get('error'))

    <section class="alert alert-danger container" role="alert">

        <strong>{{ $message }}</strong>

    </section>

@endif


@if ($message = Session::get('warning'))

    <section class="alert alert-warning container" role="alert">

        <strong>{{ $message }}</strong>

    </section>

@endif


@if ($message = Session::get('info'))

    <section class="alert alert-info container" role="alert">

        <strong>{{ $message }}</strong>

    </section>

@endif

@if (session('status'))
    <div class="alert alert-success container" role="alert">
        {{ session('status') }}
    </div>
@endif
