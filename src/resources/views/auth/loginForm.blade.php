@extends('layouts.single-form')

@php
    $title = __('user.login-form.title');
    $description = __('user.login-form.description');
    $keywords = __('user.login-form.keywords');
    $breadcrumbs = [
        ['text' => __('user.login-form.breadcrumbs'), 'url' => route('loginForm')],
    ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <form class="single-form single-form__login form login-form" method="POST" action="{{ route('login') }}">
        @csrf

        <h1>{{__('user.login-form.header')}}</h1>

        <div class="form-group form-group__input @error('login') has-error @enderror">

            <label for="login">
                {{ __('user.login-form.attributes.login') }}
            </label>

            <input id="login" type="text" class="form-control" name="login" value="{{ old('login') }}"
                   placeholder="{{ __('user.login-form.attributes.login') }}"
                   required autocomplete="email" autofocus>

            @error('login')
            <span class="help-block">{{ $message }}</span>
            @enderror

        </div>

        <div class="form-group form-group__input @error('login-password') has-error @enderror">

            <label for="password">
                {{ __('user.login-form.attributes.password') }}
            </label>

            <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}"
                   placeholder="{{ __('user.login-form.attributes.password') }}"
                   required autocomplete="new-password">

            @error('password')
            <span class="help-block">{{ $message }}</span>
            @enderror

        </div>

        <div class="form-group form-group__checkbox @error('remember') has-error @enderror">

            <div class="checkbox">
                <input type="checkbox" id="remember"
                       name="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember">
                    {{ __('user.login-form.attributes.remember') }}
                </label>
            </div>

            @error('remember')
            <span class="help-block">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-group form-group__btn">
            <button type="submit" class="btn btn-green">
                {{ __('user.login-form.loginBtn') }}
            </button>
        </div>

        <div class="form-group form-link">
            <a href="{{route('registerForm')}}">{{ __('user.login-form.register') }}</a><br>
            <a href="{{ route('recoveryForm') }}">{{ __('user.login-form.reset') }}</a>
        </div>
    </form>

@endsection
