@extends('layouts.single-form')

@php
$title = __('user.recovery-form.title');
$description = __('user.recovery-form.description');
$keywords = __('user.recovery-form.keywords');
$breadcrumbs = [
['text' => __('user.recovery-form.breadcrumbs'), 'url' => route('recoveryForm')],
];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
<meta property="og:url" content="{{URL::current()}}">
<meta property="og:type" content="website">
<meta property="og:title" content="{{$title}}">
<meta property="twitter:title" content="{{$title}}">
<meta property="og:description" content="{{$description}}">
<meta property="twitter:description" content="{{$description}}">
<meta property="og:image" content="{{asset('images/og-image.jpg')}}">
@parent
@endsection

@section('content')
<form class="single-form single-form__recovery form recovery-form" method="POST" action="{{ route('recovery') }}">
    @csrf

    <h1>{{__('user.recovery-form.header')}}</h1>

    <div class="form-group form-group__input @error('login') has-error @enderror">

        <label for="login">
            {{ __('user.recovery-form.attributes.login') }}
        </label>

        <input id="login" type="text" class="form-control" name="login" value="{{ old('login') }}"
               placeholder="{{ __('user.recovery-form.attributes.login') }}"
               required autocomplete="email" autofocus>

        @error('login')
        <span class="help-block">{{ $message }}</span>
        @enderror

    </div>

    <div class="form-group form-group__btn">
        <button type="submit" class="btn btn-green">
            {{ __('user.recovery-form.recoveryBtn') }}
        </button>
    </div>

    <div class="form-group form-link">
        <a href="{{route('registerForm')}}">{{ __('user.recovery-form.register') }}</a><br>
        <a href="{{ route('loginForm') }}">{{ __('user.recovery-form.login') }}</a>
    </div>
</form>

@endsection
