@extends('layouts.single-form')

@php
/** @var string $key */
    $title = __('user.reset-form.title');
    $description = __('user.reset-form.description');
    $keywords = __('user.reset-form.keywords');
    $breadcrumbs = [
        ['text' => __('user.reset-form.breadcrumbs'), 'url' => route('resetForm', ['key' => $key])],
    ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <form class="single-form single-form__reset form reset-form" method="POST" action="{{ route('reset', ['key' => $key]) }}">
        @csrf

        <h1>{{ __('user.reset-form.header') }}</h1>


        <div class="form-group @error('password') has-error @enderror">

            <label for="password">
                {{ __('user.reset-form.attributes.password') }}
            </label>

            <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}"
                   placeholder="{{ __('user.reset-form.attributes.password') }}"
                   required autocomplete="new-password">

            @error('password')
            <span class="help-block">{{ $message }}</span>
            @enderror

        </div>

        <div class="form-group @error('password_confirmation') has-error @enderror">
            <label for="password_confirmation">
                {{ __('user.reset-form.attributes.password_confirmation') }}
            </label>

            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation"
                   value="{{ old('password_confirmation') }}"
                   placeholder="{{ __('user.reset-form.attributes.password_confirmation') }}"
                   required autocomplete="new-password">

            @error('password_confirmation')
            <span class="help-block">{{ $message }}</span>
            @enderror

        </div>


        <div class="form-group form-group__btn">
            <button type="submit" class="btn btn-green">
                {{ __('user.reset-form.resetBtn') }}
            </button>
        </div>

        <div class="form-group form-link">
            <a href="{{route('loginForm')}}">{{ __('user.registerForm.login') }}</a><br>
            <a href="{{ route('recoveryForm') }}">{{ __('user.registerForm.reset') }}</a>
        </div>
    </form>

@endsection
