@extends('layouts.single-form')

@php
    $title = __('seo.register.title');
    $description = __('seo.register.description');
    $keywords = __('seo.register.keywords');
    $breadcrumbs = [
        ['text' => __('seo.register.breadcrumbs'), 'url' => route('registerForm')],
    ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', 'Контакты, кварт-град')

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <form class="single-form single-form__register form register-form" method="POST" action="{{ route('register') }}">
        @csrf

        <h1>{{ __('user.registerForm.header') }}</h1>

        <div class="form-group form-group__input @error('name') has-error @enderror">

            <label for="name">
                {{ __('user.registerForm.attributes.name') }}
            </label>

            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"
                   placeholder="{{ __('user.registerForm.attributes.name') }}"
                   required autocomplete="name" autofocus>

            @error('name')
            <span class="help-block">{{ $message }}</span>
            @enderror

        </div>

        <div class="form-group form-group__input @error('email') has-error @enderror">

            <label for="email">
                {{ __('user.registerForm.attributes.email') }}
            </label>

            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"
                   placeholder="{{ __('user.registerForm.attributes.email') }}"
                   required autocomplete="email">

            @error('email')
            <span class="help-block">{{ $message }}</span>
            @enderror

        </div>

        <div class="form-group form-group__input @error('phone') has-error @enderror">

            <label for="phone">
                {{ __('user.registerForm.attributes.phone') }}
            </label>

            <input type="tel" class="form-control" id="phone" name="phone" value="{{ old('phone') }}"
                   placeholder="{{ __('user.registerForm.attributes.phone') }}"
                   required autocomplete="phone">

            @error('phone')
            <span class="help-block">{{ $message }}</span>
            @enderror

        </div>

        <div class="form-group @error('password') has-error @enderror">

            <label for="password">
                {{ __('user.registerForm.attributes.password') }}
            </label>

            <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}"
                   placeholder="{{ __('user.registerForm.attributes.password') }}"
                   required autocomplete="new-password">

            @error('password')
            <span class="help-block">{{ $message }}</span>
            @enderror

        </div>

        <div class="form-group @error('password_confirmation') has-error @enderror">
            <label for="password_confirmation">
                {{ __('user.registerForm.attributes.password_confirmation') }}
            </label>

            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation"
                   value="{{ old('password_confirmation') }}"
                   placeholder="{{ __('user.registerForm.attributes.password_confirmation') }}"
                   required autocomplete="new-password">

            @error('password_confirmation')
            <span class="help-block">{{ $message }}</span>
            @enderror

        </div>

        <div class="form-group form-group__checkbox @error('term-agree') has-error @enderror">

            <div class="checkbox">
                <input type="checkbox" id="term-agree"
                       name="term-agree" {{ old('term-agree') ? 'checked' : '' }}>
                <label for="term-agree">{{ __('user.registerForm.term-agree') }}</label>
            </div>

            @error('term-agree')
            <span class="help-block">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-group form-group__btn">
            <button type="submit" class="btn btn-green">
                {{ __('user.registerForm.registerBtn') }}
            </button>
        </div>

        <div class="form-group form-link">
            <a href="{{route('loginForm')}}">{{ __('user.registerForm.login') }}</a><br>
            <a href="{{ route('recoveryForm') }}">{{ __('user.registerForm.reset') }}</a>
        </div>
    </form>

@endsection
