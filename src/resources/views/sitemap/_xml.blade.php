@php
    /** @var array $pages */
@endphp

@foreach ($pages as $page)
    <url>
        <loc>{{ $page['loc'] }}</loc>
        @if($page['lastmod'])
            <lastmod>{{ $page['lastmod'] }}</lastmod>
        @endif
        @if($page['changefreq'])
            <changefreq>{{ $page['changefreq'] }}</changefreq>
        @endif
        @if($page['priority'])
            <priority>{{ $page['priority'] }}</priority>
        @endif
    </url>
    @if(isset($page['items']))
        @include('sitemap._xml', ['pages' => $page['items']])
    @endif
@endforeach