@php
    use App\Models\Contact;use App\Models\User;

    /** @var User $user */
    /** @var Contact $model */

    $title = __('contact.admin.updateForm', ['id' => $model->contact_id]);
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.contact._menu')

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
            </div>
            <form action="{{route('admin.contact.update', ['id' => $model->contact_id])}}" method="post">
                <div class="box-body">
                    @csrf
                    @method('PUT')

                    @include('admin.contact._form')

                </div>
                <div class="box-footer">
                    <button class="btn btn-primary">{{ __('admin.save') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop