@php
    /** @var \App\Models\Contact|null $model */
@endphp

<div class="form-group @error('name') has-error @enderror">
    <label for="name">{{ __('contact.field.name') }}</label>
    <input type="text" name="name" class="form-control" id="name"
           placeholder="{{ __('contact.field.name') }}" value="{{ $model->name ?? old('name') }}">
    @error('name')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group @error('type') has-error @enderror">
    <label for="type">{{ __('contact.field.type') }}</label>
    <select id="type" name="type" class="form-control">
        <option value=""></option>
        @foreach (\App\Models\Contact::getTypes() as $key => $value)
            <option {{old('type') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('type')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group @error('value') has-error @enderror">
    <label for="value">{{ __('contact.field.value') }}</label>
    <input type="text" name="value" class="form-control" id="value"
           placeholder="{{ __('contact.field.value') }}" value="{{ $model->value ?? old('value') }}">
    @error('value')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>