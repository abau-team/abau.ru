@php
    use App\Models\Contact;

    /** @var Contact[] $models */

    $title = __('contact.admin.listActive');
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.contact._menu')

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3>{{ $title }}</h3>
            </div>
            <div class="box-body">
                <div class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="col-sm-12">
                        @include('admin.contact._contactTable', ['contacts' => $models])
                    </div>
                </div>
            </div>
            <div class="box-footer">
                {{ $models->links() }}
            </div>
        </div>
    </div>
@stop