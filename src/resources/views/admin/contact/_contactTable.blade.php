@php
    use App\Models\Contact;

    /** @var Contact[] $contacts */
@endphp
<table class="table table-bordered table-hover dataTable table-align-middle" role="grid">
    <thead>
    <tr role="row">
        <th>{{ __('contact.field.contact_id') }}</th>
        <th>{{ __('contact.field.user_id') }}</th>
        <th>{{ __('contact.field.name') }}</th>
        <th>{{ __('contact.field.type') }}</th>
        <th>{{ __('contact.field.value') }}</th>
        <th>{{ __('contact.field.created_at') }}</th>
        <th>{{ __('contact.field.blocked_at') }}</th>
        <th>{{ __('contact.field.deleted_at') }}</th>
        <th>{{ __('contact.field.activated_at') }}</th>
        <th>{{ __('contact.field.tokens') }}</th>
        <th>{{ __('contact.field.ads') }}</th>
        <th>{{ __('admin.grid-btn.buttons') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($contacts as $contact)
        <tr role="row">
            <td>{{ $contact->contact_id }}</td>
            <td><a href="{{ route('admin.contact.listByUser', ['id' => $contact->user_id]) }}"
                   title="{{ __('contact.admin.showListByUser') }}">{{ $contact->user_id }}</a></td>
            <td>{{ $contact->name }}</td>
            <td>{{ $contact->typeLabel }}</td>
            <td>{{ $contact->value }}</td>
            <td>{{ $contact->created_at }}</td>
            <td>
                @if( $contact->isBanned )
                    {{ $contact->blocked_at }}
                    <br>
                    <a class="label label-success"
                       href="{{ route('admin.contact.unBan', ['id' => $contact->user_id]) }}">{{ __('admin.grid-btn.unBan') }}</a>
                @else
                    <a class="label label-danger"
                       href="{{ route('admin.contact.ban', ['id' => $contact->user_id]) }}">{{ __('admin.grid-btn.ban') }}</a>
                @endif
            </td>
            <td>
                @if( $contact->isDeleted )
                    {{ $contact->deleted_at }}
                    <br>
                    <a class="label label-success"
                       href="{{ route('admin.contact.unDelete', ['id' => $contact->user_id]) }}">{{ __('admin.grid-btn.unDelete') }}</a>
                @else
                    <a class="label label-danger"
                       href="{{ route('admin.contact.delete', ['id' => $contact->user_id]) }}">{{ __('admin.grid-btn.delete') }}</a>
                @endif
            </td>
            <td>
                @if( $contact->isActivated )
                    {{ $contact->activated_at }}
                    <br>
                    <a class="label label-success"
                       href="{{ route('admin.contact.deactivate', ['id' => $contact->user_id]) }}">{{ __('admin.grid-btn.deactivate') }}</a>
                @else
                    <a class="label label-danger"
                       href="{{ route('admin.contact.activate', ['id' => $contact->user_id]) }}">{{ __('admin.grid-btn.activate') }}</a>
                @endif
            </td>
            <td>
                <a href="{{ route('admin.token.listByContact', ['id' => $contact->contact_id]) }}"
                   title="{{ __('token.admin.showListByContact') }}">{{ count($contact->tokens) }}</a>
            </td>
            <td>
                <a href="{{ route('admin.ad.listAll', ['contact_id' => $contact->contact_id]) }}"
                   title="{{ __('ad.admin.showListByContact') }}">{{ count($contact->ads) }}</a>
            </td>
            <td>
                <a href="{{ route('admin.contact.destroy', ['id' => $contact->user_id]) }}"
                   title="{{ __('admin.grid-btn.delete') }}"><i class="fa fa-fw fa-trash"></i></a>
                <a href="{{ route('admin.contact.updateForm', ['id' => $contact->user_id]) }}"
                   title="{{ __('admin.grid-btn.update') }}"><i
                            class="fa fa-fw fa-edit"></i></a>
                <a href="{{ route('admin.contact.show', ['id' => $contact->user_id]) }}"
                   title="{{ __('admin.grid-btn.show') }}"><i class="fa fa-fw fa-eye"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>