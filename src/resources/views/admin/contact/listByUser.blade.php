@php
    use App\Models\Contact;use App\Models\User;

    /** @var Contact[] $models */
    /** @var User $user */

    $title = __('contact.admin.listByUser', ['id' => $user->user_id]);
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.contact._menu')

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3>{{ $title }}</h3>
                <p><a href="{{ route('admin.contact.createForm', ['id' => $user->user_id]) }}">{{ __('contact.admin.createForUserButton') }}</a></p>
            </div>
            <div class="box-body">
                <div class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="col-sm-12">
                        @include('admin.contact._contactTable', ['contacts' => $models])
                    </div>
                </div>
            </div>
            <div class="box-footer">
                {{ $models->links() }}
            </div>
        </div>
    </div>
@stop