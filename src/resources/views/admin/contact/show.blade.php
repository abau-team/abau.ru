@php
    use App\Models\Contact;

    /** @var Contact $model */

    $title = __('contact.admin.show', ['id' => $model->contact_id]);
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.contact._menu')

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
                <p>
                    <a href="{{ route( "admin.contact.updateForm", ['id' => $model->contact_id]) }}"
                       class="btn btn-primary btn-sm margin">{{ __('admin.update') }}</a>

                    <a href="{{ route( "admin.contact.destroy" , ['id' => $model->contact_id]) }}"
                       class="btn btn-danger btn-sm margin">{{ __('admin.delete') }}</a>

                </p>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-bordered table-hover dataTable" role="grid">
                    <thead>
                    <tr>
                        <th>{{ __("admin.label") }}</th>
                        <th>{{ __("admin.value") }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ __('contact.field.contact_id') }}</td>
                        <td>{{ $model->contact_id }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('contact.field.user_id') }}</td>
                        <td>{{ $model->user_id }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('contact.field.name') }}</td>
                        <td>{{ $model->name }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('contact.field.type') }}</td>
                        <td>{{ $model->type_label }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('contact.field.value') }}</td>
                        <td>{{ $model->value }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop