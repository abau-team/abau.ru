<ul class="nav nav-pills margin-bottom">
    <li {{ route::currentRouteName() == "admin.contact.listActive" ?  "class=active" : "" }}>
        <a href="{{ route("admin.contact.listActive") }}">{{__("admin.menu.listActive")}}</a>
    </li>
    <li {{ route::currentRouteName() == "admin.contact.listUnActivated" ?  "class=active" : "" }}>
        <a href="{{ route("admin.contact.listUnActivated") }}">{{__("admin.menu.listUnActivated")}}</a>
    </li>
    <li {{ route::currentRouteName() == "admin.contact.listBanned" ?  "class=active" : "" }}>
        <a href="{{ route("admin.contact.listBanned") }}">{{__("admin.menu.listBanned")}}</a>
    </li>
    <li {{ route::currentRouteName() == "admin.contact.listDeleted" ?  "class=active" : "" }}>
        <a href="{{ route("admin.contact.listDeleted") }}">{{__("admin.menu.listDeleted")}}</a>
    </li>
    <li {{ route::currentRouteName() == "admin.contact.listAll" ?  "class=active" : "" }}>
        <a href="{{ route("admin.contact.listAll") }}">{{__("admin.menu.listAll")}}</a>
    </li>
</ul>