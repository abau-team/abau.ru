@php
    /** @var \App\Models\AdCategory|null $model */
@endphp

@csrf

<div class="form-group @error('name') has-error @enderror">
    <label for="name">{{ __('ad-category.fields.name') }}</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('ad-category.fields.name') }}" value="{{ $model->name ?? old('name') }}">
    @error('name')
    <span class="help-block">{{ $message }}</span>
    @enderror

</div>

<div class="form-group @error('model') has-error @enderror">
    <label for="model">{{ __('ad-category.fields.model') }}</label>
    <input type="text" name="model" class="form-control" id="model" placeholder="{{ __('ad-category.fields.model') }}" value="{{ $model->model ?? old('model') }}">
    @error('model')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group @error('code') has-error @enderror">
    <label for="code">{{ __('ad-category.fields.code') }}</label>
    <input type="text" name="code" class="form-control" id="code" placeholder="{{ __('ad-category.fields.code') }}" value="{{ $model->code ?? old('code') }}">
    @error('code')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="checkbox @error('active') has-error @enderror">

    <label for="active">
        <input type="checkbox" name="active" id="active" {{ ($model->active ?? old('active')) ? 'checked' : '' }}>
        {{ __('ad-category.fields.active') }}
    </label>
    @error('active')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>
