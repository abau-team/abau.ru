@php
    use App\Models\JobIndustry;

    /** @var JobIndustry $model */
    /** @var string $entity */
    /** @var string $route */
    /** @var string $primaryKey */
@endphp
<tr>
    <td>{{ __('job-industry.fields.job_industry_id') }}</td>
    <td>{{ $model->job_industry_id }}</td>
</tr>
<tr>
    <td>{{ __('job-industry.fields.name') }}</td>
    <td>{{ $model->local_name }}</td>
</tr>
<tr>
    <td>{{ __('job-industry.fields.code') }}</td>
    <td>{{ $model->code }}</td>
</tr>