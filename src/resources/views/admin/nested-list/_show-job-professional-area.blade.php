@php
    use App\Models\JobProfessionalArea;

    /** @var JobProfessionalArea $model */
    /** @var string $entity */
    /** @var string $route */
    /** @var string $primaryKey */
@endphp
<tr>
    <td>{{ __('job-professional-area.fields.job_professional_area_id') }}</td>
    <td>{{ $model->job_professional_area_id }}</td>
</tr>
<tr>
    <td>{{ __('job-professional-area.fields.name') }}</td>
    <td>{{ $model->local_name }}</td>
</tr>
<tr>
    <td>{{ __('job-professional-area.fields.code') }}</td>
    <td>{{ $model->code }}</td>
</tr>