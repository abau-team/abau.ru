@php
    use App\Models\Place;

    /** @var Place[] $models */
    /** @var string $entity */
    /** @var string $route */
    /** @var string $primaryKey */
@endphp

@foreach ($models as $model)
    <li>
        <div class="tree__item {{ count($model->children) > 0 && $model->type >= \App\Models\Place::TYPE_REGION ? "tree__cursor" : "" }}">
            <div class="tree__label">
                <a href="{{ route("{$route}.show", ['id' => $model->{$primaryKey}]) }}">{{ $model->name}} ({{ $model->type_label }})</a>

                <a href="{{ route("{$route}.up", ['id' => $model->{$primaryKey}]) }}"
                   title="{{ __('admin.nested.move-up') }}"><i
                            class="fa fa-fw fa-arrow-up"></i></a>

                <a href="{{ route("{$route}.down", ['id' => $model->{$primaryKey}]) }}"
                   title="{{ __("admin.nested.move-down") }}"><i
                            class="fa fa-fw fa-arrow-down"></i></a>
            </div>
            <div class="tree__buttons">
                @if($model->active)
                    <a href="{{ route("{$route}.un-active", ['id' => $model->{$primaryKey}]) }}"
                       title="{{ __("admin.set-un-active") }}"><i class="fa fa-fw fa-ban"></i></a>
                @else
                    <a href="{{ route("{$route}.active", ['id' => $model->{$primaryKey}]) }}"
                       title="{{ __("admin.set-active") }}"><i class="fa fa-fw fa-check"></i></a>
                @endif
                <a href="{{ route("{$route}.move", ['id' => $model->{$primaryKey}]) }}"
                   title="{{ __("admin.nested.move") }}"><i
                            class="fa fa-fw fa-arrows"></i></a>

                <a href="{{ route("{$route}.make-root", ['id' => $model->{$primaryKey}]) }}"
                   title="{{ __("admin.nested.make-root") }}"><i
                            class="fa fa-fw fa-level-up"></i></a>

                <a href="{{ route("{$route}.updateForm", ['id' => $model->{$primaryKey}]) }}"
                   title="{{ __('admin.update') }}"><i class="fa fa-fw fa-edit"></i></a>
                <a href="{{ route("{$route}.destroy", ['id' => $model->{$primaryKey}]) }}"
                   title="{{ __('admin.delete') }}"><i
                            class="fa fa-fw fa-remove"></i></a>
            </div>
        </div>
        @if(count($model->children) > 0)
            <ul class="{{ $model->type >= \App\Models\Place::TYPE_REGION ? "tree__nested" : "" }}">
                @include("admin.nested-list._tree-{$entity}", [
                    'models' => $model->children,
                    'entity' => $entity,
                    'route' => $route,
                    'primaryKey' => $primaryKey
                ])
            </ul>
        @endif
    </li>
@endforeach

