@php
    use App\Models\AdCategory;

    /** @var AdCategory $model */
    /** @var string $entity */
    /** @var string $route */
    /** @var string $primaryKey */
@endphp
<tr>
    <td>{{ __('ad-category.fields.ad_category_id') }}</td>
    <td>{{ $model->ad_category_id }}</td>
</tr>
<tr>
    <td>{{ __('ad-category.fields.local_name') }}</td>
    <td>{{ $model->local_name }}</td>
</tr>
<tr>
    <td>{{ __('ad-category.fields.code') }}</td>
    <td>{{ $model->code }}</td>
</tr>
<tr>
    <td>{{ __('ad-category.fields.active') }}</td>
    <td>{{ $model->active }}</td>
</tr>