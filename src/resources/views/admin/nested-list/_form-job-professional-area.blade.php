@php
    /** @var \App\Models\JobProfessionalArea|null $model */
@endphp

@csrf

<div class="form-group @error('name') has-error @enderror">
    <label for="name">{{ __('job-professional-area.fields.name') }}</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('job-professional-area.fields.name') }}" value="{{ $model->name ?? old('name') }}">
    @error('name')
    <span class="help-block">{{ $message }}</span>
    @enderror

</div>

<div class="form-group @error('code') has-error @enderror">
    <label for="code">{{ __('job-professional-area.fields.code') }}</label>
    <input type="text" name="code" class="form-control" id="code" placeholder="{{ __('job-professional-area.fields.code') }}" value="{{ $model->code ?? old('code') }}">
    @error('code')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>
