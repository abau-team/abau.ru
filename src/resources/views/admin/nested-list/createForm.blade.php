@php
    /** @var string $entity */
    /** @var string $route */

    $title = __("{$entity}.admin.createForm");
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.nested-list._menu', [
        'entity' => $entity,
        'route' => $route
    ])

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
            </div>
            <form action="{{route("{$route}.create")}}" method="post">
                <div class="box-body">
                    @include("admin.nested-list._form-{$entity}", [
                        'entity' => $entity,
                        'route' => $route
                    ])
                </div>
                <div class="box-footer">
                    <button class="btn btn-primary">{{ __('admin.create') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop