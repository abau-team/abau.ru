@php
    use App\Models\JobIndustry;

    /** @var JobIndustry $model */
    /** @var string $entity */
    /** @var string $route */
    /** @var string $primaryKey */

    $title = __("{$entity}.admin.show", ['name' => $model->local_name])
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.nested-list._menu', [
        'entity' => $entity,
        'route' => $route
    ])

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
                <p>
                    <a href="{{ route( "{$route}.updateForm", ['id' => $model->{$primaryKey}]) }}"
                       class="btn btn-primary btn-sm margin">{{ __('admin.update') }}</a>

                    <a href="{{ route( "{$route}.destroy" , ['id' => $model->{$primaryKey}]) }}"
                       class="btn btn-danger btn-sm margin">{{ __('admin.delete') }}</a>

                </p>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-bordered table-hover dataTable" role="grid">
                    <thead>
                    <tr>
                        <th>{{ __("admin.label") }}</th>
                        <th>{{ __("admin.value") }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @include("admin.nested-list._show-{$entity}", [
                        'model' => $model,
                        'primaryKey' => $primaryKey,
                        'entity' => $entity,
                        'route' => $route
                    ])
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop