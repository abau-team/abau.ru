@php
    /** @var string $entity */
    /** @var string $route */

@endphp

<ul class="nav nav-pills margin-bottom">
    <li {{ route::currentRouteName() == "{$route}.index" ?  "class='active" : "" }}>
        <a href="{{ route("{$route}.listAll" )}}">{{__("admin.menu.listAll")}}</a>
    </li>
    <li {{ route::currentRouteName() == "{$route}.create" ?  "class='active" : "" }}>
        <a href="{{ route("{$route}.createForm" )}}">{{__("admin.create")}}</a>
    </li>
</ul>