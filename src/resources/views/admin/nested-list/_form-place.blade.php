@php
    /**
    * @var \App\Models\Place|null $model
    */
@endphp

@csrf

<div class="form-group @error('type') has-error @enderror">
    <label for="type">{{ __('place.fields.type') }}</label>
    <select id="type" name="type" class="form-control">
        @foreach (\App\Models\Place::getTypes() as $key => $value)
            <option {{ ($model->type ?? old('type')) == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('type')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group @error('capital') has-error @enderror">
    <label for="capital">{{ __('place.fields.capital') }}</label>
    <select id="capital" name="capital" class="form-control">
        @foreach (\App\Models\Place::getCapitals() as $key => $value)
            <option {{ ($model->capital ?? old('capital')) == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('capital')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group @error('name') has-error @enderror">
    <label for="name">{{ __('place.fields.name') }}</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('place.fields.name') }}" value="{{ $model->name ?? old('name') }}">
    @error('name')
    <span class="help-block">{{ $message }}</span>
    @enderror

</div>

<div class="form-group @error('code') has-error @enderror">
    <label for="code">{{ __('place.fields.code') }}</label>
    <input type="text" name="code" class="form-control" id="code" placeholder="{{ __('place.fields.code') }}" value="{{ $model->code ?? old('code') }}">
    @error('code')
    <span class="help-block">{{ $message }}</span>
    @enderror

</div>

<div class="checkbox @error('active') has-error @enderror">

    <label for="active">
        <input type="checkbox" name="active" id="active" {{ ($model->active ?? old('active')) ? 'checked' : '' }}>
        Активно
    </label>
    @error('active')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>
