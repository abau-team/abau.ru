@php
    use App\Models\Place;

    /** @var Place $model */
    /** @var string $entity */
    /** @var string $route */
    /** @var string $primaryKey */
@endphp
<tr>
    <td>{{ __('place.fields.place_id') }}</td>
    <td>{{ $model->place_id }}</td>
</tr>
<tr>
    <td>{{ __('place.fields.name') }}</td>
    <td>{{ $model->local_name }}</td>
</tr>
<tr>
    <td>{{ __('place.fields.type') }}</td>
    <td>{{ $model->type_label }}</td>
</tr>
<tr>
    <td>{{ __('place.fields.capital') }}</td>
    <td>{{ $model->capital_label }}</td>
</tr>
<tr>
    <td>{{ __('place.fields.code') }}</td>
    <td>{{ $model->code }}</td>
</tr>
<tr>
    <td>{{ __('place.fields.active') }}</td>
    <td>{{ $model->active }}</td>
</tr>