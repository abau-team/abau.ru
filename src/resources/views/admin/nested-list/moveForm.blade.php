@php
    use App\Models\JobIndustry;

    /** @var JobIndustry $model */
    /** @var array $data */
    /** @var string $entity */
    /** @var string $route */
    /** @var string $primaryKey */

    $title = __("{$entity}.admin.moveForm", ['name' => $model->local_name]);
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.nested-list._menu', [
        'entity' => $entity,
        'route' => $route
    ])

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
            </div>
            <form action="{{route("{$route}.move", ['id' => $model->{$primaryKey}])}}" method="post">
                @csrf
                <div class="box-body">

                    <div class="form-group @error('parent_id') has-error @enderror">
                        <label for="parent_id">{{ __('admin.nested.parent') }}</label>
                        <select id="parent_id" name="parent_id" class="form-control">
                            @foreach ($data as $key => $value)
                                <option {{old('parent_id') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('parent_id')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="box-footer">
                    <button class="btn btn-primary">{{__('admin.nested.move', ['name' => $model->local_name])}}</button>
                </div>
            </form>
        </div>
    </div>
@stop