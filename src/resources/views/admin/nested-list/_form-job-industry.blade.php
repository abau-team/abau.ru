@php
    /** @var \App\Models\JobIndustry|null $model */
@endphp

@csrf

<div class="form-group @error('name') has-error @enderror">
    <label for="name">{{ __('job-industry.fields.name') }}</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('job-industry.fields.name') }}" value="{{ $model->name ?? old('name') }}">
    @error('name')
    <span class="help-block">{{ $message }}</span>
    @enderror

</div>

<div class="form-group @error('code') has-error @enderror">
    <label for="code">{{ __('job-industry.fields.code') }}</label>
    <input type="text" name="code" class="form-control" id="code" placeholder="{{ __('job-industry.fields.code') }}" value="{{ $model->code ?? old('code') }}">
    @error('code')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>
