@php
    use App\Models\JobIndustry;

    /** @var JobIndustry[] $models */
    /** @var string $entity */
    /** @var string $route */
    /** @var string $primaryKey */

    $title = __("{$entity}.admin.listAll")
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.nested-list._menu', [
        'entity' => $entity,
        'route' => $route
    ])

    <div class="tree tree__place">
        <ul>
            @include("admin.nested-list._tree-{$entity}", [
                'models' => $models,
                'entity' => $entity,
                'route' => $route,
                'primaryKey' => $primaryKey
                ])
        </ul>
    </div>
@stop


@section('adminlte_js')
@parent
    <script type="text/javascript">
        var toggler = document.getElementsByClassName("tree__cursor");
        var i;

        for (i = 0; i < toggler.length; i++) {
            toggler[i].addEventListener("click", function() {
                this.parentElement.querySelector(".tree__nested").classList.toggle("tree__nested_active");
                this.classList.toggle("tree__cursor_down");
            });
        }
    </script>
@stop
