@php
    use App\Models\User;

    /** @var User|null $model */
@endphp



<div class="form-group @error('password') has-error @enderror">
    <label for="password">{{ __('user.field.password') }}</label>
    <input type="password" name="password" class="form-control" id="password"
           placeholder="{{ __('user.field.password') }}"
           value="{{ old('password') }}">
    @error('password')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group @error('password_confirmation') has-error @enderror">
    <label for="password_confirmation">{{ __('user.field.password_confirmation') }}</label>
    <input type="password" name="password_confirmation" class="form-control"
           id="password_confirmation" placeholder="{{ __('user.field.password_confirmation') }}"
           value="{{ old('password_confirmation') }}">
    @error('password_confirmation')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>