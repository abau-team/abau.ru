@php
    use App\Models\User;

    /** @var User $model */

    $title = __('user.admin.show', ['id' => $model->user_id]);
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.user._menu')

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
                <p>
                    <a href="{{ route( "admin.user.updateForm", ['id' => $model->user_id]) }}"
                       class="btn btn-primary btn-sm margin">{{ __('admin.update') }}</a>

                    <a href="{{ route( "admin.user.destroy" , ['id' => $model->user_id]) }}"
                       class="btn btn-danger btn-sm margin">{{ __('admin.delete') }}</a>

                </p>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-bordered table-hover dataTable" role="grid">
                    <thead>
                    <tr>
                        <th>{{ __("admin.label") }}</th>
                        <th>{{ __("admin.value") }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ __('user.field.user_id') }}</td>
                        <td>{{ $model->user_id }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('user.field.name') }}</td>
                        <td>{{ $model->name }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('user.field.place_id') }}</td>
                        <td>{{ $model->place ? $model->place->local_name : "" }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop