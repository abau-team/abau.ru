@php
    use App\Models\User;

    /** @var User[] $users */
@endphp
<table class="table table-bordered table-hover dataTable table-align-middle" role="grid">
    <thead>
    <tr role="row">
        <th>{{ __('user.field.user_id') }}</th>
        <th>{{ __('user.field.name') }}</th>
        <th>{{ __('user.field.created_at') }}</th>
        <th>{{ __('user.field.updated_at') }}</th>
        <th>{{ __('user.field.last_active_at') }}</th>
        <th>{{ __('user.field.place_id') }}</th>
        <th>{{ __('user.field.ip') }}</th>
        <th>{{ __('user.field.blocked_at') }}</th>
        <th>{{ __('user.field.deleted_at') }}</th>
        <th>{{ __('user.field.contacts') }}</th>
        <th>{{ __('user.field.tokens') }}</th>
        <th>{{ __('user.field.ads') }}</th>
        <th>{{ __('admin.grid-btn.buttons') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr role="row">
            <td>{{ $user->user_id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->created_at }}</td>
            <td>{{ $user->updated_at }}</td>
            <td>{{ $user->last_active_at }}</td>

            <td>{{ $user->place ? $user->place->local_name : "" }}</td>
            <td>{{ $user->ipString }}</td>
            <td>
                @if( $user->isBanned )
                    {{ $user->blocked_at }}
                    <br>
                    <a class="label label-success"
                       href="{{ route('admin.user.unBan', ['id' => $user->user_id]) }}">{{ __('admin.grid-btn.unBan') }}</a>
                @else
                    <a class="label label-danger"
                       href="{{ route('admin.user.ban', ['id' => $user->user_id]) }}">{{ __('admin.grid-btn.ban') }}</a>
                @endif
            </td>
            <td>
                @if( $user->isDeleted )
                    {{ $user->deleted_at }}
                    <br>
                    <a class="label label-success"
                       href="{{ route('admin.user.unDelete', ['id' => $user->user_id]) }}">{{ __('admin.grid-btn.unDelete') }}</a>
                @else
                    <a class="label label-danger"
                       href="{{ route('admin.user.delete', ['id' => $user->user_id]) }}">{{ __('admin.grid-btn.delete') }}</a>
                @endif
            </td>
            <td>
                <a href="{{ route('admin.contact.listByUser', ['id' => $user->user_id]) }}"
                   title="{{ __('contact.admin.showListByUser') }}">{{ count($user->contacts) }}</a>
            </td>
            <td>
                <a href="{{ route('admin.token.listByUser', ['id' => $user->user_id]) }}"
                   title="{{ __('token.admin.showListByUser') }}">{{ count($user->tokens) }}</a>
            </td>
            <td>
                <a href="{{ route('admin.ad.listAll', ['user_id' => $user->user_id]) }}"
                   title="{{ __('ad.admin.showListByUser') }}">{{ count($user->ads) }}</a>
            </td>
            <td>
                <a href="{{ route('admin.user.destroy', ['id' => $user->user_id]) }}" title="{{ __('admin.grid-btn.delete') }}"><i class="fa fa-fw fa-trash"></i></a>
                <a href="{{ route('admin.user.updateForm', ['id' => $user->user_id]) }}" title="{{ __('admin.grid-btn.update') }}"><i
                            class="fa fa-fw fa-edit"></i></a>
                <a href="{{ route('admin.user.show', ['id' => $user->user_id]) }}" title="{{ __('admin.grid-btn.show') }}"><i class="fa fa-fw fa-eye"></i></a>
                <a href="{{ route('admin.user.passwordForm', ['id' => $user->user_id]) }}"  title="{{ __('admin.grid-btn.setPassword') }}"><i
                            class="fa fa-fw fa-key"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>