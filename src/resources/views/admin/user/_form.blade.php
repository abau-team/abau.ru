@php
    use App\Models\User;

    /** @var User|null $model */
@endphp



<div class="form-group @error('name') has-error @enderror">
    <label for="name">{{ __('user.field.name') }}</label>
    <input type="text" name="name" class="form-control" id="name"
           placeholder="{{ __('user.field.name') }}" value="{{ $model->name ?? old('name') }}">
    @error('name')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group @error('place_id') has-error @enderror">
    <label for="place_id">{{ __('user.field.place_id') }}</label>
    <select id="place_id" name="place_id" class="form-control">
        <option value=""></option>
        @foreach (\App\Models\Place::dataForDropdown() as $key => $value)
            <option {{old('place_id') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('place_id')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>