@php
    $title = __('user.admin.createForm');
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.user._menu')

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
            </div>
            <form action="{{route("admin.user.create")}}" method="post">
                <div class="box-body">
                    @csrf

                    @include('admin.user._form')

                    @include('admin.user._formPassword')

                </div>
                <div class="box-footer">
                    <button class="btn btn-primary">{{ __('admin.create') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop