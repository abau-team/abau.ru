@php
    use App\Models\User;

    /** @var User $model */

    $title = __('user.admin.updateForm', ['id' => $model->user_id])
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.user._menu')

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
            </div>
            <form action="{{route("admin.user.update", ['id' => $model->user_id])}}" method="post">
                <div class="box-body">
                    @csrf
                    @method('PUT')

                    @include('admin.user._form', ['model' => $model])
                </div>
                <div class="box-footer">
                    <button class="btn btn-primary">{{ __('admin.save') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop