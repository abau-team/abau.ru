<ul class="nav nav-pills margin-bottom">
    <li {{ route::currentRouteName() == "admin.user.listActive" ?  "class=active" : "" }}>
        <a href="{{ route("admin.user.listActive") }}">{{__("admin.menu.listActive")}}</a>
    </li>
    <li {{ route::currentRouteName() == "admin.user.listBanned" ?  "class=active" : "" }}>
        <a href="{{ route("admin.user.listBanned") }}">{{__("admin.menu.listBanned")}}</a>
    </li>
    <li {{ route::currentRouteName() == "admin.user.listDeleted" ?  "class=active" : "" }}>
        <a href="{{ route("admin.user.listDeleted") }}">{{__("admin.menu.listDeleted")}}</a>
    </li>
    <li {{ route::currentRouteName() == "admin.user.listAll" ?  "class=active" : "" }}>
        <a href="{{ route("admin.user.listAll") }}">{{__("admin.menu.listAll")}}</a>
    </li>
    <li {{ route::currentRouteName() == "admin.user.createForm" ?  "class=active" : "" }}>
        <a href="{{ route("admin.user.createForm") }}">{{__("admin.create")}}</a>
    </li>
</ul>