@extends('adminlte::page')

@section('title', __('admin.dashboard'))

@section('content_header')
    <h1>{{__('admin.dashboard')}}</h1>
@stop

@section('content')
    <p>Welcome to this beautiful admin panel.</p>
@stop