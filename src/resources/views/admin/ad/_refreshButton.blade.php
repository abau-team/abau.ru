@php
    /** @var \App\Models\Ad $ad */
@endphp
{{ $ad->refreshed_at }}
<br>
<a class="label label-success"
   href="{{ route('admin.ad.refresh', ['id' => $ad->ad_id]) }}">{{ __('admin.grid-btn.refresh') }}</a>
