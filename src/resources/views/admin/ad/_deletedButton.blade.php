@php
    /** @var \App\Models\Ad $ad */
@endphp
@if( $ad->isDeleted )
    {{ $ad->deleted_at }}
    <br>
    <a class="label label-success"
       href="{{ route('admin.ad.unDelete', ['id' => $ad->ad_id]) }}">{{ __('admin.grid-btn.unDelete') }}</a>
@else
    <a class="label label-danger"
       href="{{ route('admin.ad.delete', ['id' => $ad->ad_id]) }}">{{ __('admin.grid-btn.delete') }}</a>
@endif