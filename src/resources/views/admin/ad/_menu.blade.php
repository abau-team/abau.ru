@php
    /** @var string $status */
    $status = request()->get('status');
@endphp
<ul class="nav nav-pills margin-bottom">
    <li {{ $status == null ?  'class=active' : '' }}>
        <a href="{{ request()->fullUrlWithQuery(['status' => null]) }}">{{__('admin.menu.listActive')}}</a>
    </li>
    <li {{ $status == 'banned' ?  'class=active' : '' }}>
        <a href="{{ request()->fullUrlWithQuery(['status' => 'banned']) }}">{{__('admin.menu.listBanned')}}</a>
    </li>
    <li {{ $status == 'deleted' ?  'class=active' : '' }}>
        <a href="{{ request()->fullUrlWithQuery(['status' => 'deleted']) }}">{{__('admin.menu.listDeleted')}}</a>
    </li>
    <li {{ $status == 'all' ?  'class=active' : '' }}>
        <a href="{{ request()->fullUrlWithQuery(['status' => 'all']) }}">{{__('admin.menu.listAll')}}</a>
    </li>
    <li>
        <a href="{{ route('admin.ad.listAll') }}">{{__('admin.menu.clear-filters')}}</a>
    </li>
    <li>
        <a href="{{ route('admin.ad.createForm') }}">{{ __('ad.admin.createForm') }}</a>
    </li>
</ul>