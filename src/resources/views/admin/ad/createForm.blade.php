@php
    $title = __('ad.admin.createForm');
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.ad._menu')

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
            </div>
            <div class="box-body">
                <ad-form></ad-form>
                <li><a href="{{ route('admin.ad.createFormVacancy') }}">{{ __('ad.admin.createFormVacancy') }}</a></li>
                <li><a href="{{ route('admin.ad.createFormResume') }}">{{ __('ad.admin.createFormResume') }}</a></li>
            </div>
        </div>
    </div>
@stop