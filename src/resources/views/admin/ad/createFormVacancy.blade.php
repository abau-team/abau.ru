@php
    $title = __('contact.admin.createFormVacancy');
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.contact._menu')

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
            </div>
            <form action="{{route('admin.ad.create')}}" method="post">
                <div class="box-body">
                    @csrf

                    @include('admin.contact._form', ['category' => 'vacancy'])

                </div>
                <div class="box-footer">
                    <button class="btn btn-primary">{{ __('admin.create') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop