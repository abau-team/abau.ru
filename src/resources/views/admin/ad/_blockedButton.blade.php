@php
    /** @var \App\Models\Ad $ad */
@endphp
@if( $ad->isBanned )
    {{ $ad->blocked_at }}
    <br>
    <a class="label label-success"
       href="{{ route('admin.ad.unBan', ['id' => $ad->ad_id]) }}">{{ __('admin.grid-btn.unBan') }}</a>
@else
    <a class="label label-danger"
       href="{{ route('admin.ad.ban', ['id' => $ad->ad_id]) }}">{{ __('admin.grid-btn.ban') }}</a>
@endif