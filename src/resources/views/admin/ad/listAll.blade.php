@php
    /** @var string $title */
    $title = __('ad.admin.list');
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    @include('admin.ad._menu')

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3>{{ $title }}</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div class="col-sm-12">
                    <table class="table table-bordered table-hover dataTable table-align-middle" role="grid"
                           id="ad-table">
                        <thead>
                        <tr role="row">
                            <th>{{ __('ad.field.ad_id') }}</th>
                            <th>{{ __('ad.field.user_id') }}</th>
                            <th>{{ __('ad.field.phone') }}</th>
                            <th>{{ __('ad.field.view_contact') }}</th>
                            <th>{{ __('ad.field.category') }}</th>
                            <th>{{ __('ad.field.place') }}</th>
                            <th>{{ __('ad.field.created_at') }}</th>
                            <th>{{ __('ad.field.refreshed_at') }}</th>
                            <th>{{ __('ad.field.blocked_at') }}</th>
                            <th>{{ __('ad.field.deleted_at') }}</th>
                            <th>{{ __('admin.grid-btn.buttons') }}</th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </div>
@stop
@section('adminlte_js')
    @parent

    <script type="text/javascript">
        $(document).ready(function () {
            $('#ad-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "",
                columns: [
                    {data: 'ad_id', name: 'ads.ad_id'},
                    {data: 'user_id', name: 'ads.user_id'},
                    {data: 'phone', name: 'ads.phone', orderable: false},
                    {data: 'view_contact', name: 'ads.view_contact', searchable: false},
                    {data: 'ad_category_id', name: 'ads.ad_category_id', searchable: false},
                    {data: 'place', name: 'place.name', searchable: false},
                    {data: 'created_at', name: 'ads.created_at', searchable: false},
                    {data: 'refreshed_at', name: 'ads.refreshed_at', searchable: false},
                    {data: 'blocked_at', name: 'ads.blocked_at', searchable: false},
                    {data: 'deleted_at', name: 'ads.deleted_at', searchable: false},
                    {data: 'buttons', 'name': 'buttons', orderable: false, searchable: false}
                ],
                order: [[0, 'desc']],
                language: {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Russian.json"
                },
                lengthMenu: [[50, 100, 500, -1], [50, 100, 500, "All"]]
            });
        });
    </script>
@stop