@php
    /** @var \App\Models\Ad $ad */
@endphp
<a href="{{route('admin.ad.updateForm', ['id' => $ad->ad_id])}}"
   title="{{ __('admin.update') }}"><i class="fa fa-fw fa-edit"></i></a>
<a href="{{route('admin.ad.destroy', ['id' => $ad->ad_id])}}"
   title="{{ __('admin.grid-btn.delete') }}"><i class="fa fa-fw fa-trash"></i></a>
<a href="{{ route('admin.ad.show', ['id' => $ad->ad_id]) }}"
   title="{{ __('admin.grid-btn.show') }}"><i class="fa fa-fw fa-eye"></i></a>