@php
    use App\Models\Token;

    /** @var Token[] $tokens */
@endphp
<table class="table table-bordered table-hover dataTable table-align-middle" role="grid">
    <thead>
    <tr role="row">
        <th>{{ __('token.field.contact_id') }}</th>
        <th>{{ __('token.field.user_id') }}</th>
        <th>{{ __('token.field.type_label') }}</th>
        <th>{{ __('token.field.value') }}</th>
        <th>{{ __('token.field.created_at') }}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($tokens as $token)
        <tr role="row">
            <td>
                <a href="{{ route('admin.token.listByContact', ['id' => $token->contact_id]) }}"
                   title="{{ __('token.admin.showListByContact') }}">{{ $token->contact_id }}</a>
            </td>
            <td>
                <a href="{{ route('admin.token.listByUser', ['id' => $token->contact->user_id]) }}"
                   title="{{ __('token.admin.showListByUser') }}">{{ $token->contact->user_id }}</a>
            </td>
            <td>{{ $token->type_label }}</td>
            <td>{{ $token->value }}</td>
            <td>{{ $token->created_at }}</td>
            <td>
                <a href="{{ route('admin.token.destroy', ['type' => $token->type, 'value' => $token->value]) }}" title="{{ __('admin.grid-btn.delete') }}"><i
                            class="fa fa-fw fa-trash"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>