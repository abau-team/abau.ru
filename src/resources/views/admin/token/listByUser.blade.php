@php
    use App\Models\Token;
    use App\Models\User;

    /** @var Token[] $models */
    /** @var User $user */

    $title = __('token.admin.listByUser', ['id' => $user->user_id]);
@endphp

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3>{{ $title }}</h3>
            </div>
            <div class="box-body">
                <div class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="col-sm-12">
                        @include('admin.token._tokenTable', ['tokens' => $models])
                    </div>
                </div>
            </div>
            <div class="box-footer">
                {{ $models->links() }}
            </div>
        </div>
    </div>
@stop