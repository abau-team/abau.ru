@php
    /** @var $exception Exception */
@endphp

@extends('errors::minimal')

@section('title', __('error.500.title'))
@section('code', __('error.500.header'))
@section('message', __('error.500.message'))
