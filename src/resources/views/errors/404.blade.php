@php
    /** @var $exception Exception */
@endphp

@extends('errors::minimal')

@section('title', __('error.404.title'))
@section('code', __('error.404.header'))
@section('message', __('error.404.message'))
