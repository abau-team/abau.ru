@php
    /** @var $exception Exception */
@endphp

@extends('errors::minimal')

@section('title', __('error.401.title'))
@section('code', __('error.401.header'))
@section('message', __('error.401.message'))
