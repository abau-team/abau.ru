@extends('layouts.page')

@section('content')
    <h1>@yield('code')</h1>

    <p>@yield('message')</p>
@endsection