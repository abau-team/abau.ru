@php
    /** @var $exception Exception */
@endphp

@extends('errors::minimal')

@section('title', __('error.419.title'))
@section('code', __('error.419.header'))
@section('message', __('error.419.message'))
