@php
    /** @var $exception Exception */
@endphp

@extends('errors::minimal')

@section('title', __('error.503.title'))
@section('code', __('error.503.header'))
@section('message', $exception->getMessage() ?? __('error.503.message'))
