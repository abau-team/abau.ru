@php
    /** @var $exception Exception */
@endphp

@extends('errors::minimal')

@section('title', __('error.403.title'))
@section('code', __('error.403.header'))
@section('message', $exception->getMessage() ?? __('error.403.message'))
