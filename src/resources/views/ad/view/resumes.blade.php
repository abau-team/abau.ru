@extends('layouts.page')

@php
    /**
      * @var Ad $model
      */
        use App\Models\Ad;
        $title = trans('ad.resume-view.seo.title', ['resume_name' => $model->title, 'place_name' => $model->place->name_prepositional]);
        $description = trans('ad.resume-view.seo.description' );
        $keywords = '';
        $breadcrumbs = [
            ['text' => $model->place->name_nominative, 'url' => route('ad.search', ['place' => $model->place->code])],
            ['text' => $model->category->local_name, 'url' => route('ad.searchCategory', ['place' => $model->place->code, 'category' => $model->category->code])],
            ['text' => $model->adResume->first_name . " - " . $model->adResume->last_name, 'url' => route('ad.view.full', ['place' => $model->place->code, 'category' => $model->category->code, 'id' => $model->ad_id])],
        ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <div class="ad-view">
        <div class="ad-view__header">
            <div>
                <h1>{{ $model->title }}</h1>
                <div class="line">
                    {{ trans('ad.resume-view.ad_id', ['ad_id' => $model->ad_id]) }},
                    {{ trans('ad.resume-view.views', ['views' => $model->views]) }},
                    {{ trans('ad.resume-view.created_at', ['time' => date('d.m.Y H:m', strtotime($model->created_at))])  }}
                </div>
            </div>
            <div class="price">
                {{ $model->price }}
            </div>
        </div>

        @can('update', $model)
            <div class="ad__control">
                <a href="{{ route('ad.updateForm', ['id' => $model->ad_id]) }}">{{ trans('ad.ad-search.edit') }}</a>
                <a href="{{ route('ad.delete', ['id' => $model->ad_id]) }}">{{ trans('ad.ad-search.delete') }}</a>

                @can('admin', $model)
                    <a href="{{ route('ad.block', ['id' => $model->ad_id]) }}">{{ trans('ad.ad-search.block') }}</a>
                @endcan
            </div>
        @endcan

        <div class="ad-view__body">
            <div class="ad-view__column">

                <h2>{{ trans('ad.resume-view.add_info') }}</h2>

                <div class="ad-view__add-info">
                    <div class="values">

                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume.name') }}</span> {{ "{$model->adResume->last_name} {$model->adResume->first_name} {$model->adResume->patronymic_name}" }}
                        </div>

                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume.sex') }}</span> {{ $model->adResume->sex_label }}
                        </div>

                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume.age') }}</span> {{ trans_choice('ad.resume.age-value', $model->adResume->age, ['age' => $model->adResume->age]) }}
                        </div>

                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume.professional_area') }}</span> {{ $model->adResume->jobProfessionalArea }}
                        </div>

                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume.employment') }}</span> {{ $model->adResume->employment_label }}
                        </div>
                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume.schedule') }}</span> {{ $model->adResume->schedule_label }} {{ $model->adResume->schedule_comment ? "({$model->adResume->schedule_comment})" : "" }}
                        </div>
                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume.experience') }}</span> {{ $model->adResume->experience_label }}
                        </div>
                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume.education') }}</span> {{ $model->adResume->education_label  }}
                        </div>
                        @if($model->adResume->driveLicenses->count())
                            <div class="values__item">
                                <span class="label">{{ trans('ad.resume.driveLicenses') }}</span> {{ $model->adResume->driveLicense }}
                            </div>
                        @endif

                        @if($model->adResume->jobSkills->count())
                            <div class="values__item">
                                <span class="label">{{ trans('ad.resume.jobSkills') }}</span> {{ $model->adResume->jobSkill }}
                            </div>
                        @endif
                    </div>
                </div>

                <h2>{{ trans('ad.resume-view.description') }}</h2>
                <div class="ad-view__text">
                    {{ $model->adResume->ad->description }}
                </div>

                @if(count($model->adResume->jobExperiences) > 0)
                    <h2>{{ trans('ad.resume-view.experience') }}</h2>
                    <div class="ad-view__experiences">
                        @foreach($model->adResume->jobExperiences as $key => $experience)
                            <h3>{{ $key + 1 }}) {{ $experience->organization }}</h3>
                            <div class="values__item">
                                <span class="label">{{ trans('job-experience.interval') }}</span> {{ $experience->interval }}
                            </div>
                            <div class="values__item">
                                <span class="label">{{ trans('job-experience.job_industry') }}</span> {{ $experience->jobIndustry->local_name }}
                            </div>
                            <div class="values__item">
                                <span class="label">{{ trans('job-experience.place') }}</span> {{ $experience->place->name_nominative }}
                            </div>
                            @if($experience->site)
                                <div class="values__item">
                                    <span class="label">{{ trans('job-experience.site') }}</span> <a
                                            href="{{ $experience->site }}">{{ $experience->site }}</a>
                                </div>
                            @endif
                            <div class="values__item">
                                <span class="label">{{ trans('job-experience.position') }}</span> {{ $experience->position }}
                            </div>
                            <div class="values__item">
                                <span class="label">{{ trans('job-experience.duties') }}</span> {{ $experience->duties }}
                            </div>
                        @endforeach
                    </div>
                @endif

                @if(count($model->adResume->jobEducations) > 0)
                    <h2>{{ trans('ad.resume-view.education') }}</h2>
                    <div class="ad-view__educations">
                        @foreach($model->adResume->jobEducations as $key => $education)
                            <h3>{{ $key + 1 }}) {{ $education->name }}</h3>
                            <div class="values__item">
                                <span class="label">{{ trans('job-education.type')  }}</span> {{ $education->type_label  }}
                            </div>
                            <div class="values__item">
                                <span class="label">{{ trans('job-education.year') }}</span> {{ $education->year  }}
                            </div>
                            @if($education->department)
                                <div class="values__item">
                                    <span class="label">{{ trans('job-education.department') }}</span> {{ $education->department  }}
                                </div>
                            @endif
                            <div class="values__item">
                                <span class="label">{{ trans('job-education.specialization') }}</span> {{ $education->specialization  }}
                            </div>
                        @endforeach
                    </div>
                @endif

                @if($model->photos->count())
                    <div class="ad-view__photos">
                        @foreach($model->photos as $key => $photo)
                            <a href="{{ $photo->path }}">
                                <img src="{{ $photo->path }}" alt="{{ $title . '_' . $key }}"/>
                            </a>
                        @endforeach
                    </div>
                @endif


            </div>
            <div class="ad-view__column">
                <h2>{{ trans('ad.resume-view.contacts') }}</h2>
                <div class="ad-view__contact">

                    @if($model->email)
                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume-view.email') }}</span> {{ $model->email }}
                        </div>
                    @endif

                    @if($model->phone)
                        <div class="values__item">
                            <span class="label">{{ trans('ad.resume-view.phone') }}</span> {{ $model->phone }}
                        </div>
                    @endif

                    @foreach($model->contacts as $contact)
                        @if($contact->value != $model->email && $contact->value != $model->phone)
                            <div class="values__item">
                                <span class="label">{{ $contact->type_label }}</span> {{ $contact->value }}
                                @if($contact->name != $model->adResume->name)
                                    ({{$contact->name}})
                                @endif
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection