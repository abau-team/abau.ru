@extends('layouts.page')

@php
    /**
          * @var Ad $model
          */
        use App\Models\Ad;
        $title = trans('ad.vacancy-view.seo.title', ['vacancy_name' => $model->title, 'place_name' => $model->place->name_prepositional, 'company_name' => $model->adVacancy->company_name]);//__('seo.ad.create.index.title', ['city_name' => __("place.places.{$place->name}.genitive")]);
        $description = trans('ad.vacancy-view.seo.description' );
        $keywords = '';
        $breadcrumbs = [
            ['text' => $model->place->name_nominative, 'url' => route('ad.search', ['place' => $model->place->code])],
            ['text' => $model->category->local_name, 'url' => route('ad.searchCategory', ['place' => $model->place->code, 'category' => $model->category->code])],
            ['text' => $model->adVacancy->company_name . " - " . $model->title, 'url' => route('ad.view.full', ['place' => $model->place->code, 'category' => $model->category->code, 'id' => $model->ad_id])],
        ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <div class="ad-view">
        <div class="ad-view__header">
            <div>
                <h1>{{ $model->title }}</h1>
                <div class="line">
                    {{ trans('ad.vacancy-view.ad_id', ['ad_id' => $model->ad_id]) }},
                    {{ trans('ad.vacancy-view.views', ['views' => $model->views]) }},
                    {{ trans('ad.vacancy-view.created_at', ['time' => date('d.m.Y H:m', strtotime($model->created_at))])  }}
                </div>
            </div>
            <div class="price">
                {{ $model->adVacancy->price }}
            </div>
        </div>

        @can('update', $model)
            <div class="ad__control">
                <a href="{{ route('ad.updateForm', ['id' => $model->ad_id]) }}">{{ trans('ad.ad-search.edit') }}</a>
                <a href="{{ route('ad.delete', ['id' => $model->ad_id]) }}">{{ trans('ad.ad-search.delete') }}</a>

                @can('admin', $model)
                    <a href="{{ route('ad.block', ['id' => $model->ad_id]) }}">{{ trans('ad.ad-search.block') }}</a>
                @endcan
            </div>
        @endcan

        <div class="ad-view__body">
            <div class="ad-view__column">

                <h2>{{ trans('ad.vacancy-view.add_info') }}</h2>

                <div class="ad-view__add-info">
                    <div class="values">
                        <div class="values__item">
                            <span class="label">{{ trans('ad.vacancy.industry') }}</span> {{ $model->adVacancy->jobIndustry }}
                        </div>

                        <div class="values__item">
                            <span class="label">{{ trans('ad.vacancy.professional_area') }}</span> {{ $model->adVacancy->jobProfessionalArea }}
                        </div>

                        <div class="values__item">
                            <span class="label">{{ trans('ad.vacancy.employment') }}</span> {{ $model->adVacancy->employment_label }}
                        </div>
                        <div class="values__item">
                            <span class="label">{{ trans('ad.vacancy.schedule') }}</span> {{ $model->adVacancy->schedule_label }} {{ $model->adVacancy->schedule_comment ? "({$model->adVacancy->schedule_comment})" : '' }}
                        </div>
                        <div class="values__item">
                            <span class="label">{{ trans('ad.vacancy.experience') }}</span> {{ $model->adVacancy->experience_label }}
                        </div>
                        <div class="values__item">
                            <span class="label">{{ trans('ad.vacancy.education') }}</span> {{ $model->adVacancy->education_label  }}
                        </div>
                        @if(count($model->adVacancy->driveLicenses) > 0)
                            <div class="values__item">
                                <span class="label">{{ trans('ad.vacancy.driveLicenses') }}</span> {{ $model->adVacancy->driveLicense }}
                            </div>
                        @endif

                        @if($model->adVacancy->jobSkills->count())
                            <div class="values__item">
                                <span class="label">{{ trans('ad.vacancy.jobSkills') }}</span> {{ $model->adVacancy->jobSkill }}
                            </div>
                        @endif
                    </div>
                </div>

                <h2>{{ trans('ad.vacancy-view.description') }}</h2>
                <div class="ad-view__text">
                    {{ $model->description }}
                </div>

                @if($model->photos->count())
                    <div class="ad-view__photos">
                        @foreach($model->photos as $key => $photo)
                            <a href="{{ $photo->path }}">
                                <img src="{{ $photo->path }}" alt="{{ $title . '_' . $key }}"/>
                            </a>
                        @endforeach
                    </div>
                @endif


            </div>
            <div class="ad-view__column">
                <h2>{{ trans('ad.vacancy-view.contacts') }}</h2>
                <div class="ad-view__contact">


                    <div class="values__item">
                        <span class="label">{{ trans('ad.vacancy-view.company_name') }}</span> {{ $model->adVacancy->company_name }}
                    </div>

                    <div class="values__item">
                        <span class="label">{{ trans('ad.vacancy-view.address') }}</span> {{ $model->adVacancy->address }}
                    </div>

                    <div class="values__item">
                        <span class="label">{{ trans('ad.vacancy-view.contact_name') }}</span> {{ $model->adVacancy->name }}
                    </div>

                    @if($model->email)
                        <div class="values__item">
                            <span class="label">{{ trans('ad.vacancy-view.email') }}</span> {{ $model->email }}
                        </div>
                    @endif

                    @if($model->phone)
                        <div class="values__item">
                            <span class="label">{{ trans('ad.vacancy-view.phone') }}</span> {{ $model->phone }}
                        </div>
                    @endif

                    @foreach($model->contacts as $contact)
                        @if($contact->value != $model->email && $contact->value != $model->phone)
                            <div class="values__item">
                                <span class="label">{{ $contact->type_label }}</span> {{ $contact->value }}
                                @if($contact->name != $model->adVacancy->name)
                                    ({{ $contact->name }})
                                @endif
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection