@extends('layouts.page')

@php
        $title = trans('ad.resume-create.seo.title');
        $description = trans('ad.resume-create.seo.description');
        $keywords = trans('ad.resume-create.seo.keywords');
        $breadcrumbs = [
            ['text' => trans('ad.ad-create.breadcrumbs'), 'url' => route('ad.create.form')],
            ['text' => trans('ad.resume-create.breadcrumbs'), 'url' => route('ad.create.formCategory', ['category' => 'resume'])],
        ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')


    <form class="form form__ad form__ad_vacancy" method="POST" action="{{ route('ad.create.create') }}"
          enctype="multipart/form-data">

        <div class="form__header">
            <h1>{{ trans('ad.resume-create.title') }}</h1>

            <p>{{ trans('ad.resume-create.synopsis') }}</p>
        </div>

        <div class="form__body">
            @csrf
            <input type="hidden" name="ad_category_id"
                   value="{{ \App\Models\AdCategory::whereCode('resumes')->first(['ad_category_id'])->ad_category_id }}">

            <div class="form__block">

                <div class="form-group form-group__select required @error('place_id') has-error @enderror">
                    <label for="place_id">{{ __("ad.resume.place") }}</label>
                    <select name="place_id" id="place_id" class="form-control" autocomplete="off">
                        <option value="">{{ trans('ad.resume.place') }}</option>
                        @foreach(\App\Models\Place::whereIsLeaf()->orderBy('capital', 'desc')->orderBy('_lft')->get() as $value)
                            @if(old('place_id'))
                                <option {{old('place_id') == $value->place_id ? 'selected' : ''}}
                                        value="{{ $value->place_id }}">{{ $value->local_name }}</option>
                            @else
                                <option {{session('place', 'nizhny_novgorod') == $value->code ? 'selected' : ''}}
                                        value="{{ $value->place_id }}">{{ $value->local_name }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('place_id')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('title') has-error @enderror">
                    <label for="title">{{ __("ad.resume.title") }}</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}"
                           placeholder="{{ __("ad.resume.title") }}" required autofocus>
                    @error('title')
                    <span class="help-block help-block__error">{{ $message }}</span>
                    @enderror
                    <span class="help-block help-block__hint">{{ trans('ad.resume.title-hint') }}</span>
                </div>

                <div class="form-group form-group__select required @error('job_professional_area') has-error @enderror">
                    <label for="professional-area">{{ trans("ad.resume.professional_area") }}</label>

                    <select name="job_professional_area" id="professional-area" class="form-control">
                        <option value="">{{ trans("ad.resume.professional_area") }}</option>
                        @foreach(\App\Models\JobProfessionalArea::getTree() as $value)
                            <option {{old('job_professional_area') == $value->job_professional_area_id ? 'selected' : ''}}
                                    value="{{ $value->job_professional_area_id }}">{{ $value->local_name }}</option>
                        @endforeach

                    </select>
                    @error('job_professional_area')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('description') has-error @enderror">
                    <label for="vacancy_description">{{ __("ad.resume.description") }}</label>
                    <textarea type="text" name="description" id="vacancy_description" class="form-control"
                              placeholder="{{ __("ad.resume.description") }}"
                              required>{{ old('description') }}</textarea>
                    @error('description')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                    <span class="help-block help-block__hint">{{ trans('ad.resume.description-hint') }}</span>
                </div>

                <div class="form-group form-group__input @error('job_skill') has-error @enderror">
                    <label for="job_skill">{{ trans("ad.resume.job_skill") }}</label>

                    <input type="text" name="job_skill" id="job_skill" class="form-control"
                           value="{{ old('job_skill') }}"
                           placeholder="{{ trans('ad.resume.job_skill') }}">

                    @error('job_skill')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                    <span class="help-block help-block__hint">{{ trans('ad.resume.job_skill-hint') }}</span>
                </div>
            </div>

            <div class="form__block">
                <h2>Сведения об условиях и оплате труда</h2>

                <div class="form-group form-group__checkbox @error('remote_work') has-error @enderror">

                    <div class="checkbox">
                        <input type="checkbox" id="remote_work"
                               name="remote_work" {{ old('remote_work') ? 'checked' : '' }}>
                        <label for="remote_work">{{ trans('ad.resume.remote_work') }}</label>
                    </div>

                    @error('remote_work')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>


                <div class="form-group form-group__select required @error('schedule') has-error @enderror">
                    <label for="vacancy_schedule">{{ __("ad.resume.schedule") }}</label>

                    <select name="schedule" id="vacancy_schedule" class="form-control">
                        @foreach(\App\Models\AdResume::getSchedules() as $key => $value)
                            <option {{old('schedule') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('schedule')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('employment') has-error @enderror">
                    <label for="vacancy_employment">{{ __("ad.resume.employment") }}</label>

                    <select name="employment" id="vacancy_employment" class="form-control">
                        @foreach(\App\Models\AdResume::getEmployments() as $key => $value)
                            <option {{old('employment') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('employment')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('education') has-error @enderror">
                    <label for="education">{{ __("ad.resume.education") }}</label>

                    <select name="education" id="education" class="form-control">
                        @foreach(\App\Models\AdResume::getEducation() as $key => $value)
                            <option {{old('education') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('education')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('experience') has-error @enderror">
                    <label for="experience">{{ __("ad.resume.experience") }}</label>

                    <select name="experience" id="experience" class="form-control">
                        @foreach(\App\Models\AdResume::getExperiences() as $key => $value)
                            <option {{old('experience') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('experience')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__checkbox @error('remote_work') has-error @enderror">

                    <label>{{ trans("ad.resume.driveLicenses") }}</label>

                    <div class="driver_licence">
                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_A"
                                   name="driver_licence[A]" {{ old('driver_licence[A]') ? 'checked' : '' }}>
                            <label for="driver_licence_A">
                                A
                            </label>
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_B"
                                   name="driver_licence[B]" {{ old('driver_licence[B]') ? 'checked' : '' }}>
                            <label for="driver_licence_B">
                                B
                            </label>
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_C"
                                   name="driver_licence[C]" {{ old('driver_licence[C]') ? 'checked' : '' }}>
                            <label for="driver_licence_C">
                                C
                            </label>
                        </div>

                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_D"
                                   name="driver_licence[D]" {{ old('driver_licence[D]') ? 'checked' : '' }}>
                            <label for="driver_licence_D">
                                D
                            </label>
                        </div>

                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_E"
                                   name="driver_licence[E]" {{ old('driver_licence[E]') ? 'checked' : '' }}>
                            <label for="driver_licence_E">
                                E
                            </label>
                        </div>
                    </div>


                    @error('driver_licence')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__inline">
                    <div class="form-group__input required @error('salary') has-error @enderror">
                        <label for="salary">{{ trans("ad.resume.salary") }}</label>
                        <input type="text" name="salary" id="salary" class="form-control"
                               value="{{ old('salary') }}"
                               placeholder="{{ trans("ad.resume.salary") }}" required>
                        @error('salary')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group__select required @error('currency') has-error @enderror">
                        <label for="currency">{{ trans("ad.resume.currency") }}</label>

                        <select name="currency" id="currency" class="form-control">
                            @foreach(\App\Models\Ad::currencies() as $key => $value)
                                <option {{old('currency') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('currency')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                    <span class="help-block help-block__hint">{{ trans('ad.resume.salary-hint') }}</span>
                </div>

            </div>

            <div class="form__block">

                <h2>Контактная информация</h2>


                <div class="form-group form-group__inline">

                    <div class="form-group__input required @error('last_name') has-error @enderror">
                        <label for="last_name">{{ __("ad.resume.last_name") }}</label>
                        <input type="text" name="last_name" id="last_name"
                               class="form-control"
                               value="{{ old('last_name') }}"
                               placeholder="{{ __("ad.resume.last_name") }}" required>
                        @error('last_name')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group__input required @error('first_name') has-error @enderror">
                        <label for="first_name">{{ __("ad.resume.first_name") }}</label>
                        @if(old('first_name') == null && auth()->check() && isset(auth()->user()->name))
                            <input type="text" name="first_name" id="first_name"
                                   class="form-control"
                                   value="{{ auth()->user()->name }}"
                                   placeholder="{{ __("ad.resume.first_name") }}" required>
                        @else
                            <input type="text" name="first_name" id="first_name"
                                   class="form-control"
                                   value="{{ old('first_name') }}"
                                   placeholder="{{ __("ad.resume.first_name") }}" required>
                        @endif
                        @error('first_name')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group__input required @error('patronymic_name') has-error @enderror">
                        <label for="patronymic_name">{{ __("ad.resume.patronymic_name") }}</label>
                        <input type="text" name="patronymic_name" id="patronymic_name"
                               class="form-control"
                               value="{{ old('patronymic_name') }}"
                               placeholder="{{ __("ad.resume.patronymic_name") }}" required>
                        @error('patronymic_name')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>

                </div>

                <div class="form-group form-group__select required @error('sex') has-error @enderror">
                    <label for="sex">{{ __("ad.resume.sex") }}</label>

                    <select name="sex" id="sex" class="form-control">
                        @foreach(\App\Models\AdResume::getSexes() as $key => $value)
                            <option {{old('sex') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('sex')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>


                <div class="form-group form-group__inline">

                    <div class="form-group__select required @error('day_of_birth') has-error @enderror">
                        <label for="day_of_birth">{{ trans("ad.resume.day_of_birth") }}</label>

                        <select name="day_of_birth" id="day_of_birth" class="form-control">
                            @foreach(range(1,31) as $value)
                                <option {{old('day_of_birth') == $value ? 'selected' : ''}} value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('day_of_birth')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group__select required @error('month_of_birth') has-error @enderror">
                        <label for="month_of_birth">{{ trans("ad.resume.month_of_birth") }}</label>

                        <select name="month_of_birth" id="month_of_birth" class="form-control">
                            @foreach(\App\Models\AdResume::getMonths() as $key => $value)
                                <option {{old('month_of_birth') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('month_of_birth')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>


                    <div class="form-group__select required @error('year_of_birth') has-error @enderror">
                        <label for="year_of_birth">{{ trans("ad.resume.year_of_birth") }}</label>

                        <select name="year_of_birth" id="year_of_birth" class="form-control">
                            @foreach(range(2006, 1930) as $value)
                                <option {{old('month_of_birth') == $value ? 'selected' : ''}} value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('year_of_birth')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                    @error('date_of_birth')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('email') has-error @enderror">
                    <label for="email">{{ __("ad.resume.contact_email") }}</label>
                    @if(old('email') == null && auth()->check() && isset(auth()->user()->emails[0]))
                        <input type="email" name="email" id="email" class="form-control"
                               value="{{ auth()->user()->emails[0]->value }}"
                               placeholder="{{ __("ad.resume.contact_email") }}" required>
                    @else
                        <input type="email" name="email" id="email" class="form-control"
                               value="{{ old('email') }}"
                               placeholder="{{ __("ad.resume.contact_email") }}" required>
                    @endif
                    @error('email')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('phone') has-error @enderror">
                    <label for="phone">{{ __("ad.resume.contact_phone") }}</label>
                    @if(old('phone') == null && auth()->check() && isset(auth()->user()->phones[0]))
                        <input type="tel" name="phone" id="phone" class="form-control"
                               value="{{ auth()->user()->phones[0]->value }}"
                               placeholder="{{ __("ad.resume.contact_phone") }}" required>
                    @else
                        <input type="tel" name="phone" id="phone" class="form-control"
                               value="{{ old('phone') }}"
                               placeholder="{{ __("ad.resume.contact_phone") }}" required>
                    @endif
                    @error('phone')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>


                <div class="form-group form-group__input required @error('address') has-error @enderror">
                    <label for="address">{{ __("ad.resume.address") }}</label>
                    <input type="text" name="address" id="address"
                           class="form-control"
                           value="{{ old('address') }}"
                           placeholder="{{ __("ad.resume.address") }}" required>
                    @error('address')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>


                <div class="form-group form-group__input @error('photos') has-error @enderror">
                    <label for="photo">{{ __("ad.resume.photos") }}</label>
                    <input type="file" multiple="multiple" name="photos[]" accept="image"/>
                    @error('photos')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                @if(!auth()->check())

                    <div class="form-group form-group__checkbox @error('want_register') has-error @enderror">

                        <div class="checkbox">
                            <input type="checkbox" id="want_register"
                                   name="want_register" {{ old('want_register') ? 'checked' : '' }}>
                            <label for="want_register">
                                Я хочу зарегистрироваться и пользоваться всеми преимуществами сайта
                            </label>
                        </div>

                        @error('want_register')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>

                @endif

            </div>

        </div>

        <div class="form__footer">
            <div class="form-group form-group__btn">
                <button type="submit" class="btn btn-green">
                    {{ __('ad.resume.submit') }}
                </button>
            </div>
        </div>
    </form>
@endsection