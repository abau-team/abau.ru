@extends('layouts.page')

@php
        $title = trans('ad.ad-create.seo.title');
        $description = trans('ad.ad-create.seo.description');
        $keywords = trans('ad.ad-create.seo.keywords');
        $breadcrumbs = [
            ['text' => trans('ad.ad-create.breadcrumbs'), 'url' => route('ad.create.form')],
        ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <h1>Добавить объявление</h1>
    <p>Выберите категорию в которую вы хотите подать объявление:</p>
    <ul>
        <li><a href="{{ route('ad.create.formCategory', ['category' => 'vacancy']) }}">Разместить вакансию</a></li>
        <li><a href="{{ route('ad.create.formCategory', ['category' => 'resume']) }}">Создать резюме</a></li>
    </ul>
@endsection