@extends('layouts.page')

@php
        $title = trans('ad.vacancy-create.seo.title');
        $description = trans('ad.vacancy-create.seo.description');
        $keywords = trans('ad.vacancy-create.seo.keywords');
        $breadcrumbs = [
            ['text' => trans('ad.ad-create.breadcrumbs'), 'url' => route('ad.create.form')],
            ['text' => trans('ad.vacancy-create.breadcrumbs'), 'url' => route('ad.create.formCategory', ['category' => 'vacancy'])],
        ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')


    <form class="form form__ad form__ad_vacancy" method="POST" action="{{ route('ad.create.create') }}"
          enctype="multipart/form-data">

        <div class="form__header">
            <h1>{{ trans('ad.vacancy-create.title') }}</h1>

            <p>{{ trans('ad.vacancy-create.synopsis') }}</p>
        </div>

        <div class="form__body">
            @csrf
            <input type="hidden" name="ad_category_id"
                   value="{{ \App\Models\AdCategory::whereCode('vacancies')->first(['ad_category_id'])->ad_category_id }}">

            <div class="form__block">

                <div class="form-group form-group__select required @error('place_id') has-error @enderror">
                    <label for="place_id">{{ __("ad.resume.place") }}</label>
                    <select name="place_id" id="place_id" class="form-control">
                        <option value="">{{ trans('ad.resume.place') }}</option>
                        @foreach(\App\Models\Place::whereIsLeaf()->orderBy('capital', 'desc')->orderBy('_lft')->get() as $value)
                            @if(old('place_id'))
                                <option {{old('place_id') == $value->place_id ? 'selected' : ''}}
                                        value="{{ $value->place_id }}">{{ $value->local_name }}</option>
                            @else
                                <option {{session('place', 'nizhny_novgorod') == $value->code ? 'selected' : ''}}
                                        value="{{ $value->place_id }}">{{ $value->local_name }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('place_id')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('job_industry') has-error @enderror">
                    <label for="job_industry">{{ __("ad.vacancy.industry") }}</label>
                    <select name="job_industry" id="job_industry" class="form-control">
                        <option value="">{{ trans('ad.vacancy.industry') }}</option>
                        @foreach(\App\Models\JobIndustry::getTree() as $value)
                            <option {{request('job_industry') == $value->job_industry_id ? 'selected' : ''}}
                                    value="{{ $value->job_industry_id }}">{{ $value->local_name }}</option>
                        @endforeach
                    </select>
                    @error('job_industry')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>


                <div class="form-group form-group__input required @error('title') has-error @enderror">
                    <label for="title">{{ __("ad.vacancy.title") }}</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}"
                           placeholder="{{ __("ad.vacancy.title") }}" required autofocus>
                    @error('title')
                    <span class="help-block help-block__error">{{ $message }}</span>
                    @enderror
                    <span class="help-block help-block__hint">{{ trans('ad.vacancy.title-hint') }}</span>
                </div>

                <div class="form-group form-group__select required @error('job_professional_area') has-error @enderror">
                    <label for="professional-area">{{ trans("ad.vacancy.professional_area") }}</label>

                    <select name="job_professional_area" id="professional-area" class="form-control">
                        <option value="">{{ trans("ad.vacancy.professional_area") }}</option>
                        @foreach(\App\Models\JobProfessionalArea::getTree() as $value)
                            <option {{old('job_professional_area') == $value->job_professional_area_id ? 'selected' : ''}}
                                    value="{{ $value->job_professional_area_id }}">{{ $value->local_name }}</option>
                        @endforeach

                    </select>
                    @error('job_professional_area')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('description') has-error @enderror">
                    <label for="vacancy_description">{{ __("ad.vacancy.description") }}</label>
                    <textarea type="text" name="description" id="vacancy_description" class="form-control"
                              placeholder="{{ __("ad.vacancy.description") }}"
                              required>{{ old('description') }}</textarea>
                    @error('description')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                    <span class="help-block help-block__hint">{{ trans('ad.vacancy.description-hint') }}</span>
                </div>

                <div class="form-group form-group__input @error('job_skill') has-error @enderror">
                    <label for="job_skill">{{ trans("ad.vacancy.job_skill") }}</label>

                    <input type="text" name="job_skill" id="job_skill" class="form-control"
                           value="{{ old('job_skill') }}"
                           placeholder="{{ trans('ad.vacancy.job_skill') }}">

                    @error('job_skill')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                    <span class="help-block help-block__hint">{{ trans('ad.vacancy.job_skill-hint') }}</span>
                </div>
            </div>

            <div class="form__block">
                <h2>Сведения об условиях и оплате труда</h2>

                <div class="form-group form-group__checkbox @error('remote_work') has-error @enderror">

                    <div class="checkbox">
                        <input type="checkbox" id="remote_work"
                               name="remote_work" {{ old('remote_work') ? 'checked' : '' }}>
                        <label for="remote_work">{{ trans('ad.vacancy.remote_work') }}</label>
                    </div>

                    @error('remote_work')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>


                <div class="form-group form-group__select required @error('schedule') has-error @enderror">
                    <label for="vacancy_schedule">{{ __("ad.vacancy.schedule") }}</label>

                    <select name="schedule" id="vacancy_schedule" class="form-control">
                        @foreach(\App\Models\AdVacancy::getSchedules() as $key => $value)
                            <option {{old('schedule') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('schedule')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('employment') has-error @enderror">
                    <label for="vacancy_employment">{{ __("ad.vacancy.employment") }}</label>

                    <select name="employment" id="vacancy_employment" class="form-control">
                        @foreach(\App\Models\AdVacancy::getEmployments() as $key => $value)
                            <option {{old('employment') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('employment')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('education') has-error @enderror">
                    <label for="education">{{ __("ad.vacancy.education") }}</label>

                    <select name="education" id="education" class="form-control">
                        @foreach(\App\Models\AdVacancy::getEducation() as $key => $value)
                            <option {{old('education') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('education')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('experience') has-error @enderror">
                    <label for="experience">{{ __("ad.vacancy.experience") }}</label>

                    <select name="experience" id="experience" class="form-control">
                        @foreach(\App\Models\AdVacancy::getExperiences() as $key => $value)
                            <option {{old('experience') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('experience')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__checkbox @error('remote_work') has-error @enderror">

                    <label>{{ trans("ad.vacancy.driveLicenses") }}</label>

                    <div class="driver_licence">
                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_A"
                                   name="driver_licence[A]" {{ old('driver_licence[A]') ? 'checked' : '' }}>
                            <label for="driver_licence_A">
                                A
                            </label>
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_B"
                                   name="driver_licence[B]" {{ old('driver_licence[B]') ? 'checked' : '' }}>
                            <label for="driver_licence_B">
                                B
                            </label>
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_C"
                                   name="driver_licence[C]" {{ old('driver_licence[C]') ? 'checked' : '' }}>
                            <label for="driver_licence_C">
                                C
                            </label>
                        </div>

                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_D"
                                   name="driver_licence[D]" {{ old('driver_licence[D]') ? 'checked' : '' }}>
                            <label for="driver_licence_D">
                                D
                            </label>
                        </div>

                        <div class="checkbox">
                            <input type="checkbox" id="driver_licence_E"
                                   name="driver_licence[E]" {{ old('driver_licence[E]') ? 'checked' : '' }}>
                            <label for="driver_licence_E">
                                E
                            </label>
                        </div>
                    </div>


                    @error('driver_licence')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__inline">
                    <div class="form-group__input required @error('salary_from') has-error @enderror">
                        <label for="vacancy_salary_from">{{ trans("ad.vacancy.salary_from") }}</label>
                        <input type="text" name="salary_from" id="vacancy_salary_from" class="form-control"
                               value="{{ old('salary_from') }}"
                               placeholder="{{ trans("ad.vacancy.salary_from") }}" required>
                        @error('salary_from')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group__input required @error('salary_to') has-error @enderror">
                        <label for="vacancy_salary_to">{{ trans("ad.vacancy.salary_to") }}</label>
                        <input type="text" name="salary_to" id="vacancy_salary_to" class="form-control"
                               value="{{ old('salary_to') }}"
                               placeholder="{{ trans("ad.vacancy.salary_to") }}" required>
                        @error('salary_to')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group__select required @error('currency') has-error @enderror">
                        <label for="currency">{{ trans("ad.vacancy.currency") }}</label>

                        <select name="currency" id="currency" class="form-control">
                            @foreach(\App\Models\Ad::currencies() as $key => $value)
                                <option {{old('currency') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('currency')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                    <span class="help-block help-block__hint">{{ trans('ad.vacancy.salary-hint') }}</span>
                </div>

            </div>

            <div class="form__block">

                <h2>Контактная информация</h2>

                <div class="form-group form-group__input required @error('company_name') has-error @enderror">
                    <label for="company_name">{{ __("ad.vacancy.contact_company_name") }}</label>
                    <input type="text" name="company_name" id="company_name"
                           class="form-control"
                           value="{{ old('company_name') }}"
                           placeholder="{{ __("ad.vacancy.contact_company_name") }}" required>
                    @error('company_name')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('address') has-error @enderror">
                    <label for="address">{{ __("ad.vacancy.address") }}</label>
                    <input type="text" name="address" id="address"
                           class="form-control"
                           value="{{ old('address') }}"
                           placeholder="{{ __("ad.vacancy.address") }}" required>
                    @error('address')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('name') has-error @enderror">
                    <label for="name">{{ __("ad.vacancy.name") }}</label>

                    @if(old('name') == null && auth()->check() && isset(auth()->user()->name))
                        <input type="text" name="name" id="name" class="form-control"
                               value="{{ auth()->user()->name }}"
                               placeholder="{{ __("ad.vacancy.name") }}" required>
                    @else
                        <input type="text" name="name" id="name" class="form-control"
                               value="{{ old('name') }}"
                               placeholder="{{ __("ad.vacancy.name") }}" required>
                    @endif
                    @error('name')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>


                <div class="form-group form-group__input required @error('email') has-error @enderror">
                    <label for="email">{{ __("ad.vacancy.contact_email") }}</label>
                    @if(old('email') == null && auth()->check() && isset(auth()->user()->emails[0]))
                        <input type="email" name="email" id="email" class="form-control"
                               value="{{ auth()->user()->emails[0]->value }}"
                               placeholder="{{ __("ad.vacancy.contact_email") }}" required>
                    @else
                        <input type="email" name="email" id="email" class="form-control"
                               value="{{ old('email') }}"
                               placeholder="{{ __("ad.vacancy.contact_email") }}" required>
                    @endif
                    @error('email')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('phone') has-error @enderror">
                    <label for="phone">{{ __("ad.vacancy.contact_phone") }}</label>
                    @if(old('phone') == null && auth()->check() && isset(auth()->user()->phones[0]))
                        <input type="tel" name="phone" id="phone" class="form-control"
                               value="{{ auth()->user()->phones[0]->value }}"
                               placeholder="{{ __("ad.vacancy.contact_phone") }}" required>
                    @else
                        <input type="tel" name="phone" id="phone" class="form-control"
                               value="{{ old('phone') }}"
                               placeholder="{{ __("ad.vacancy.contact_phone") }}" required>
                    @endif
                    @error('phone')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input @error('photos') has-error @enderror">
                    <label for="photo">{{ __("ad.vacancy.photos") }}</label>
                    <input type="file" multiple="multiple" name="photos[]" accept="image"/>
                    @error('photos')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                @if(!auth()->check())

                    <div class="form-group form-group__checkbox @error('want_register') has-error @enderror">

                        <div class="checkbox">
                            <input type="checkbox" id="want_register"
                                   name="want_register" {{ old('want_register') ? 'checked' : '' }}>
                            <label for="want_register">
                                Я хочу зарегистрироваться и пользоваться всеми преимуществами сайта
                            </label>
                        </div>

                        @error('want_register')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>

                @endif

            </div>

        </div>

        <div class="form__footer">
            <div class="form-group form-group__btn">
                <button type="submit" class="btn btn-green">
                    {{ __('ad.vacancy.submit') }}
                </button>
            </div>
        </div>
    </form>
@endsection