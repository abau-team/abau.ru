@extends('layouts.page')

@php
    /**
          * @var \App\Models\Place $place
          */
        $title = __('seo.ad.create.index.title', ['city_name' => __("place.places.{$place->name}.genitive")]);
        $description = __('seo.ad.create.index.description', ['city_name' => __("place.places.{$place->name}.genitive")]);
        $keywords = __('seo.ad.create.index.keywords', ['city_name' => __("place.places.{$place->name}.genitive")]);
        $breadcrumbs = [
            ['text' => __("place.places.{$place->name}.nominative"), 'url' => route('ad.search')],
            ['text' => __('seo.ad.create.index.breadcrumbs', ['city_name' => __("place.places.{$place->name}.genitive")]), 'url' => route('ad.create.index')],
        ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <h1>Добавить объявление</h1>
    <p>Выберите категорию в которую вы хотите подать объявление:</p>
    <ul>
        <li><a href="{{ route('ad.create.vacancy') }}">Разместить вакансию</a></li>
        <li><a href="{{ route('ad.create.resume') }}">Создать резюме</a></li>
    </ul>
@endsection