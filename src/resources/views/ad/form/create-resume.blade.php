@extends('layouts.page')

@php
    /** @var Place $place */
    use App\Models\Place;

    $title = __('seo.ad.create.resume.title');
    $description = __('seo.ad.create.resume.description');
    $keywords = __('seo.ad.create.resume.keywords');
    $breadcrumbs = [
        ['text' => __("place.places.{$place->name}.nominative"), 'url' => route('ad.search')],
        ['text' => __('seo.ad.create.index.breadcrumbs'), 'url' => route('ad.create.index')],
        ['text' => __('seo.ad.create.resume.breadcrumbs'), 'url' => route('ad.create.resume')],
    ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <h1>Добавить объявление</h1>
    <p>Выберите категорию в которую вы хотите подать объявление:</p>
    <ul>
        <li>Разместить вакансию</li>
        <li>Создать резюме</li>
    </ul>
@endsection