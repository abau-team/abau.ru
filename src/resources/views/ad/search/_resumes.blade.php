@php
    use App\Models\Ad;
    use App\Models\Place;
    use App\Models\AdCategory;
    /**
      * @var Place $place
      * @var AdCategory $category
      * @var Ad $ad
      */
@endphp
<div class="ad-search__item ad">
    <div class="ad__image">
        <a href="{{ route('ad.view.full', ['place' => $place->code, 'category' => $category->code, 'id' => $ad->ad_id]) }}">
            <img src="{{ isset($ad->photos[0]) && $ad->photos[0]->path ? $ad->photos[0]->path : asset('/images/no-image.png') }}" alt="{{ $ad->title }}">
        </a>
    </div>
    <div class="ad__text">
        <h2>
            <a href="{{ route('ad.view.full', ['place' => $place->code, 'category' => $category->code, 'id' => $ad->ad_id]) }}">
                {{$ad->title}}
            </a>
        </h2>

        <div class="ad__category">{{ $category->local_name }}</div>

        <div class="ad__category">{{ $ad->adResume->jobProfessionalArea }}</div>

        <div class="ad__text-group">
            <div class="ad__date">{{ $ad->created_at }}</div>
            <div class="ad__views">{{ trans('ad.vacancy-view.views', ['views' => $ad->views]) }}</div>
        </div>
        <div class="ad__price">{{ $ad->adResume->price }}</div>
        @can('update', $ad)
            <div class="ad__control">
                <a href="{{ route('ad.updateForm', ['id' => $ad->ad_id]) }}">{{ trans('ad.ad-search.edit') }}</a>
                <a href="{{ route('ad.delete', ['id' => $ad->ad_id]) }}">{{ trans('ad.ad-search.delete') }}</a>

                @can('admin', $ad)
                    <a href="{{ route('ad.block', ['id' => $ad->ad_id]) }}">{{ trans('ad.ad-search.block') }}</a>
                @endcan
            </div>
        @endcan
    </div>
</div>