@extends('layouts.search')

@php
    use App\Models\Place;
    use App\Models\AdCategory;
    use App\Models\Ad;
    /**
      * @var Place $place
      * @var AdCategory $category
      * @var Ad[] $ads
      */
        $title = trans("ad.ad-search.{$category->code}.seo.title", ['place_name' => $place->name_prepositional]);
        $description = trans("ad.ad-search.{$category->code}.seo.description", ['place_name' => $place->name_prepositional]);
        $keywords = trans("ad.ad-search.{$category->code}.seo.keywords", ['place_name' => $place->name_prepositional]);
        $breadcrumbs = [
            ['text' => $place->name_nominative, 'url' => route('ad.search', ['place' => $place->code])],
            ['text' => $category->local_name, 'url' => route('ad.searchCategory', ['place' => $place->code, 'category' => $category->code])],
        ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <h1>{{ trans("ad.ad-search.{$category->code}.seo.h1", ['place_name' => $place->name_prepositional]) }}</h1>
    <div class="ad-search__list">
        @foreach($ads as $ad)

            @include("ad.search._{$category->code}", ['ad' => $ad, 'category' => $category, 'place' => $place])

        @endforeach
    </div>
    <div class="ad-search__pagination">
        {{$ads->appends(request()->input())->links()}}
    </div>
@endsection
@section('form')
    <form class="form form-search"
          action="{{ route('ad.searchCategory', ['place' => $place->code, 'category' => $category->code]) }}"
          method="GET" onchange="submit()">

        <div class="form__header">
            <h2>{{ trans("ad.ad-search.{$category->code}.form.title") }}</h2>
        </div>

        <div class="form__body">
            <div class="form-group @error('q') has-error @enderror">
                <input type="text" name="q" placeholder="{{ trans("ad.ad-search.q") }}"
                       value="{{ request('q') }}">
                @error('q')
                <span class="help-block">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group form-group__select required @error('place') has-error @enderror">
                <select name="place" class="form-control">
                    <option value="">{{ trans('ad.ad-search.place') }}</option>
                    @foreach(\App\Models\Place::whereIsLeaf()->orderBy('capital', 'desc')->orderBy('_lft')->get() as $value)
                        <option {{request('place') == $value->code ? 'selected' : ''}}
                                value="{{ $value->code }}">{{ $value->local_name }}</option>
                    @endforeach
                </select>
                @error('place')
                <span class="help-block">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group form-group__select required @error('category') has-error @enderror">
                <select name="category" class="form-control">
                    <option value="">{{ trans('ad.ad-search.category') }}</option>
                    @foreach(\App\Models\AdCategory::whereIsLeaf()->orderBy('_lft')->get() as $value)
                        <option {{request('category') == $value->code ? 'selected' : ''}}
                                value="{{ $value->code }}">{{ $value->local_name }}</option>
                    @endforeach
                </select>
                @error('place')
                <span class="help-block">{{ $message }}</span>
                @enderror
            </div>

            @include("ad.search._{$category->code}Form", ['category' => $category, 'place' => $place])

        </div>

        <div class="form__footer">
            <div class="form-group form-group__btn">
                <button type="submit" class="btn btn-green">
                    {{ __('ad.ad-search.search') }}
                </button>
            </div>
        </div>
    </form>
@endsection