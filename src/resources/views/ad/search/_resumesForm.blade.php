<div class="form-group form-group__select required @error('job_professional_area') has-error @enderror">

    <select name="job_professional_area" id="professional-area" class="form-control">
        <option value="">{{ trans('ad.resume.professional_area') }}</option>
        @foreach(\App\Models\JobProfessionalArea::getTree() as $value)
            <option {{request('job_professional_area') == $value->code ? 'selected' : ''}}
                    value="{{ $value->code }}">{{ $value->local_name }}</option>
        @endforeach
    </select>
    @error('job_professional_area')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__select required @error('schedule') has-error @enderror">

    <select name="schedule" id="vacancy_schedule" class="form-control">
        <option value="">{{ trans('ad.resume.schedule') }}</option>
        @foreach(\App\Models\AdVacancy::getSchedules() as $key => $value)
            <option {{request('schedule') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('schedule')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__select required @error('employment') has-error @enderror">

    <select name="employment" id="vacancy_employment" class="form-control">
        <option value="">{{ trans('ad.resume.employment') }}</option>
        @foreach(\App\Models\AdVacancy::getEmployments() as $key => $value)
            <option {{request('employment') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('employment')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__select required @error('sex') has-error @enderror">

    <select name="sex" id="sex" class="form-control">
        <option value="">{{ trans('ad.resume.sex') }}</option>
        @foreach(\App\Models\AdVacancy::getSexes() as $key => $value)
            <option {{request('sex') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('sex')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__checkbox @error('automobile') has-error @enderror">

    <div class="checkbox">
        <input type="checkbox" id="automobile"
               name="automobile" {{ request('automobile') ? 'checked' : '' }}>
        <label for="automobile">
            {{ trans('ad.resume.automobile') }}
        </label>
    </div>

    @error('automobile')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group @error('salary') has-error @enderror">
    <input type="text" name="salary" placeholder="{{ trans("ad.ad-search.resumes.form.salary") }}"
           value="{{ request('salary') }}">
    @error('salary')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__checkbox @error('salaryNotNull') has-error @enderror">

    <div class="checkbox">
        <input type="checkbox" id="salaryNotNull"
               name="salaryNotNull" {{ request('salaryNotNull') ? 'checked' : '' }}>
        <label for="salaryNotNull">
            {{ trans('ad.ad-search.resumes.form.salaryNotNull') }}
        </label>
    </div>

    @error('salaryNotNull')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>