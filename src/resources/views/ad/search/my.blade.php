@extends('layouts.search')

@php
    use App\Models\Place;
    use App\Models\Ad;
    /**
      * @var Place $place
      * @var Ad[] $ads
      */
        $title = trans("ad.ad-search.my.seo.title");
        $description = trans("ad.ad-search.my.seo.description");
        $keywords = trans("ad.ad-search.my.seo.keywords");
        $breadcrumbs = [
            ['text' => trans("ad.ad-search.my.seo.breadcrumbs"), 'url' => route('ad.my')],
        ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')
    <h1>{{ trans("ad.ad-search.my.seo.h1") }}</h1>

    <div class="ad-search__list">
        @foreach($ads as $ad)

            @include("ad.search._ad", ['ad' => $ad])

        @endforeach
    </div>
    <div class="ad-search__pagination">
        {{$ads->appends(request()->input())->links()}}
    </div>
@endsection
@section('form')
    <form class="form form-search"
          action="{{ route('ad.my') }}"
          method="GET" onchange="submit()">

        <div class="form__header">
            <h2>{{ trans("ad.ad-search.index.form.title") }}</h2>
        </div>

        <div class="form__body">
            <div class="form-group @error('q') has-error @enderror">
                <input type="text" name="q" placeholder="{{ trans("ad.ad-search.q") }}"
                       value="{{ request('q') }}">
                @error('q')
                <span class="help-block">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group form-group__select required @error('place') has-error @enderror">
                <select name="place" class="form-control">
                    <option value="">{{ trans('ad.ad-search.place') }}</option>
                    @foreach(\App\Models\Place::whereIsLeaf()->orderBy('capital', 'desc')->orderBy('_lft')->get() as $value)
                        <option {{request('place') == $value->code ? 'selected' : ''}}
                                value="{{ $value->code }}">{{ $value->local_name }}</option>
                    @endforeach
                </select>
                @error('place')
                <span class="help-block">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group form-group__select required @error('category') has-error @enderror">
                <select name="category" class="form-control">
                    <option value="">{{ trans('ad.ad-search.category') }}</option>
                    @foreach(\App\Models\AdCategory::whereIsLeaf()->orderBy('_lft')->get() as $value)
                        <option {{request('category') == $value->code ? 'selected' : ''}}
                                value="{{ $value->code }}">{{ $value->local_name }}</option>
                    @endforeach
                </select>
                @error('place')
                <span class="help-block">{{ $message }}</span>
                @enderror
            </div>

        </div>

        <div class="form__footer">
            <div class="form-group form-group__btn">
                <button type="submit" class="btn btn-green">
                    {{ __('ad.ad-search.search') }}
                </button>
            </div>
        </div>
    </form>
@endsection