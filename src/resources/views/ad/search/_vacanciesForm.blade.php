<div class="form-group form-group__select required @error('job_industry') has-error @enderror">
    <select name="job_industry" id="job_industry" class="form-control">
        <option value="">{{ trans('ad.vacancy.industry') }}</option>
        @foreach(\App\Models\JobIndustry::getTree() as $value)
            <option {{request('job_industry') == $value->code ? 'selected' : ''}}
                    value="{{ $value->code }}">{{ $value->local_name }}</option>
        @endforeach
    </select>
    @error('job_industry')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__select required @error('job_professional_area') has-error @enderror">

    <select name="job_professional_area" id="professional-area" class="form-control">
        <option value="">{{ trans('ad.vacancy.professional_area') }}</option>
        @foreach(\App\Models\JobProfessionalArea::getTree() as $value)
            <option {{request('job_professional_area') == $value->code ? 'selected' : ''}}
                    value="{{ $value->code }}">{{ $value->local_name }}</option>
        @endforeach
    </select>
    @error('job_professional_area')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__select required @error('schedule') has-error @enderror">

    <select name="schedule" id="vacancy_schedule" class="form-control">
        <option value="">{{ trans('ad.vacancy.schedule') }}</option>
        @foreach(\App\Models\AdVacancy::getSchedules() as $key => $value)
            <option {{request('schedule') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('schedule')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__select required @error('employment') has-error @enderror">

    <select name="employment" id="vacancy_employment" class="form-control">
        <option value="">{{ trans('ad.vacancy.employment') }}</option>
        @foreach(\App\Models\AdVacancy::getEmployments() as $key => $value)
            <option {{request('employment') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('employment')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__select required @error('experience') has-error @enderror">

    <select name="experience" id="experience" class="form-control">
        @foreach(\App\Models\AdVacancy::getExperiences() as $key => $value)
            <option {{request('experience') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @error('experience')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group @error('salary') has-error @enderror">
    <input type="text" name="salary" placeholder="{{ trans("ad.vacancy.salary_from") }}"
           value="{{ request('salary') }}">
    @error('salary')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>

<div class="form-group form-group__checkbox @error('remote_work') has-error @enderror">

    <div class="checkbox">
        <input type="checkbox" id="remote_work"
               name="remote_work" {{ request('remote_work') ? 'checked' : '' }}>
        <label for="remote_work">
            {{ trans('ad.vacancy.remote_work') }}
        </label>
    </div>

    @error('remote_work')
    <span class="help-block">{{ $message }}</span>
    @enderror
</div>
