@extends('layouts.page')

@php
    /**
      * @var Place $place
      */
        use App\Models\Place;

        $title = __('seo.ad.create.vacancy.title', ['city_name' => __("place.places.{$place->name}.genitive")]);
        $description = __('seo.ad.create.vacancy.description', ['city_name' => __("place.places.{$place->name}.genitive")]);
        $keywords = __('seo.ad.create.vacancy.keywords', ['city_name' => __("place.places.{$place->name}.genitive")]);
        $breadcrumbs = [
            ['text' => $place->name_nominative, 'url' => route('ad.search', ['place' => $place->code])],
            ['text' => __('seo.ad.create.index.breadcrumbs', ['city_name' => __("place.places.{$place->name}.genitive")]), 'url' => route('ad.create.index', ['place' => $place->code])],
            ['text' => __('seo.ad.create.vacancy.breadcrumbs', ['city_name' => __("place.places.{$place->name}.genitive")]), 'url' => route('ad.create.vacancy', ['place' => $place->code])],
        ];
@endphp

@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)

@section('endHead')
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$title}}">
    <meta property="twitter:title" content="{{$title}}">
    <meta property="og:description" content="{{$description}}">
    <meta property="twitter:description" content="{{$description}}">
    <meta property="og:image" content="{{asset('images/og-image.jpg')}}">
    @parent
@endsection

@section('content')


    <form class="form form__ad form__ad_vacancy" method="POST" action="{{ route('ad.create.create') }}" enctype="multipart/form-data">

        <div class="form__header">
            <h1>{{ __("Post vacancy in :city_name", ['city_name' => $place->name_prepositional]) }}</h1>

            <p>Для размещения вакансии заполните все необходимые поля, помеченные звездочкой</p>
        </div>

        <div class="form__body">
            @csrf
            <input type="hidden" name="place" value="{{ request('place') }}">
            <input type="hidden" name="ad_category_id" value="{{ \App\Models\AdCategory::whereCode('vacancies')->first(['ad_category_id'])->ad_category_id }}">



            <div class="form__block">

                <div class="form-group form-group__select required @error('job_industry_id') has-error @enderror">
                    <label for="industry">{{ __("ad.vacancy.industry") }}</label>

                    <select name="job_industry_id" id="industry" class="form-control">

                        @foreach(\App\Models\JobIndustry::getTree() as $value)
                            <optgroup label="{{ $value->local_name }}">
                                @foreach($value->children as $child)
                                    <option {{old('job_industry_id') == $child->job_industry_id ? 'selected' : ''}}
                                            value="{{ $child->job_industry_id }}">{{ $child->local_name }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach

                    </select>
                    @error('job_industry_id')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('title') has-error @enderror">
                    <label for="title">{{ __("ad.vacancy.title") }}</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}"
                           placeholder="{{ __("ad.vacancy.title") }}" required autofocus>
                    @error('title')
                    <span class="help-block help-block__error">{{ $message }}</span>
                    @enderror
                    <span class="help-block help-block__hint">Указывайте только название должности, запрещено указывать контактные данные и использовать любые другие способы привлечения внимания.</span>
                </div>

                <div class="form-group form-group__select required @error('job_professional_area_id') has-error @enderror">
                    <label for="professional-area">{{ __("ad.vacancy.professional_area") }}</label>

                    <select name="job_professional_area_id" id="professional-area" class="form-control">

                        @foreach(\App\Models\JobProfessionalArea::getTree() as $value)
                            <optgroup label="{{ $value->local_name }}">
                                @foreach($value->children as $child)
                                    <option {{old('job_professional_area_id') == $child->job_professional_area_id ? 'selected' : ''}}
                                            value="{{ $child->job_professional_area_id }}">{{ $child->local_name }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach

                    </select>
                    @error('job_professional_area_id')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('description') has-error @enderror">
                    <label for="vacancy_description">{{ __("ad.vacancy.description") }}</label>
                    <textarea type="text" name="description" id="vacancy_description" class="form-control"
                              placeholder="{{ __("ad.vacancy.description") }}"
                              required>{{ old('description') }}</textarea>
                    @error('description')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                    <span class="help-block help-block__hint">Для выделения заголовков допускается использование html тега - <b>&lt;b&gt;Текст&lt;/b&gt;</b></span>
                </div>
            </div>

            <div class="form__block">
                <h2>Сведения об условиях и оплате труда</h2>

                <div class="form-group form-group__checkbox @error('remote_work') has-error @enderror">

                    <div class="checkbox">
                        <input type="checkbox" id="remote_work"
                               name="remote_work" {{ old('remote_work') ? 'checked' : '' }}>
                        <label for="remote_work">
                            Удаленная работа
                        </label>
                    </div>

                    @error('remote_work')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>



                <div class="form-group form-group__select required @error('schedule') has-error @enderror">
                    <label for="vacancy_schedule">{{ __("ad.vacancy.schedule") }}</label>

                    <select name="schedule" id="vacancy_schedule" class="form-control">
                        @foreach(\App\Models\AdVacancy::getSchedules() as $key => $value)
                            <option {{old('schedule') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('schedule')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('employment') has-error @enderror">
                    <label for="vacancy_employment">{{ __("ad.vacancy.employment") }}</label>

                    <select name="employment" id="vacancy_employment" class="form-control">
                        @foreach(\App\Models\AdVacancy::getEmployments() as $key => $value)
                            <option {{old('employment') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('employment')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('education') has-error @enderror">
                    <label for="education">{{ __("ad.vacancy.education") }}</label>

                    <select name="education" id="education" class="form-control">
                        @foreach(\App\Models\AdVacancy::getEducation() as $key => $value)
                            <option {{old('education') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('education')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__select required @error('experience') has-error @enderror">
                    <label for="experience">{{ __("ad.vacancy.experience") }}</label>

                    <select name="experience" id="experience" class="form-control">
                        @foreach(\App\Models\AdVacancy::getExperiences() as $key => $value)
                            <option {{old('experience') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('experience')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__inline">
                    <div class="form-group__input required @error('salary_from') has-error @enderror">
                        <label for="vacancy_salary_from">{{ __("ad.vacancy.salary_from") }}</label>
                        <input type="text" name="salary_from" id="vacancy_salary_from" class="form-control"
                               value="{{ old('salary_from') }}"
                               placeholder="{{ __("ad.vacancy.salary_from") }}" required>
                        @error('salary_from')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group__input required @error('salary_to') has-error @enderror">
                        <label for="vacancy_salary_to">{{ __("ad.vacancy.salary_to") }}</label>
                        <input type="text" name="salary_to" id="vacancy_salary_to" class="form-control"
                               value="{{ old('salary_to') }}"
                               placeholder="{{ __("ad.vacancy.salary_to") }}" required>
                        @error('salary_to')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group__select required @error('currency') has-error @enderror">
                        <label for="currency">{{ __("ad.vacancy.currency") }}</label>

                        <select name="currency" id="currency" class="form-control">
                            @foreach(\App\Models\Ad::currencies() as $key => $value)
                                <option {{old('currency') == $key ? 'selected' : ''}} value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('currency')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                    <span class="help-block help-block__hint">Указывайте минимальный уровень оплаты, который работник будет гарантированно получать.</span>
                </div>

            </div>

            <div class="form__block">

                <h2>Контактная информация</h2>

                <div class="form-group form-group__checkbox @error('want_register') has-error @enderror">

                    <div class="checkbox">
                        <input type="checkbox" id="want_register"
                               name="want_register" {{ old('want_register') ? 'checked' : '' }}>
                        <label for="want_register">
                            Я хочу зарегистрироваться и пользоваться всеми преимуществами сайта
                        </label>
                    </div>

                    @error('want_register')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('company_name') has-error @enderror">
                    <label for="company_name">{{ __("ad.vacancy.company_name") }}</label>
                    <input type="text" name="company_name" id="company_name"
                           class="form-control"
                           value="{{ old('company_name') }}"
                           placeholder="{{ __("ad.vacancy.company_name") }}" required>
                    @error('company_name')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('address') has-error @enderror">
                    <label for="address">{{ __("ad.vacancy.address") }}</label>
                    <input type="text" name="address" id="address"
                           class="form-control"
                           value="{{ old('address') }}"
                           placeholder="{{ __("ad.vacancy.address") }}" required>
                    @error('address')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('name') has-error @enderror">
                    <label for="name">{{ __("ad.vacancy.name") }}</label>
                    <input type="text" name="name" id="name" class="form-control"
                           value="{{ old('name') }}"
                           placeholder="{{ __("ad.vacancy.name") }}" required>
                    @error('name')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('email') has-error @enderror">
                    <label for="email">{{ __("ad.vacancy.email") }}</label>
                    <input type="text" name="email" id="email" class="form-control"
                           value="{{ old('email') }}"
                           placeholder="{{ __("ad.vacancy.email") }}" required>
                    @error('email')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input required @error('phone') has-error @enderror">
                    <label for="phone">{{ __("ad.vacancy.phone") }}</label>
                    <input type="text" name="phone" id="phone" class="form-control"
                           value="{{ old('phone') }}"
                           placeholder="{{ __("ad.vacancy.phone") }}" required>
                    @error('phone')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group form-group__input @error('photos') has-error @enderror">
                    <label for="photo">{{ __("ad.vacancy.photos") }}</label>
                    <input type="file" multiple="multiple" name="photos[]" accept="image" />
                    @error('photos')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>



            </div>

        </div>

        <div class="form__footer">
            <div class="form-group form-group__btn">
                <button type="submit" class="btn btn-green">
                    {{ __('ad.vacancy.submit') }}
                </button>
            </div>
        </div>
    </form>
@endsection