@php
    /** @var $contact Contact */
    /** @var $token Token */

    use App\Models\Contact;
    use App\Models\Token;
@endphp

@component('mail::message')

{{ __('contact.email.recovery.body', ['url' => route('resetForm', ['key' => $token->value])]) }}

@endcomponent