@php
    /** @var $contact Contact */
    /** @var $token Token */

    use App\Models\Contact;
    use App\Models\Token;
@endphp

@component('mail::message')

{{ __('contact.email.activate.body', ['url' => route('activateEmail', ['key' => $token->value])]) }}

@endcomponent