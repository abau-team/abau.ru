<?php

return [
    'ad_id' => 'Номер объявления',
    'type' => 'Тип',
    'year' => 'Год окончания',
    'name' => 'Название учебного заведения',
    'department' => 'Факультет',
    'specialization' => 'Специализация',

    'types' => [
        'university' => 'Высшее',
        'university-uncompleted' => 'Неоконченное высшее',
        'vocational-school' => 'Среднее-специональное',
        'refresher-course' => 'Курсы повышения квалификации',
    ],
];