<?php

return [
    'ad' => 'Объявление',
    'ad_id' => 'Номер объявления',
    'started_at' => 'Начало работы',
    'current_work' => 'По настоящее время',
    'interval' => 'Период работы',
    'intervalValueNow' => ':date — По настоящее время',
    'intervalValue' => ':from — :to',
    'finished_at' => 'Окончание',
    'organization' => 'Организация',
    'place' => 'Регион',
    'place_id' => 'ID места',
    'site' => 'Сайт компании',
    'job_industry_id' => 'ID индустрии',
    'job_industry' => 'Сфера деятельности компании',
    'position' => 'Должность',
    'duties' => 'Обязанности на рабочем месте',
];