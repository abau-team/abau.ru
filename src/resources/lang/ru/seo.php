<?php
return [
    'welcome' => [
        'title' => 'Работа, поиск работы, свежие вакансии, резюме',
        'description' => 'Сайт о поиске работы. Свежие и актуальные вакансии. Зайди и найди работу мечты.',
        'keywords' => 'работа, вакансии, резюме',
        'breadcrumbs' => '',
    ],
    'register' => [
        'title' => 'Регистрация нового пользователя',
        'description' => 'Страница регистрации нового пользователя сервиса Abau.ru',
        'keywords' => '',
        'breadcrumbs' => 'Регистрация',
    ],
    'login' => [
        'title' => 'Авторизация',
        'description' => 'Страница авторизации пользователя сервиса Abau.ru',
        'keywords' => '',
        'breadcrumbs' => 'Авторизация',
    ],
    'reset' => [
        'title' => 'Сброс пароля',
        'description' => 'Страница сброса пароля сервиса Abau.ru',
        'keywords' => '',
        'breadcrumbs' => 'Сброс пароля',
    ],
    'recovery' => [
        'title' => 'Восстановление пароля',
        'description' => 'Страница восстановления пароля сервиса Abau.ru',
        'keywords' => '',
        'breadcrumbs' => 'Восстановление пароля',
    ],
    'term' => [
        'index' => [
            'title' => 'Правила сайта Abau.ru',
            'description' => 'Используя наш сервис вы полностью соглашаетесь с данными правилами.',
            'keywords' => '',
            'breadcrumbs' => 'Правила сайта',
        ],
    ],
    'ad' => [
        'create' => [
            'index' => [
                'title' => 'Подать бесплатное объявление в :city_name',
                'description' => 'Бесплатно подать объявление о работе, создать резюме или разместить вакансию',
                'keywords' => '',
                'breadcrumbs' => 'Подать объявление',
            ],
            'resume' => [
                'title' => 'Создать резюме бесплатно в :city_name',
                'description' => 'Создать резюме ',
                'keywords' => '',
                'breadcrumbs' => 'Резюме',
            ],
        ],
        'search' => [
            'index' => [
                'title' => 'Доска бесплатных объявлений :city_name',
                'description' => 'Abau.ru :city_name - Свежие объявления, актуальные вакансии и резюме.',
                'keywords' => 'объявления :city_name, доска объявлений',
                'breadcrumbs' => ':city_name',
            ],
        ],
    ]
];
?>