<?php

return [
    'types' => [
        'activate' => 'Активации',
        'recovery' => 'Восстановления',
    ],
    'field' => [
        'contact_id' => 'ID Контакта',
        'user_id' => 'ID пользователя',
        'type' => 'Тип токена',
        'type_label' => 'Тип токена',
        'value' => 'Токен',
        'created_at' => 'Дата создания'
    ],
    'admin' => [
        'showListByContact' => 'Показать все токены контакта',
        'showListByUser' => 'Показать все токены пользователя',
        'delete' => 'Удалить токен',
        'listAll' => 'Список токенов',
        'listByContact' => 'Список токенов контакта № :id',
        'listByUser' => 'Список токенов пользователя № :id',
    ],
];