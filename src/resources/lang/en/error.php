<?php

return [
    401 => [
        'title' => 'Error #401 - Unauthorized',
        'header' => 'Error #401',
        'message' => 'Unauthorized'
    ],
    403 => [
        'title' => 'Error #403 - Forbidden',
        'header' => 'Error #403',
        'message' => 'Forbidden'
    ],
    404 => [
        'title' => 'Error #404 - Page Not Found',
        'header' => 'Error #404',
        'message' => 'The requested page not found'
    ],
    419 => [
        'title' => 'Error #419 - Page Expired',
        'header' => 'Error #419',
        'message' => 'The requested page expired'
    ],
    429 => [
        'title' => 'Error #429 - Too many request',
        'header' => 'Error #419',
        'message' => 'Too many request to our server'
    ],
    500 => [
        'title' => 'Error #500 - Server Error',
        'header' => 'Error #500',
        'message' => 'Server error'
    ],
    503 => [
        'title' => 'Error #503 - Service Unavailable',
        'header' => 'Error #503',
        'message' => 'Service Unavailable'
    ],
];