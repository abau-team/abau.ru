<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;

trait TraitCheckLocalization {
    /**
     * Check the view has not translate strings
     *
     * @param TestResponse $response
     */
    protected function dontSeeLocaleString(TestResponse $response)
    {
        //$response->assertDontSeeText('admin.');
        //$response->assertDontSeeText('ad.');
        //$response->assertDontSeeText('ad-category.');
        //$response->assertDontSeeText('job-industry.');
        //$response->assertDontSeeText('job-professional-area.');
        //$response->assertDontSeeText('contact.');
        //$response->assertDontSeeText('general.');
        //$response->assertDontSeeText('place.');
        $response->assertDontSeeText('seo.');
        $response->assertDontSeeText('token.');
        $response->assertDontSeeText('user.');
        $response->assertDontSeeText('validation.');

    }
}