<?php

namespace Tests\Feature;

use Tests\TestCase;

class TermsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHelpIndexPageTest()
    {
        $response = $this->get(route('term'));
        $response->assertStatus(200);

        $response->assertSeeTextInOrder([
            __('seo.term.index.title'),
        ]);

        $response->assertViewIs('term.index');
    }
}
