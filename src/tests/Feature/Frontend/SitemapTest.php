<?php

namespace Tests\Feature;

use Tests\TestCase;

class SitemapTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHelpIndexPageTest()
    {
        $response = $this->get(route('sitemap.xml'));
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/xml; charset=UTF-8');

        $response->assertViewIs('sitemap.xml');
        $response->assertViewHas('pages');
    }
}
