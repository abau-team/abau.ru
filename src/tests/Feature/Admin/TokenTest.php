<?php

namespace Tests\Feature\Admin;

use App\Models\Contact;
use App\Models\Token;
use App\Models\User;
use DatabaseSeeder;
use Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\TraitCheckLocalization;
use Tests\TestCase;

class TokenTest extends TestCase {

    use WithFaker, TraitCheckLocalization, TraitAdminMenu;

    protected function createUser() {
        return User::create([
            'name' => $this->faker->firstName,
            'password' => Hash::make('secret'),
        ]);
    }

    /**
     * @var User $user
     *
     * @return Contact|Model
     */
    protected function createContactEmail($user)
    {
        return $user->contacts()->create([
            'name' => $this->faker->firstName,
            'type' => Contact::TYPE_PHONE,
            'value' => $this->faker->regexify('\+[0-9]{1,4}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}'),
        ])->refresh();
    }

    /**
     * @var User $user
     *
     * @return Contact|Model
     */
    protected function createContactPhone($user) {
        return $user->contacts()->create([
            'name' => $this->faker->firstName,
            'type' => Contact::TYPE_EMAIL,
            'value' => $this->faker->email,
        ])->refresh();
    }


    public function testSetDb() {
        $this->artisan('migrate:fresh');
        $seeder = new DatabaseSeeder();
        $seeder->run();
        $this->assertTrue(true);
    }

    public function testListAll()
    {

        $response = $this->get(route('admin.token.listAll'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('token.admin.listAll')
        ]);

        $response->assertViewIs("admin.token.listAll");

        $response->assertViewHasAll(['models']);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }



    public function testListByUser()
    {
        $user = $this->createUser();
        $contactEmail = $this->createContactEmail($user);
        $contactEmail->createActivationToken();
        $contactEmail->createRecoveryToken();
        $contactPhone = $this->createContactPhone($user);
        $contactPhone->createRecoveryToken();
        $contactPhone->createActivationToken();

        $response = $this->get(route('admin.token.listByUser', ['id' => $user->user_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('token.admin.listByUser', ['id' => $user->user_id])
        ]);

        $response->assertViewIs("admin.token.listByUser");

        /*$models = Token::leftJoin('contacts', 'tokens.contact_id', '=', 'contacts.contact_id')
            ->where(['contacts.user_id' => $user->user_id])->paginate(50);*/

        $response->assertViewHasAll([
            'user' => $user->refresh(),
            'models'
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testListByContact() {
        $user = $this->createUser();
        $contactEmail = $this->createContactEmail($user);
        $contactEmail->createActivationToken();

        $response = $this->get(route('admin.token.listByContact', ['id' => $user->user_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('token.admin.listByContact', ['id' => $user->user_id])
        ]);

        $response->assertViewIs("admin.token.listByContact");

        /*$response->assertViewHasAll([
            'contact' => $contactEmail->refresh(),
            //'models'
        ]);*/

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testDestroy()
    {
        $user = $this->createUser();
        $contactEmail = $this->createContactEmail($user);
        $token = $contactEmail->createActivationToken();

        $this->get(route('admin.token.destroy', ['type' => $token->type, 'contact' => '99999999e99999']))->assertStatus(404);

        $this->get(route('admin.token.destroy', ['type' => $token->type, 'value' => $token->value]))
            ->assertRedirect(route('admin.token.listAll'))->assertSessionHas('success');

        $this->assertNull(Token::whereContactId($contactEmail->contact_id)->first());

    }

}