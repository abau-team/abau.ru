<?php

namespace Tests\Feature\Admin;

use App\Models\Contact;
use App\Models\User;
use DatabaseSeeder;
use Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\TraitCheckLocalization;
use Tests\TestCase;

class ContactTest extends TestCase
{

    use TraitAdminMenu, TraitCheckLocalization;

    use WithFaker;

    protected function createUser()
    {
        return User::create([
            'name' => $this->faker->firstName,
            'password' => Hash::make('secret'),
        ]);
    }

    /**
     * @return Contact
     */
    protected function createContact()
    {
        return $this->createUser()->contacts()->create([
            'name' => $this->faker->firstName,
            'type' => Contact::TYPE_EMAIL,
            'value' => $this->faker->email
        ])->refresh();
    }

    public function testSetDb()
    {
        $this->artisan('migrate:fresh');
        $seeder = new DatabaseSeeder();
        $seeder->run();
        $this->assertTrue(true);
    }

    public function testListActive()
    {
        $response = $this->get(route('admin.contact.listActive'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('contact.admin.listActive')
        ]);

        $response->assertViewIs("admin.contact.listActive");

        $response->assertViewHasAll(['models']);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testListAll()
    {

        $response = $this->get(route('admin.contact.listAll'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('contact.admin.listAll')
        ]);

        $response->assertViewIs("admin.contact.listAll");

        $response->assertViewHasAll(['models']);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }


    public function testListUnActivated()
    {

        $response = $this->get(route('admin.contact.listUnActivated'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('contact.admin.listUnActivated')
        ]);

        $response->assertViewIs("admin.contact.listUnActivated");

        $response->assertViewHasAll(['models']);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testListBanned()
    {

        $response = $this->get(route('admin.contact.listBanned'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('contact.admin.listBanned')
        ]);

        $response->assertViewIs("admin.contact.listBanned");

        $response->assertViewHasAll(['models']);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }


    public function testListDeleted()
    {

        $response = $this->get(route('admin.contact.listDeleted'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('contact.admin.listDeleted')
        ]);

        $response->assertViewIs("admin.contact.listDeleted");

        $response->assertViewHasAll(['models']);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testListByUser()
    {

        $first = $this->createContact()->refresh();

        $response = $this->get(route('admin.contact.listByUser', ['id' => $first->user_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('contact.admin.listByUser', ['id' => $first->user_id])
        ]);

        $response->assertViewIs("admin.contact.listByUser");

        $response->assertViewHasAll([
            'models',
            'user' => $first->user,
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testCreateForm()
    {
        $user = $this->createUser();

        $response = $this->get(route('admin.contact.createForm', ['id' => $user->user_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('contact.admin.createForm', ['id' => $user->user_id])
        ]);

        $response->assertViewIs("admin.contact.createForm");

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="type"',
            'name="value"',
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Проверка контроллера сохранения
     */
    public function testCreate()
    {
        $user = $this->createUser();
        $value = $this->faker->email;

        // Проверка пустых полей - Ошибка валидации
        $this->post(route('admin.contact.create', ['id' => $user->user_id]))
            ->assertStatus(302)
            ->assertSessionHasErrors(['type', 'value']);

        // Проверка заполненных значений - Валидация успешная
        $this->post(route('admin.contact.create', ['id' => $user->user_id]), [
            'name' => $this->faker->firstName,
            'type' => Contact::TYPE_EMAIL,
            'value' => $value
        ])->assertStatus(302)
            ->assertRedirect(route('admin.contact.listActive'))
            ->assertSessionHasNoErrors();

        $contact = Contact::first();

        $this->assertTrue($contact->value == $value);
        $this->assertEmpty($contact->deleted_at);
        $this->assertEmpty($contact->blocked_at);
        $this->assertEmpty($contact->activated_at);

    }

    public function testShow()
    {
        $first = $this->createContact();

        $this->get(route('admin.contact.show', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.contact.show', ['id' => $first->contact_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('contact.admin.show', ['id' => $first->contact_id])
        ]);

        $response->assertViewIs("admin.contact.show");

        $response->assertViewHasAll(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            "<td>{$first->contact_id}</td>",
            "<td>{$first->user_id}</td>",
            "<td>{$first->value}</td>",
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testUpdateForm()
    {
        $first = $this->createContact();

        $this->get(route('admin.contact.updateForm', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.contact.updateForm', ['id' => $first->contact_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('contact.admin.updateForm', ['id' => $first->contact_id])
        ]);

        $response->assertViewIs("admin.contact.updateForm");

        $response->assertViewHasAll(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="type"',
            'name="value"',
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Проверка контролера редактирования
     */
    public function testUpdate()
    {
        $first = $this->createContact();

        // Проверка пустых полей - Ошибка валидации
        $this->put(route('admin.contact.update', ['id' => $first->contact_id]))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name']);

        // Проверка заполненных значений - Валидация успешная
        $this->put(route('admin.contact.update', ['id' => $first->contact_id]), [
            'name' => $this->faker->firstName,
            'type' => Contact::TYPE_EMAIL,
            'value' => $this->faker->email,
        ])->assertStatus(302)
            ->assertRedirect(route('admin.contact.listActive'))
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $result = Contact::find($first->contact_id);
        $this->assertTrue($first->value != $result->value);
    }

    public function testDelete()
    {
        $first = $this->createContact();

        $this->assertEmpty($first->deleted_at);

        $this->get(route('admin.contact.delete', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.contact.delete', ['id' => $first->contact_id]))
            ->assertRedirect(route('admin.contact.listActive'))->assertSessionHas('success');

        $first = Contact::find($first->contact_id);

        $this->assertNotNull($first->deleted_at);

    }

    public function testUnDelete()
    {
        $first = $this->createContact();
        $first->markDeleted();
        $first->refresh();

        $this->assertNotNull($first->deleted_at);

        $this->get(route('admin.contact.unDelete', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.contact.unDelete', ['id' => $first->contact_id]))
            ->assertRedirect(route('admin.contact.listActive'))->assertSessionHas('success');

        $first->refresh();

        $this->assertNull($first->deleted_at);

    }

    public function testDestroy()
    {
        $first = $this->createContact();

        $this->get(route('admin.contact.destroy', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.contact.destroy', ['id' => $first->contact_id]))
            ->assertRedirect(route('admin.contact.listActive'))->assertSessionHas('success');

        $this->assertNull(Contact::withoutGlobalScopes()->find($first->contact_id));

    }

    public function testBan()
    {
        $first = $this->createContact();

        $this->assertNull($first->blocked_at);

        $this->get(route('admin.contact.ban', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.contact.ban', ['id' => $first->contact_id]))
            ->assertRedirect(route('admin.contact.listActive'))->assertSessionHas('success');

        $first->refresh();

        $this->assertNotNull($first->blocked_at);

    }

    public function testUnBan()
    {
        $first = $this->createContact();

        $first->ban();

        $first->refresh();

        $this->assertNotNull($first->blocked_at);

        $this->get(route('admin.contact.unBan', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.contact.unBan', ['id' => $first->contact_id]))
            ->assertRedirect(route('admin.contact.listActive'))->assertSessionHas('success');

        $first->refresh();

        $this->assertNull($first->blocked_at);

    }

    public function testActivate()
    {
        $first = $this->createContact();

        $this->assertNull($first->activated_at);

        $this->get(route('admin.contact.activate', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.contact.activate', ['id' => $first->contact_id]))
            ->assertRedirect(route('admin.contact.listActive'))->assertSessionHas('success');

        $first->refresh();

        $this->assertNotNull($first->activated_at);

    }

    public function testDeactivate()
    {
        $first = $this->createContact();

        $first->activate();

        $first->refresh();

        $this->assertNotNull($first->activated_at);

        $this->get(route('admin.contact.deactivate', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.contact.deactivate', ['id' => $first->contact_id]))
            ->assertRedirect(route('admin.contact.listActive'))->assertSessionHas('success');

        $first->refresh();

        $this->assertNull($first->activated_at);

    }

}