<?php

namespace Tests\Feature\Admin;

use Tests\Feature\TraitCheckLocalization;
use Tests\TestCase;

class AdminDashboardTest extends TestCase
{

    use TraitAdminMenu, TraitCheckLocalization;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndexPage()
    {
        $response = $this->get(route('admin.home'));

        $response->assertStatus(200);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testClearCache() {
        $response = $this->get(route('admin.clearCache'));
        $response->assertRedirect(route('admin.home'))
            ->assertSessionHasNoErrors()->assertSessionHas('success', __('admin.flash.cache-s'));
    }
}
