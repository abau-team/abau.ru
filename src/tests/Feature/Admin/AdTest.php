<?php

namespace Tests\Feature\Admin;

use DatabaseSeeder;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdTest extends TestCase
{
    use WithFaker;

    public function testDb()
    {
        $this->artisan('migrate:fresh');
        $seeder = new DatabaseSeeder();
        $seeder->run();
        $this->assertTrue(true);
    }

    public function testList()
    {
        $response = $this->get(route('admin.ad.listAll'));
        $response->assertSuccessful();
        $response->assertViewIs('admin.ad.listAll');
    }

    public function testDataTables()
    {
        $response = $this->get( 'http://abau.test/voito/ads/index?status=all&draw=2&columns%5B0%5D%5Bdata%5D=ad_id&columns%5B0%5D%5Bname%5D=ads.ad_id&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=user_id&columns%5B1%5D%5Bname%5D=ads.user_id&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=phone&columns%5B2%5D%5Bname%5D=ads.phone&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=view_contact&columns%5B3%5D%5Bname%5D=ads.view_contact&columns%5B3%5D%5Bsearchable%5D=false&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=ad_category_id&columns%5B4%5D%5Bname%5D=ads.ad_category_id&columns%5B4%5D%5Bsearchable%5D=false&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=place&columns%5B5%5D%5Bname%5D=place.name&columns%5B5%5D%5Bsearchable%5D=false&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=created_at&columns%5B6%5D%5Bname%5D=ads.created_at&columns%5B6%5D%5Bsearchable%5D=false&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=refreshed_at&columns%5B7%5D%5Bname%5D=ads.refreshed_at&columns%5B7%5D%5Bsearchable%5D=false&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=blocked_at&columns%5B8%5D%5Bname%5D=ads.blocked_at&columns%5B8%5D%5Bsearchable%5D=false&columns%5B8%5D%5Borderable%5D=true&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=deleted_at&columns%5B9%5D%5Bname%5D=ads.deleted_at&columns%5B9%5D%5Bsearchable%5D=false&columns%5B9%5D%5Borderable%5D=true&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=buttons&columns%5B10%5D%5Bname%5D=buttons&columns%5B10%5D%5Bsearchable%5D=false&columns%5B10%5D%5Borderable%5D=false&columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=1&order%5B0%5D%5Bdir%5D=asc&start=0&length=50&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1559223849457', ['HTTP_X-Requested-With' => 'XMLHttpRequest']);
        $response->assertSuccessful();
    }

    public function testCreateForm() {
        $response = $this->get(route('admin.ad.createForm'));
        $response->assertSuccessful();
        $response->assertViewIs('admin.ad.createForm');
        // Проверка наличия полей формы
        /*$response->assertSeeInOrder([
            'name="title"',
            'name="description"',
            'name="price"',
            'name="category"',
            'name="user_id"'
        ]);*/
    }

    /*public function testCreateFormForUser() {
        $response = $this->get(route('admin.ad.createForm'));
        $response->assertSuccessful();
        $response->assertViewIs('admin.ad.createForm');
        $response->assertSeeInOrder([
            'name="title"',
            'name="description"',
            'name="price"',
            'name="category"'
        ]);
    }

    public function testCreate() {
        // Проверка пустых полей - Ошибка валидации
        $this->post(route('admin.ad.create'))
            ->assertStatus(302)
            ->assertSessionHasErrors(['price', 'category']);

        // Проверка заполненных значений - Валидация успешная
        $this->post(route('admin.ad.create'), [
            //'name' => $this->faker->city,
            //'code' => $this->faker->regexify('[a-zA-Z0-9]{25}'),
        ])->assertStatus(302)
            ->assertRedirect(route('admin.ad.list'))
            ->assertSessionHasNoErrors();
    }

    public function testCreateForUser() {
        // Проверка пустых полей - Ошибка валидации
        $this->post(route('admin.ad.createForUser'))
            ->assertStatus(302)
            ->assertSessionHasErrors(['price', 'category']);

        // Проверка заполненных значений - Валидация успешная
        $this->post(route('admin.ad.createForUser'), [
            //'name' => $this->faker->city,
            //'code' => $this->faker->regexify('[a-zA-Z0-9]{25}'),
        ])->assertStatus(302)
            ->assertRedirect(route('admin.ad.listByUser'))
            ->assertSessionHasNoErrors();
    }*/

}