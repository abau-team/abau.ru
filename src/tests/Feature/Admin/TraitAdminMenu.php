<?php

namespace Tests\Feature\Admin;

use Illuminate\Foundation\Testing\TestResponse;

trait TraitAdminMenu {

    /**
     * Проверка отображения пунктов меню
     * @param TestResponse $response
     */
    protected function seeAdminMenu(TestResponse $response)
    {
        $response->assertSeeTextInOrder([
            __('admin.menu.dashboard'),
            __('admin.menu.ads'),
            //__('admin.menu.ads-active'),
            //__('admin.menu.ads-inactive'),
            //__('admin.menu.ads-banned'),
            __('admin.menu.places'),
            __('admin.menu.lists'),
            __('admin.menu.ad-categories'),
            __('admin.menu.work'),
            __('admin.menu.job-professional-areas'),
            __('admin.menu.job-industries'),
            __('admin.menu.users'),
            __('admin.menu.contacts'),
            __('admin.menu.tokens'),
            __('admin.menu.clear-cache'),
        ]);
    }

}