<?php

namespace Tests\Feature\Admin;

use App\Models\Place;
use DatabaseSeeder;
use Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\TraitCheckLocalization;
use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{

    use TraitAdminMenu, TraitCheckLocalization;

    use WithFaker;

    public function testSetDb()
    {
        $this->artisan('migrate:fresh');
        $seeder = new DatabaseSeeder();
        $seeder->run();
        $this->assertTrue(true);
    }


    /**
     * @return User
     */
    protected function createUser()
    {
        return User::create([
            'name' => $this->faker->firstName,
            'password' => Hash::make('secret'),
        ]);
    }


    public function testIndex()
    {

        $response = $this->get(route('admin.user.listActive'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('user.admin.listActive')
        ]);

        $response->assertViewIs("admin.user.listActive");

        $response->assertViewHasAll(['models']);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testIndexAll()
    {

        $response = $this->get(route('admin.user.listAll'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('user.admin.listAll')
        ]);

        $response->assertViewIs("admin.user.listAll");

        $response->assertViewHasAll(['models']);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testIndexBanned()
    {

        $response = $this->get(route('admin.user.listBanned'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('user.admin.listBanned')
        ]);

        $response->assertViewIs("admin.user.listBanned");

        $response->assertViewHasAll(['models']);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }


    public function testIndexDeletedPage()
    {

        $response = $this->get(route('admin.user.listDeleted'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('user.admin.listDeleted')
        ]);

        $response->assertViewIs("admin.user.listDeleted");

        $response->assertViewHas('models');

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testCreateForm()
    {
        $response = $this->get(route('admin.user.createForm'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("user.admin.createForm"),
        ]);

        $response->assertViewIs("admin.user.createForm");

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="name"',
            'name="place_id"',
            'name="password"',
            'name="password_confirmation"',
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Проверка контроллера сохранения
     */
    public function testCreate()
    {

        $name = $this->faker->firstName;
        $place = Place::where(['type' => Place::TYPE_CITY])->first();

        // Проверка пустых полей - Ошибка валидации
        $this->post(route('admin.user.create'))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name', 'password']);

        // Проверка заполненных значений - Валидация успешная
        $this->post(route('admin.user.create'), [
            'name' => $name,
            'place_id' => $place->place_id,
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ])->assertStatus(302)
            ->assertRedirect(route('admin.user.listActive'))
            ->assertSessionHasNoErrors();

        $user = User::first();

        $this->assertTrue($user->name == $name);
        $this->assertEmpty($user->deleted_at);
        $this->assertEmpty($user->blocked_at);

    }

    public function testShow()
    {
        $first = $this->createUser();

        $this->get(route('admin.user.show', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.user.show', ['id' => $first->user_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('user.admin.show', ['id' => $first->user_id])
        ]);

        $response->assertViewIs("admin.user.show");

        $response->assertViewHasAll(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            "<td>{$first->user_id}</td>",
            "<td>{$first->name}</td>"
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testUpdateForm()
    {
        $first = $this->createUser();

        $this->get(route('admin.user.updateForm', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.user.updateForm', ['id' => $first->user_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("user.admin.updateForm", ['id' => $first->user_id])
        ]);

        $response->assertViewIs("admin.user.updateForm");

        $response->assertViewHasAll(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="name"',
            'name="place_id"',
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Проверка контролера редактирования
     */
    public function testUpdate()
    {
        $first = $this->createUser();
        $place = Place::where(['type' => Place::TYPE_CITY])->first();

        // Проверка пустых полей - Ошибка валидации
        $this->put(route('admin.user.update', ['id' => $first->user_id]))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name']);

        // Проверка заполненных значений - Валидация успешная
        $this->put(route('admin.user.update', ['id' => $first->user_id]), [
            'name' => $this->faker->firstName,
            'place_id' => $place->place_id,
        ])->assertStatus(302)
            ->assertRedirect(route('admin.user.listActive'))
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $result = User::find($first->user_id);
        $this->assertTrue($first->name != $result->name && $first->place_id != $result->place_id);
    }

    public function testDelete()
    {
        $first = $this->createUser();

        $this->assertEmpty($first->deleted_at);

        $this->get(route('admin.user.delete', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.user.delete', ['id' => $first->user_id]))
            ->assertRedirect(route('admin.user.listActive'))->assertSessionHas('success');

        $first = User::find($first->user_id);

        $this->assertNotNull($first->deleted_at);

    }

    public function testUnDelete()
    {
        $first = $this->createUser();
        $first->markDeleted();
        $first->refresh();

        $this->assertNotNull($first->deleted_at);

        $this->get(route('admin.user.unDelete', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.user.unDelete', ['id' => $first->user_id]))
            ->assertRedirect(route('admin.user.listActive'))->assertSessionHas('success');

        $first->refresh();

        $this->assertNull($first->deleted_at);

    }

    public function testDestroy()
    {
        $first = $this->createUser();

        $this->get(route('admin.user.destroy', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.user.destroy', ['id' => $first->user_id]))
            ->assertRedirect(route('admin.user.listActive'))->assertSessionHas('success');

        $this->assertNull(User::find($first->user_id));

    }

    public function testBan()
    {
        $first = $this->createUser();

        $this->assertNull($first->blocked_at);

        $this->get(route('admin.user.ban', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.user.ban', ['id' => $first->user_id]))
            ->assertRedirect(route('admin.user.listActive'))->assertSessionHas('success');

        $first->refresh();

        $this->assertNotNull($first->blocked_at);

    }

    public function testUnBan()
    {
        $first = $this->createUser();

        $first->ban();

        $first->refresh();

        $this->assertNotNull($first->blocked_at);

        $this->get(route('admin.user.unBan', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.user.unBan', ['id' => $first->user_id]))
            ->assertRedirect(route('admin.user.listActive'))->assertSessionHas('success');

        $first->refresh();

        $this->assertNull($first->blocked_at);

    }

    public function testSetPasswordForm()
    {
        $first = $this->createUser();

        $this->get(route('admin.user.passwordForm', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.user.passwordForm', ['id' => $first->user_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("user.admin.passwordForm", ['id' => $first->user_id])
        ]);

        $response->assertViewIs("admin.user.passwordForm");

        $response->assertViewHasAll(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="password"',
            'name="password_confirmation"',
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Проверка контролера редактирования
     */
    public function testSetPassword()
    {
        $first = $this->createUser();

        // Проверка пустых полей - Ошибка валидации
        $this->put(route('admin.user.password', ['id' => $first->user_id]))
            ->assertStatus(302)
            ->assertSessionHasErrors(['password']);

        // Проверка заполненных значений - Валидация успешная
        $this->put(route('admin.user.password', ['id' => $first->user_id]), [
            'password' => 'another_secret',
            'password_confirmation' => 'another_secret'
        ])->assertStatus(302)
            ->assertRedirect(route('admin.user.listActive'))
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $first->refresh();
        $this->assertTrue($first->checkPassword('another_secret'));
    }

}