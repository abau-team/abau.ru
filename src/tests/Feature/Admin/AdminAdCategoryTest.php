<?php

namespace Tests\Feature\Admin;

use App\Models\AdCategory;
use Tests\Feature\TraitCheckLocalization;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminAdCategoryTest extends TestCase
{

    use WithFaker, RefreshDatabase, TraitAdminMenu, TraitCheckLocalization;

    /**
     * @return AdCategory
     */
    protected function createAdCategory() {
        return AdCategory::create([
            'name' => $this->faker->city,
            'code' => $this->faker->regexify('[a-zA-Z0-9]{25}'),
        ]);
    }

    /**
     * Проверка Страница списка
     */
    public function testListAll()
    {
        $this->createAdCategory();
        $this->createAdCategory();
        $this->createAdCategory();
        $this->createAdCategory();

        $response = $this->get(route('admin.ad-category.listAll'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('ad-category.admin.listAll')
        ]);

        $response->assertViewIs("admin.nested-list.listAll");

        $response->assertViewHas(['models' => AdCategory::defaultOrder()->get()->toTree()]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Test index page
     *
     * @return void
     */
    public function testCreateForm()
    {
        $response = $this->get(route('admin.ad-category.createForm'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("ad-category.admin.createForm"),
        ]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="name"',
            'name="code"',
            'name="active"'
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Проверка контроллера сохранения
     */
    public function testCreate()
    {

        // Проверка пустых полей - Ошибка валидации
        $this->post(route('admin.ad-category.create'))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name', 'code']);

        // Проверка заполненных значений - Валидация успешная
        $this->post(route('admin.ad-category.create'), [
            'name' => $this->faker->city,
            'code' => $this->faker->regexify('[a-zA-Z0-9]{25}'),
        ])->assertStatus(302)
            ->assertRedirect(route('admin.ad-category.listAll'))
            ->assertSessionHasNoErrors();


    }

    /**
     * Проверка контролера редактирования
     */
    public function testUpdate()
    {
        $first = $this->createAdCategory();

        // Проверка пустых полей - Ошибка валидации
        $this->put(route('admin.ad-category.update', ['id' => $first->ad_category_id]))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name', 'code']);

        // Проверка заполненных значений - Валидация успешная
        $this->put(route('admin.ad-category.update', ['id' => $first->ad_category_id]), [
            'name' => $this->faker->city,
            'code' => $this->faker->uuid,
        ])->assertStatus(302)
            ->assertRedirect(route('admin.ad-category.listAll'))
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $result = AdCategory::find($first->ad_category_id);
        $this->assertFalse($first == $result);
    }


    public function testMove()
    {
        $first = $this->createAdCategory();

        $second = $this->createAdCategory();

        $this->post(route('admin.ad-category.move', ['id' => $first->ad_category_id]))->assertSessionHasErrors(['parent_id']);

        $this->post(route('admin.ad-category.move', ['id' => 9999999]))->assertStatus(404);

        $this->post(route('admin.ad-category.move', ['id' => $first->ad_category_id]), ['parent_id' => 9999999])->assertSessionHasErrors(['parent_id']);

        $this->post(route('admin.ad-category.move', ['id' => $first->ad_category_id]), ['parent_id' => $second->ad_category_id])
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $first = AdCategory::find($first->ad_category_id);
        $second = AdCategory::find($second->ad_category_id);

        $this->assertTrue($second->children[0] == $first);

    }

    public function testShow()
    {
        $first = $this->createAdCategory();

        $this->get(route('admin.ad-category.show', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.ad-category.show', ['id' => $first->ad_category_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("ad-category.admin.show", ['name' => $first->local_name])
        ]);

        $response->assertViewIs("admin.nested-list.show");

        $response->assertViewHas(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            "<td>{$first->ad_category_id}</td>",
            "<td>{$first->local_name}</td>",
            "<td>{$first->code}</td>"
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testUpdateForm()
    {
        $first = $this->createAdCategory();

        $this->get(route('admin.ad-category.updateForm', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.ad-category.updateForm', ['id' => $first->ad_category_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("ad-category.admin.updateForm", ['name' => $first->local_name])
        ]);

        $response->assertViewIs("admin.nested-list.updateForm");

        $response->assertViewHas(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="name"',
            'name="code"',
            'name="active"'
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testDestroy() {
        $first = $this->createAdCategory();

        $this->get(route('admin.ad-category.destroy', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.ad-category.destroy', ['id' => $first->ad_category_id]))
            ->assertRedirect(route('admin.ad-category.listAll'))->assertSessionHas('success');

        $this->assertNull(AdCategory::find($first->ad_category_id));

    }

    public function testShiftUp() {
        $first = $this->createAdCategory();
        $second = $this->createAdCategory();
        $third = $this->createAdCategory();
        $first->appendNode($second);
        $first->appendNode($third);

        $this->get(route('admin.ad-category.up', ['id' => $third->ad_category_id]))
            ->assertRedirect()->assertSessionHas('success');


        $this->get(route('admin.ad-category.up', ['id' => $third->ad_category_id]))
            ->assertRedirect()->assertSessionHas('error');
    }

    public function testShiftDown() {
        $first = $this->createAdCategory();
        $this->createAdCategory();

        $this->get(route('admin.ad-category.down', ['id' => $first->ad_category_id]))
            ->assertRedirect()->assertSessionHas('success');


        $this->get(route('admin.ad-category.down', ['id' => $first->ad_category_id]))
            ->assertRedirect()->assertSessionHas('error');
    }


    public function testActive() {
        $first = $this->createAdCategory();

        $this->get(route('admin.ad-category.active', ['id' => $first->ad_category_id]))
            ->assertRedirect()->assertSessionHas('success');

        $second = AdCategory::find($first->ad_category_id);

        $this->assertTrue($second->active == 1);
    }

    public function testUnActive() {
        $first = $this->createAdCategory();

        $this->get(route('admin.ad-category.un-active', ['id' => $first->ad_category_id]))
            ->assertRedirect()->assertSessionHas('success');

        $second = AdCategory::find($first->ad_category_id);

        $this->assertTrue($second->active == 0);
    }

    public function testMakeRoot() {
        $first = $this->createAdCategory();
        $second = $this->createAdCategory();
        $first->appendNode($second);

        $first = AdCategory::find($first->ad_category_id);
        $second = AdCategory::find($second->ad_category_id);

        $this->assertTrue($second->parent == $first);

        $this->get(route('admin.ad-category.make-root', ['id' => $second->ad_category_id]))
            ->assertRedirect()->assertSessionHas('success');

        $second = AdCategory::find($second->ad_category_id);
        $this->assertNull($second->parent);
    }

    public function testMoveForm() {
        $first = $this->createAdCategory();

        $response = $this->get(route('admin.ad-category.moveForm', ['id' => $first->ad_category_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("ad-category.admin.moveForm", ['name' => $first->local_name])
        ]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="parent_id"',
        ]);

        $response->assertViewIs("admin.nested-list.moveForm");

        $response->assertViewHas([
            'model' => $first,
            'data' => AdCategory::dataForDropdown($first->ad_category_id)
        ]);

    }


}
