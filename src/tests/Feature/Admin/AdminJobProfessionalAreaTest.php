<?php

namespace Tests\Feature\Admin;

use App\Models\JobProfessionalArea;
use Tests\Feature\TraitCheckLocalization;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminJobProfessionalAreaTest extends TestCase
{

    use WithFaker, RefreshDatabase, TraitAdminMenu, TraitCheckLocalization;

    /**
     * @return JobProfessionalArea
     */
    protected function createJobProfessionalArea() {
        return JobProfessionalArea::create([
            'name' => $this->faker->city,
            'code' => $this->faker->regexify('[a-zA-Z0-9]{25}'),
        ]);
    }

    /**
     * Проверка Страница списка
     */
    public function testListAll()
    {
        $this->createJobProfessionalArea();
        $this->createJobProfessionalArea();
        $this->createJobProfessionalArea();
        $this->createJobProfessionalArea();

        $response = $this->get(route('admin.job-professional-area.listAll'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('job-professional-area.admin.listAll')
        ]);

        $response->assertViewIs('admin.nested-list.listAll');

        $response->assertViewHas(['models' => JobProfessionalArea::defaultOrder()->get()->toTree()]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Test index page
     *
     * @return void
     */
    public function testCreateForm()
    {
        $response = $this->get(route('admin.job-professional-area.createForm'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('job-professional-area.admin.createForm'),
        ]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="name"',
            'name="code"',
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Проверка контроллера сохранения
     */
    public function testCreate()
    {

        // Проверка пустых полей - Ошибка валидации
        $this->post(route('admin.job-professional-area.create'))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name', 'code']);

        // Проверка заполненных значений - Валидация успешная
        $this->post(route('admin.job-professional-area.create'), [
            'name' => $this->faker->city,
            'code' => $this->faker->regexify('[a-zA-Z0-9]{25}'),
        ])->assertStatus(302)
            ->assertRedirect(route('admin.job-professional-area.listAll'))
            ->assertSessionHasNoErrors();


    }

    /**
     * Проверка контролера редактирования
     */
    public function testUpdate()
    {
        $first = $this->createJobProfessionalArea();

        // Проверка пустых полей - Ошибка валидации
        $this->put(route('admin.job-professional-area.update', ['id' => $first->job_professional_area_id]))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name', 'code']);

        // Проверка заполненных значений - Валидация успешная
        $this->put(route('admin.job-professional-area.update', ['id' => $first->job_professional_area_id]), [
            'name' => $this->faker->city,
            'code' => $this->faker->uuid,
        ])->assertStatus(302)
            ->assertRedirect(route('admin.job-professional-area.listAll'))
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $result = JobProfessionalArea::find($first->job_professional_area_id);
        $this->assertFalse($first == $result);
    }


    public function testMove()
    {
        $first = $this->createJobProfessionalArea();

        $second = $this->createJobProfessionalArea();

        $this->post(route('admin.job-professional-area.move', ['id' => $first->job_professional_area_id]))->assertSessionHasErrors(['parent_id']);

        $this->post(route('admin.job-professional-area.move', ['id' => 9999999]))->assertStatus(404);

        $this->post(route('admin.job-professional-area.move', ['id' => $first->job_professional_area_id]), ['parent_id' => 9999999])->assertSessionHasErrors(['parent_id']);

        $this->post(route('admin.job-professional-area.move', ['id' => $first->job_professional_area_id]), ['parent_id' => $second->job_professional_area_id])
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $first = JobProfessionalArea::find($first->job_professional_area_id);
        $second = JobProfessionalArea::find($second->job_professional_area_id);

        $this->assertTrue($second->children[0] == $first);

    }

    public function testShow()
    {
        $first = $this->createJobProfessionalArea();

        $this->get(route('admin.job-professional-area.show', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.job-professional-area.show', ['id' => $first->job_professional_area_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("job-professional-area.admin.show", ['name' => $first->local_name])
        ]);

        $response->assertViewIs("admin.nested-list.show");

        $response->assertViewHas(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            "<td>{$first->job_professional_area_id}</td>",
            "<td>{$first->local_name}</td>",
            "<td>{$first->code}</td>"
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testUpdateForm()
    {
        $first = $this->createJobProfessionalArea();

        $this->get(route('admin.job-professional-area.updateForm', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.job-professional-area.updateForm', ['id' => $first->job_professional_area_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("job-professional-area.admin.updateForm", ['name' => $first->local_name])
        ]);

        $response->assertViewIs("admin.nested-list.updateForm");

        $response->assertViewHas(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="name"',
            'name="code"',
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testDestroy() {
        $first = $this->createJobProfessionalArea();

        $this->get(route('admin.job-professional-area.destroy', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.job-professional-area.destroy', ['id' => $first->job_professional_area_id]))
            ->assertRedirect(route('admin.job-professional-area.listAll'))->assertSessionHas('success');

        $this->assertNull(JobProfessionalArea::find($first->job_professional_area_id));

    }

    public function testShiftUp() {
        $first = $this->createJobProfessionalArea();
        $second = $this->createJobProfessionalArea();
        $third = $this->createJobProfessionalArea();
        $first->appendNode($second);
        $first->appendNode($third);

        $this->get(route('admin.job-professional-area.up', ['id' => $third->job_professional_area_id]))
            ->assertRedirect()->assertSessionHas('success');


        $this->get(route('admin.job-professional-area.up', ['id' => $third->job_professional_area_id]))
            ->assertRedirect()->assertSessionHas('error');
    }

    public function testShiftDown() {
        $first = $this->createJobProfessionalArea();
        $this->createJobProfessionalArea();

        $this->get(route('admin.job-professional-area.down', ['id' => $first->job_professional_area_id]))
            ->assertRedirect()->assertSessionHas('success');


        $this->get(route('admin.job-professional-area.down', ['id' => $first->job_professional_area_id]))
            ->assertRedirect()->assertSessionHas('error');
    }

    public function testMakeRoot() {
        $first = $this->createJobProfessionalArea();
        $second = $this->createJobProfessionalArea();
        $first->appendNode($second);

        $first = JobProfessionalArea::find($first->job_professional_area_id);
        $second = JobProfessionalArea::find($second->job_professional_area_id);

        $this->assertTrue($second->parent == $first);

        $this->get(route('admin.job-professional-area.make-root', ['id' => $second->job_professional_area_id]))
            ->assertRedirect()->assertSessionHas('success');

        $second = JobProfessionalArea::find($second->job_professional_area_id);
        $this->assertNull($second->parent);
    }

    public function testMoveForm() {
        $first = $this->createJobProfessionalArea();

        $response = $this->get(route('admin.job-professional-area.moveForm', ['id' => $first->job_professional_area_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("job-professional-area.admin.moveForm", ['name' => $first->local_name])
        ]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="parent_id"',
        ]);

        $response->assertViewIs("admin.nested-list.moveForm");

        $response->assertViewHas([
            'model' => $first,
            'data' => JobProfessionalArea::dataForDropdown($first->job_professional_area_id)
        ]);

    }


}
