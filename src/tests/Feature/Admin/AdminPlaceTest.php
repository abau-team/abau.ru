<?php

namespace Tests\Feature\Admin;

use App\Models\Place;
use Tests\Feature\TraitCheckLocalization;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminPlaceTest extends TestCase
{

    use WithFaker, RefreshDatabase, TraitAdminMenu, TraitCheckLocalization;

    /**
     * @return Place
     */
    protected function createPlace() {
        return Place::create([
            'name' => $this->faker->city,
            'code' => $this->faker->regexify('[a-zA-Z0-9]{25}'),
            'type' => Place::TYPE_CITY,
        ]);
    }

    /**
     * Проверка Страница списка
     */
    public function testListAll()
    {
        $response = $this->get(route('admin.place.listAll'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('place.admin.listAll')
        ]);

        $response->assertViewIs("admin.nested-list.listAll");

        $response->assertViewHas(['models' => Place::defaultOrder()->get()->toTree()]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Test index page
     *
     * @return void
     */
    public function testCreateForm()
    {
        $response = $this->get(route('admin.place.createForm'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("place.admin.createForm"),
        ]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="type"',
            'name="capital"',
            'name="name"',
            'name="code"',
            'name="active"'
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Проверка контроллера сохранения
     */
    public function testCreate()
    {

        // Проверка пустых полей - Ошибка валидации
        $this->post(route('admin.place.create'))
            ->assertStatus(302)
            ->assertSessionHasErrors(['type', 'name', 'code']);

        // Проверка заполненных значений - Валидация успешная
        $this->post(route('admin.place.create'), [
            'name' => $this->faker->city,
            'code' => $this->faker->regexify('[a-zA-Z0-9]{26}'),
            'type' => Place::TYPE_CITY,
        ])->assertStatus(302)
            ->assertRedirect(route('admin.place.listAll'))
            ->assertSessionHasNoErrors();


    }

    /**
     * Проверка контролера редактирования
     */
    public function testUpdate()
    {
        $first = $this->createPlace();

        // Проверка пустых полей - Ошибка валидации
        $this->put(route('admin.place.update', ['id' => $first->place_id]))
            ->assertStatus(302)
            ->assertSessionHasErrors(['type', 'name', 'code']);

        // Проверка заполненных значений - Валидация успешная
        $this->put(route('admin.place.update', ['id' => $first->place_id]), [
            'name' => $this->faker->city,
            'code' => $this->faker->uuid,
            'type' => Place::TYPE_CITY,
        ])->assertStatus(302)
            ->assertRedirect(route('admin.place.listAll'))
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $result = Place::find($first->place_id);
        $this->assertFalse($first == $result);
    }


    public function testMoveStore()
    {
        $first = $this->createPlace();

        $second = $this->createPlace();

        $this->post(route('admin.place.move', ['id' => $first->place_id]))->assertSessionHasErrors(['parent_id']);

        $this->post(route('admin.place.move', ['id' => 9999999]))->assertStatus(404);

        $this->post(route('admin.place.move', ['id' => $first->place_id]), ['parent_id' => 9999999])->assertSessionHasErrors(['parent_id']);

        $this->post(route('admin.place.move', ['id' => $first->place_id]), ['parent_id' => $second->place_id])
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $first = Place::find($first->place_id);
        $second = Place::find($second->place_id);

        $this->assertTrue($second->children[0] == $first);

    }

    public function testShow()
    {
        $first = $this->createPlace();

        $this->get(route('admin.place.show', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.place.show', ['id' => $first->place_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("place.admin.show", ['name' => $first->local_name])
        ]);

        $response->assertViewIs("admin.nested-list.show");

        $response->assertViewHas(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            "<td>{$first->place_id}</td>",
            "<td>{$first->local_name}</td>",
            "<td>{$first->type_label}</td>",
            "<td>{$first->capital_label}</td>",
            "<td>{$first->code}</td>"
        ]);

        $this->seeAdminMenu($response);
    }

    public function testUpdateForm()
    {
        $first = $this->createPlace();

        $this->get(route('admin.place.updateForm', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.place.updateForm', ['id' => $first->place_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("place.admin.updateForm", ['name' => $first->local_name])
        ]);

        $response->assertViewIs("admin.nested-list.updateForm");

        $response->assertViewHas(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="type"',
            'name="capital"',
            'name="name"',
            'name="code"',
            'name="active"'
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testDestroy() {
        $first = $this->createPlace();

        $this->get(route('admin.place.destroy', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.place.destroy', ['id' => $first->place_id]))
            ->assertRedirect(route('admin.place.listAll'))->assertSessionHas('success');

        $this->assertNull(Place::find($first->place_id));

    }

    public function testShiftUp() {
        $first = $this->createPlace();
        $second = $this->createPlace();
        $third = $this->createPlace();
        $first->appendNode($second);
        $first->appendNode($third);

        $this->get(route('admin.place.up', ['id' => $third->place_id]))
            ->assertRedirect()->assertSessionHas('success');


        $this->get(route('admin.place.up', ['id' => $third->place_id]))
            ->assertRedirect()->assertSessionHas('error');
    }

    public function testShiftDown() {
        $first = $this->createPlace();
        $this->createPlace();

        $this->get(route('admin.place.down', ['id' => $first->place_id]))
            ->assertRedirect()->assertSessionHas('success');


        $this->get(route('admin.place.down', ['id' => $first->place_id]))
            ->assertRedirect()->assertSessionHas('error');
    }


    public function testActive() {
        $first = $this->createPlace();

        $this->get(route('admin.place.active', ['id' => $first->place_id]))
            ->assertRedirect()->assertSessionHas('success');

        $second = Place::find($first->place_id);

        $this->assertTrue($second->active == 1);
    }

    public function testUnActive() {
        $first = $this->createPlace();

        $this->get(route('admin.place.un-active', ['id' => $first->place_id]))
            ->assertRedirect()->assertSessionHas('success');

        $second = Place::find($first->place_id);

        $this->assertTrue($second->active == 0);
    }

    public function testMakeRoot() {
        $first = $this->createPlace();
        $second = $this->createPlace();
        $first->appendNode($second);

        $first = Place::find($first->place_id);
        $second = Place::find($second->place_id);

        $this->assertTrue($second->parent == $first);

        $this->get(route('admin.place.make-root', ['id' => $second->place_id]))
            ->assertRedirect()->assertSessionHas('success');

        $second = Place::find($second->place_id);
        $this->assertNull($second->parent);
    }

    public function testMoveForm() {
        $first = $this->createPlace();
        $this->createPlace();

        $response = $this->get(route('admin.place.moveForm', ['id' => $first->place_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("place.admin.moveForm", ['name' => $first->local_name])
        ]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="parent_id"',
        ]);

        $response->assertViewIs("admin.nested-list.moveForm");

        $response->assertViewHas([
            'model' => $first,
            'data' => Place::dataForDropdown($first->place_id)
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);

    }


}
