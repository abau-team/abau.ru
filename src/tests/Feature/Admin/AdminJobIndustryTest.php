<?php

namespace Tests\Feature\Admin;

use App\Models\JobIndustry;
use Tests\Feature\TraitCheckLocalization;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminJobIndustryTest extends TestCase
{

    use WithFaker, RefreshDatabase, TraitAdminMenu, TraitCheckLocalization;

    /**
     * @return JobIndustry
     */
    protected function createJobIndustry() {
        return JobIndustry::create([
            'name' => $this->faker->city,
            'code' => $this->faker->regexify('[a-zA-Z0-9]{25}'),
        ]);
    }

    /**
     * Проверка Страница списка
     */
    public function testListAll()
    {
        $this->createJobIndustry();
        $this->createJobIndustry();
        $this->createJobIndustry();
        $this->createJobIndustry();

        $response = $this->get(route('admin.job-industry.listAll'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __('job-industry.admin.listAll')
        ]);

        $response->assertViewIs("admin.nested-list.listAll");

        $response->assertViewHas(['models' => JobIndustry::defaultOrder()->get()->toTree()]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Test index page
     *
     * @return void
     */
    public function testCreateForm()
    {
        $response = $this->get(route('admin.job-industry.createForm'));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("job-industry.admin.createForm"),
        ]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="name"',
            'name="code"',
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    /**
     * Проверка контроллера сохранения
     */
    public function testCreate()
    {

        // Проверка пустых полей - Ошибка валидации
        $this->post(route('admin.job-industry.create'))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name', 'code']);

        // Проверка заполненных значений - Валидация успешная
        $this->post(route('admin.job-industry.create'), [
            'name' => $this->faker->city,
            'code' => $this->faker->regexify('[a-zA-Z0-9]{25}'),
        ])->assertStatus(302)
            ->assertRedirect(route('admin.job-industry.listAll'))
            ->assertSessionHasNoErrors();


    }

    /**
     * Проверка контролера редактирования
     */
    public function testUpdate()
    {
        $first = $this->createJobIndustry();

        // Проверка пустых полей - Ошибка валидации
        $this->put(route('admin.job-industry.update', ['id' => $first->job_industry_id]))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name', 'code']);

        // Проверка заполненных значений - Валидация успешная
        $this->put(route('admin.job-industry.update', ['id' => $first->job_industry_id]), [
            'name' => $this->faker->city,
            'code' => $this->faker->uuid,
        ])->assertStatus(302)
            ->assertRedirect(route('admin.job-industry.listAll'))
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $result = JobIndustry::find($first->job_industry_id);
        $this->assertFalse($first == $result);
    }


    public function testMove()
    {
        $first = $this->createJobIndustry();

        $second = $this->createJobIndustry();

        $this->post(route('admin.job-industry.move', ['id' => $first->job_industry_id]))->assertSessionHasErrors(['parent_id']);

        $this->post(route('admin.job-industry.move', ['id' => 9999999]))->assertStatus(404);

        $this->post(route('admin.job-industry.move', ['id' => $first->job_industry_id]), ['parent_id' => 9999999])->assertSessionHasErrors(['parent_id']);

        $this->post(route('admin.job-industry.move', ['id' => $first->job_industry_id]), ['parent_id' => $second->job_industry_id])
            ->assertSessionHasNoErrors()->assertSessionHas('success');

        $first = JobIndustry::find($first->job_industry_id);
        $second = JobIndustry::find($second->job_industry_id);

        $this->assertTrue($second->children[0] == $first);

    }

    public function testShow()
    {
        $first = $this->createJobIndustry();

        $this->get(route('admin.job-industry.show', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.job-industry.show', ['id' => $first->job_industry_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("job-industry.admin.show", ['name' => $first->local_name])
        ]);

        $response->assertViewIs("admin.nested-list.show");

        $response->assertViewHas(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            "<td>{$first->job_industry_id}</td>",
            "<td>{$first->local_name}</td>",
            "<td>{$first->code}</td>"
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testUpdateForm()
    {
        $first = $this->createJobIndustry();

        $this->get(route('admin.job-industry.updateForm', ['id' => 99999999]))->assertStatus(404);

        $response = $this->get(route('admin.job-industry.updateForm', ['id' => $first->job_industry_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("job-industry.admin.updateForm", ['name' => $first->local_name])
        ]);

        $response->assertViewIs("admin.nested-list.updateForm");

        $response->assertViewHas(['model' => $first]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="name"',
            'name="code"',
        ]);

        $this->seeAdminMenu($response);
        $this->dontSeeLocaleString($response);
    }

    public function testDestroy() {
        $first = $this->createJobIndustry();

        $this->get(route('admin.job-industry.destroy', ['id' => 99999999]))->assertStatus(404);

        $this->get(route('admin.job-industry.destroy', ['id' => $first->job_industry_id]))
            ->assertRedirect(route('admin.job-industry.listAll'))->assertSessionHas('success');

        $this->assertNull(JobIndustry::find($first->job_industry_id));

    }

    public function testShiftUp() {
        $first = $this->createJobIndustry();
        $second = $this->createJobIndustry();
        $third = $this->createJobIndustry();
        $first->appendNode($second);
        $first->appendNode($third);

        $this->get(route('admin.job-industry.up', ['id' => $third->job_industry_id]))
            ->assertRedirect()->assertSessionHas('success');


        $this->get(route('admin.job-industry.up', ['id' => $third->job_industry_id]))
            ->assertRedirect()->assertSessionHas('error');
    }

    public function testShiftDown() {
        $first = $this->createJobIndustry();
        $this->createJobIndustry();

        $this->get(route('admin.job-industry.down', ['id' => $first->job_industry_id]))
            ->assertRedirect()->assertSessionHas('success');


        $this->get(route('admin.job-industry.down', ['id' => $first->job_industry_id]))
            ->assertRedirect()->assertSessionHas('error');
    }

    public function testMakeRoot() {
        $first = $this->createJobIndustry();
        $second = $this->createJobIndustry();
        $first->appendNode($second);

        $first = JobIndustry::find($first->job_industry_id);
        $second = JobIndustry::find($second->job_industry_id);

        $this->assertTrue($second->parent == $first);

        $this->get(route('admin.job-industry.make-root', ['id' => $second->job_industry_id]))
            ->assertRedirect()->assertSessionHas('success');

        $second = JobIndustry::find($second->job_industry_id);
        $this->assertNull($second->parent);
    }

    public function testMoveForm() {
        $first = $this->createJobIndustry();

        $response = $this->get(route('admin.job-industry.moveForm', ['id' => $first->job_industry_id]));

        //Проверка ответа
        $response->assertStatus(200);

        // Проверка заголовка - Страница успешно загрузилась
        $response->assertSeeTextInOrder([
            __("job-industry.admin.moveForm", ['name' => $first->local_name])
        ]);

        // Проверка наличия полей формы
        $response->assertSeeInOrder([
            'name="parent_id"',
        ]);

        $response->assertViewIs("admin.nested-list.moveForm");

        $response->assertViewHas([
            'model' => $first,
            'data' => JobIndustry::dataForDropdown($first->job_industry_id)
        ]);

    }


}
