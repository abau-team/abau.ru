<?php

namespace Tests\Feature\Auth;

use App\Mail\VerificationEmail;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\RefreshDb;
use Tests\TestCase;
use Mail;

class RegisterTest extends TestCase
{

    use RefreshDb, WithFaker;

    public function testRegisterForm()
    {

        $this->get(route('registerForm'))
            ->assertSuccessful()
            ->assertViewIs('auth.registerForm');

    }

    public function testRegister()
    {
        // Запрет отправки Email
        Mail::fake();
        Mail::assertNothingSent();

        $userArray = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => '+7 (905) ' . $this->faker->regexify('/[0-9]{3}-[0-9]{2}-[0-9]{2}/'),
            'password' => 'secret',
            'password_confirmation' => 'secret',
            'term-agree' => true
        ];

        // Проверка пустого значения
        $this->post(route('register'))
            ->assertStatus(302)
            ->assertSessionHasErrors(['name', 'email', 'phone', 'password', 'term-agree']);

        // Проверка заполненного значения
        $this->post(route('register'), [
            'name' => $userArray['name'],
            'email' => $userArray['email'],
            'phone' => $userArray['phone'],
            'password' => $userArray['password'],
            'password_confirmation' => $userArray['password_confirmation'],
            'term-agree' => $userArray['term-agree']
        ])->assertStatus(302)
            //->assertRedirect(route('loginForm'))
            //->assertSessionHas('success', __('user.registerForm.flash-success'))
            ->assertSessionHasNoErrors();

        $user = User::first();

        $this->assertTrue($user->name == $userArray['name']);

        foreach ($user->contacts() as $contact) {
            /** @var Contact $contact */
            if ($contact->type == Contact::TYPE_EMAIL) {

                $this->assertTrue($userArray['email'] == $contact->value);

                Mail::assertSent(VerificationEmail::class, function ($mail) use ($contact) {
                    return $mail->contact === $contact;
                });

            } elseif ($contact->type == Contact::TYPE_PHONE) {
                $this->assertTrue($userArray['phone'] == $contact->value);
            }
        }

    }

}