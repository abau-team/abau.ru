<?php

namespace Tests;

use DatabaseSeeder;

trait RefreshDb
{

    /**
     * Refresh and seed database
     */
    public function databaseTest()
    {
        $this->artisan('migrate:fresh');
        $seeder = new DatabaseSeeder();
        $seeder->run();
        $this->assertTrue(true);
    }

}