<?php

namespace Tests\Unit\Http\Controller;

use App\Http\Controllers\SitemapController;
use ReflectionClass;
use Tests\TestCase;

class SitemapControllerTest extends TestCase {

    /**
     * @throws \ReflectionException
     */
    public function testGeneratePage() {
        $object = new SitemapController();
        $reflection = new ReflectionClass(get_class($object));

        $property = $reflection->getProperty('pages');
        $property->setAccessible(true);
        $this->assertEmpty($property->getValue($object));

        $method = $reflection->getMethod('generatePages');
        $method->setAccessible(true);
        $method->invokeArgs($object, []);

        $this->assertNotEmpty($property->getValue($object));
    }

}