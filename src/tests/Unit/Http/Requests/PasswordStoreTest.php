<?php

namespace Tests\Unit\Http\Requests\Admin;

use App\Http\Requests\PasswordStore;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Unit\Http\Requests\TraitValidation;
use Illuminate\Foundation\Testing\WithFaker;

class PasswordStoreTest extends TestCase {

    use TraitValidation, WithFaker, RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->rules = (new PasswordStore())->rules();
        $this->validator = $this->app['validator'];

    }

    public function testPasswordValidation() {
        $this->assertTrue($this->validateConfirmedFields('password', 'secret', 'secret'));
        $this->assertFalse($this->validateConfirmedFields('password', 'secret', 'secret2'));
        $this->assertFalse($this->validateConfirmedFields('password', '', ''));
        $this->assertFalse($this->validateConfirmedFields('password', 't', 't'));

        $tooLong = $this->faker->regexify('[a-zA-Z0-9]{41}');
        $this->assertFalse($this->validateConfirmedFields('password', $tooLong, $tooLong));
    }

}