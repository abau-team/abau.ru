<?php

namespace Tests\Unit\Http\Requests;

use Validator;

trait TraitValidation
{

    /**
     * @var array
     */
    protected $rules;

    /**
     * @var Validator
     */
    protected $validator;

    protected function getFieldValidator($field, $value)
    {
        return $this->validator->make(
            [$field => $value],
            [$field => $this->rules[$field]]
        );
    }

    protected function getFieldConfirmedValidator($field, $value, $valueConfirmed)
    {
        return $this->validator->make(
            [
                $field => $value,
                $field . '_confirmation' => $valueConfirmed
            ],
            [$field => $this->rules[$field]]
        );
    }

    protected function validateField($field, $value)
    {
        return $this->getFieldValidator($field, $value)->passes();
    }

    protected function validateConfirmedFields($field, $value, $valueConfirmed)
    {
        return $this->getFieldConfirmedValidator($field, $value, $valueConfirmed)->passes();
    }

}