<?php

namespace Tests\Unit\Http\Requests\Admin;

use App\Http\Requests\Admin\AdCategoryStore;
use App\Models\AdCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Unit\Http\Requests\TraitValidation;
use Illuminate\Foundation\Testing\WithFaker;

class AdCategoryStoreTest extends TestCase {

    use TraitValidation, WithFaker, RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->rules = (new AdCategoryStore())->rules();
        $this->validator = $this->app['validator'];

    }

    public function testNameValidation() {
        $this->assertTrue($this->validateField('name', 'test'));
        $this->assertFalse($this->validateField('name', ''));
        $this->assertFalse($this->validateField('name', 't'));
        $this->assertFalse($this->validateField('name', false));
        $this->assertFalse($this->validateField('name', $this->faker->regexify('[a-zA-Z0-9]{51}')));
    }

    public function testModelValidation() {
        $this->assertTrue($this->validateField('model', 'test'));
        $this->assertTrue($this->validateField('model', ''));
        $this->assertTrue($this->validateField('model', 't'));
        $this->assertTrue($this->validateField('model', false));
        $this->assertFalse($this->validateField('model', $this->faker->regexify('[a-zA-Z0-9]{51}')));
    }

    public function testCodeValidation() {
        AdCategory::create(['name' => 'not_unique', 'code' => 'not_unique']);

        $this->assertTrue($this->validateField('code', 'test'));
        $this->assertFalse($this->validateField('code', ''));
        $this->assertFalse($this->validateField('code', 't'));
        $this->assertFalse($this->validateField('code', false));
        $this->assertFalse($this->validateField('code', 'not_unique'));
        $this->assertFalse($this->validateField('code', $this->faker->regexify('[a-zA-Z0-9]{41}')));
    }

    public function testActiveValidation() {
        $this->assertTrue($this->validateField('active', true));
        $this->assertTrue($this->validateField('active', false));
        $this->assertTrue($this->validateField('active', 1));
        $this->assertTrue($this->validateField('active', 0));
        $this->assertTrue($this->validateField('active', "0"));
        $this->assertTrue($this->validateField('active', "1"));
        $this->assertFalse($this->validateField('active', null));
        $this->assertFalse($this->validateField('active', 'test'));
        $this->assertFalse($this->validateField('active', 123));
    }

}