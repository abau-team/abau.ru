<?php

namespace Tests\Unit\Http\Requests\Admin;

use App\Http\Requests\Admin\PlaceStore;
use App\Models\Place;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Unit\Http\Requests\TraitValidation;
use Illuminate\Foundation\Testing\WithFaker;

class PlaceStoreTest extends TestCase {

    use TraitValidation, WithFaker, RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->rules = (new PlaceStore())->rules();
        $this->validator = $this->app['validator'];

    }

    public function testNameValidation() {
        $this->assertTrue($this->validateField('name', 'test'));
        $this->assertFalse($this->validateField('name', ''));
        $this->assertFalse($this->validateField('name', 't'));
        $this->assertFalse($this->validateField('name', false));
        $this->assertFalse($this->validateField('name', $this->faker->regexify('[a-zA-Z0-9]{101}')));
    }

    public function testCodeValidation() {
        Place::create(['name' => 'not_unique', 'code' => 'not_unique', 'type' => Place::TYPE_CITY]);

        $this->assertTrue($this->validateField('code', 'test'));
        $this->assertFalse($this->validateField('code', ''));
        $this->assertFalse($this->validateField('code', 't'));
        $this->assertFalse($this->validateField('code', false));
        $this->assertFalse($this->validateField('code', 'not_unique'));
        $this->assertFalse($this->validateField('code', $this->faker->regexify('[a-zA-Z0-9]{41}')));
    }

    public function testTypeValidation() {
        $this->assertTrue($this->validateField('type', $this->faker->randomElement(array_flip(Place::getTypes()))));
        $this->assertFalse($this->validateField('type', ''));
        $this->assertFalse($this->validateField('type', 'test'));
        $this->assertFalse($this->validateField('type', 99999));
    }

    public function testCapitalValidation() {
        $this->assertTrue($this->validateField('capital', $this->faker->randomElement(array_flip(Place::getCapitals()))));
        $this->assertTrue($this->validateField('capital', ''));
        $this->assertTrue($this->validateField('capital', null));
        $this->assertFalse($this->validateField('capital', 'test'));
        $this->assertFalse($this->validateField('capital', 99999));
        $this->assertTrue($this->validateField('capital', false));
    }

    public function testActiveValidation() {
        $this->assertTrue($this->validateField('active', true));
        $this->assertTrue($this->validateField('active', false));
        $this->assertTrue($this->validateField('active', 1));
        $this->assertTrue($this->validateField('active', 0));
        $this->assertTrue($this->validateField('active', "0"));
        $this->assertTrue($this->validateField('active', "1"));
        $this->assertFalse($this->validateField('active', null));
        $this->assertFalse($this->validateField('active', 'test'));
        $this->assertFalse($this->validateField('active', 123));
    }

}