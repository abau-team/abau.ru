<?php

namespace Tests\Unit\Http\Requests\Admin;

use App\Http\Requests\Admin\AdminUserStore;
use App\Models\Place;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Unit\Http\Requests\TraitValidation;
use Illuminate\Foundation\Testing\WithFaker;

class AdminUserStoreTest extends TestCase {

    use TraitValidation, WithFaker, RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->rules = (new AdminUserStore())->rules();
        $this->validator = $this->app['validator'];

    }

    public function testNameValidation() {
        $this->assertTrue($this->validateField('name', 'test'));
        $this->assertFalse($this->validateField('name', ''));
        $this->assertFalse($this->validateField('name', 't'));
        $this->assertFalse($this->validateField('name', false));
        $this->assertFalse($this->validateField('name', $this->faker->regexify('[a-zA-Z0-9]{51}')));
    }

    public function testPlaceValidation() {
        $place = Place::create(['name' => 'exist', 'code' => 'exist', 'type' => Place::TYPE_CITY]);
        $this->assertFalse($this->validateField('place_id', 'text'));
        $this->assertTrue($this->validateField('place_id', ''));
        $this->assertFalse($this->validateField('place_id', 99999999));
        $this->assertFalse($this->validateField('place_id', false));
        $this->assertTrue($this->validateField('place_id', $place->place_id));
    }

    public function testPasswordValidation() {
        $this->assertTrue($this->validateConfirmedFields('password', 'secret', 'secret'));
        $this->assertFalse($this->validateConfirmedFields('password', 'secret', 'secret2'));
        $this->assertFalse($this->validateConfirmedFields('password', '', ''));
        $this->assertFalse($this->validateConfirmedFields('password', 't', 't'));

        $tooLong = $this->faker->regexify('[a-zA-Z0-9]{41}');
        $this->assertFalse($this->validateConfirmedFields('password', $tooLong, $tooLong));
    }

}