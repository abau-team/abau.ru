<?php

namespace Tests\Unit\Http\Requests\Admin;

use App\Http\Requests\Admin\JobProfessionalAreaStore;
use App\Models\JobProfessionalArea;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Unit\Http\Requests\TraitValidation;
use Illuminate\Foundation\Testing\WithFaker;

class JobProfessionalAreaStoreTest extends TestCase {

    use TraitValidation, WithFaker, RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->rules = (new JobProfessionalAreaStore())->rules();
        $this->validator = $this->app['validator'];

    }

    public function testNameValidation() {
        $this->assertTrue($this->validateField('name', 'test'));
        $this->assertFalse($this->validateField('name', ''));
        $this->assertFalse($this->validateField('name', 't'));
        $this->assertFalse($this->validateField('name', false));
        $this->assertFalse($this->validateField('name', $this->faker->regexify('[a-zA-Z0-9]{51}')));
    }

    public function testCodeValidation() {
        JobProfessionalArea::create(['name' => 'not_unique', 'code' => 'not_unique']);

        $this->assertTrue($this->validateField('code', 'test'));
        $this->assertFalse($this->validateField('code', ''));
        $this->assertFalse($this->validateField('code', 't'));
        $this->assertFalse($this->validateField('code', false));
        $this->assertFalse($this->validateField('code', 'not_unique'));
        $this->assertFalse($this->validateField('code', $this->faker->regexify('[a-zA-Z0-9]{41}')));
    }
    
}