<?php

namespace Tests\Unit;

use App\AdminMenuFilter;
use JeroenNoten\LaravelAdminLte\Menu\Builder;
use Tests\TestCase;

class AdminMenuFilterTest extends TestCase
{
    /**
     * @var AdminMenuFilter
     */
    protected $testClass;

    protected function setUp(): void
    {
        $this->testClass = new AdminMenuFilter();
        parent::setUp();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTransform()
    {
        $data = ['translate' => 'test'];

        $answer = $this->testClass->transform($data, new Builder());


        $this->assertTrue($answer['text'] == __($data['translate']));
    }
}
