<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\AdCategory;

class AdCategoryTest extends TestCase
{
    use RefreshDatabase;


    public function testGetLocalNameAttribute() {
        $testData = [
            'name' => 'Test',
            'code' => 'test',
        ];

        $testModel = new AdCategory($testData);

        $this->assertTrue(
            __("ad.ad-category.categories.{$testData['code']}") == $testModel->local_name
        );
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDataForDropdown()
    {
        $translate = function ($data) {
            return __("ad.ad-category.categories.{$data}");
        };

        $data = [
            ['name' => 'Job', 'code' => 'job', 'children' => [
                ['name' => 'Vacancy', 'code' => 'vacancies'],
                ['name' => 'Resume', 'code' => 'resumes']
            ]]
        ];

        $dataAfter1 = [
            $translate($data[0]['code']),
            "-" . $translate($data[0]['children'][0]['code']),
            "-" . $translate($data[0]['children'][1]['code']),
        ];

        $dataAfter2 = [
            $translate($data[0]['code']),
            "-" . $translate($data[0]['children'][0]['code']),
        ];

        AdCategory::rebuildTree($data);

        $answer = AdCategory::dataForDropdown();

        $this->assertTrue(array_values($answer) == array_values($dataAfter1));

        $model = AdCategory::whereCode('resumes')->first();

        $answer = AdCategory::dataForDropdown($model->ad_category_id);

        $this->assertTrue(array_values($answer) == array_values($dataAfter2));
    }
}
