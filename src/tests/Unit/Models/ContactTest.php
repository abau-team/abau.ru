<?php

namespace Tests\Unit\Models;

use App\Models\Contact;
use App\Models\User;
use DatabaseSeeder;
use Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactTest extends TestCase {

    use WithFaker;

    protected function createUser() {
        return User::create([
            'name' => $this->faker->firstName,
            'password' => Hash::make('secret'),
        ]);
    }

    /**
     * @return Contact
     */
    protected function createContact() {
        return $this->createUser()->contacts()->create([
            'name' => $this->faker->firstName,
            'type' => $this->faker->randomElement([Contact::TYPE_PHONE, Contact::TYPE_EMAIL]),
            'value' => $this->faker->phoneNumber
        ])->refresh();
    }

    public function testSetDb() {
        $this->artisan('migrate:fresh');
        $seeder = new DatabaseSeeder();
        $seeder->run();
        $this->assertTrue(true);
    }

    public function testCreate() {
        $contact = $this->createContact();
        $check = Contact::find($contact->contact_id);
        $this->assertNotEmpty($check);
        $this->assertTrue($contact->name == $check->name);
    }

    public function testBan() {
        $first = $this->createContact();
        $this->assertFalse($first->isBanned);
        $this->assertTrue($first->ban());
        $first->refresh();
        $this->assertTrue($first->isBanned);
        $this->assertFalse($first->ban());
    }

    public function testUnBan() {
        $first = $this->createContact();
        $this->assertFalse($first->unBan());
        $first->refresh();
        $this->assertTrue($first->ban());
        $first->refresh();
        $this->assertTrue($first->isBanned);
        $this->assertTrue($first->unBan());
        $first->refresh();
        $this->assertFalse($first->isBanned);
    }

    public function testIsBan() {
        $first = $this->createContact();
        $this->assertNull($first->blocked_at);
        $this->assertFalse($first->isBanned);
        $first->blocked_at = now();
        $this->assertTrue($first->isBanned);
    }

    public function testMarkDelete() {
        $first = $this->createContact();
        $this->assertFalse($first->isDeleted);
        $this->assertTrue($first->markDeleted());
        $first->refresh();
        $this->assertTrue($first->isDeleted);
        $this->assertFalse($first->markDeleted());
    }

    public function testMarkUnDelete() {
        $first = $this->createContact();
        $this->assertFalse($first->markUnDeleted());
        $first->refresh();
        $this->assertTrue($first->markDeleted());
        $first->refresh();
        $this->assertTrue($first->isDeleted);
        $this->assertTrue($first->markUnDeleted());
        $first->refresh();
        $this->assertFalse($first->isDeleted);
    }

    public function testIsDeleted() {
        $first = $this->createContact();
        $this->assertNull($first->deleted_at);
        $this->assertFalse($first->isDeleted);
        $first->deleted_at = now();
        $this->assertTrue($first->isDeleted);
    }

    public function testIsActivated() {
        $first = $this->createContact();
        $this->assertNull($first->activated_at);
        $this->assertFalse($first->isActivated);
        $first->activated_at = now();
        $this->assertTrue($first->isActivated);
    }

    public function testActivate() {
        $first = $this->createContact();
        $this->assertFalse($first->isActivated);
        $this->assertTrue($first->activate());
        $first->refresh();
        $this->assertTrue($first->isActivated);
        $this->assertFalse($first->activate());
    }

    public function testDeactivate() {
        $first = $this->createContact();
        $this->assertFalse($first->deactivate());
        $first->refresh();
        $this->assertTrue($first->activate());
        $first->refresh();
        $this->assertTrue($first->isActivated);
        $this->assertTrue($first->deactivate());
        $first->refresh();
        $this->assertFalse($first->isActivated);
    }

    public function testGenerateToken() {
        $first = $this->createContact();
        $first->type = Contact::TYPE_EMAIL;
        $this->assertTrue(preg_match('/[a-zA-Z0-9]{20,30}/', $first->generateToken()) > 0);
        $first->type = Contact::TYPE_PHONE;
        $this->assertTrue(preg_match('/[A-Z0-9]{6}/', $first->generateToken()) > 0);

    }

}