<?php
namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\JobProfessionalArea;

class JobProfessionalAreaTest extends TestCase {

    use RefreshDatabase;

    public function testGetLocalNameAttribute() {
        $testData = [
            'name' => 'Test',
            'code' => 'test'
        ];

        $testModel = new JobProfessionalArea($testData);

        $this->assertTrue(
            __("ad.job_professional_area.values.{$testData['code']}") == $testModel->local_name
        );
    }

}