<?php

namespace Tests\Unit\Models;

use App\Models\User;
use DatabaseSeeder;
use Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase {

    use WithFaker;

    public function testSetDb() {
        $this->artisan('migrate:fresh');
        $seeder = new DatabaseSeeder();
        $seeder->run();
        $this->assertTrue(true);
    }

    /**
     * @return User
     */
    protected function createUser()
    {
        return User::create([
            'name' => $this->faker->firstName,
            'password' => Hash::make('secret'),
        ]);
    }

    public function testBan() {
        $first = $this->createUser();
        $this->assertFalse($first->isBanned);
        $this->assertTrue($first->ban());
        $first->refresh();
        $this->assertTrue($first->isBanned);
        $this->assertFalse($first->ban());
    }

    public function testUnBan() {
        $first = $this->createUser();
        $this->assertFalse($first->unBan());
        $first->refresh();
        $this->assertTrue($first->ban());
        $first->refresh();
        $this->assertTrue($first->isBanned);
        $this->assertTrue($first->unBan());
        $first->refresh();
        $this->assertFalse($first->isBanned);
    }

    public function testIsBan() {
        $first = $this->createUser();
        $this->assertNull($first->blocked_at);
        $this->assertFalse($first->isBanned);
        $first->blocked_at = now();
        $this->assertTrue($first->isBanned);
    }

    public function testMarkDelete() {
        $first = $this->createUser();
        $this->assertFalse($first->isDeleted);
        $this->assertTrue($first->markDeleted());
        $first->refresh();
        $this->assertTrue($first->isDeleted);
        $this->assertFalse($first->markDeleted());
    }

    public function testMarkUnDelete() {
        $first = $this->createUser();
        $this->assertFalse($first->markUnDeleted());
        $first->refresh();
        $this->assertTrue($first->markDeleted());
        $first->refresh();
        $this->assertTrue($first->isDeleted);
        $this->assertTrue($first->markUnDeleted());
        $first->refresh();
        $this->assertFalse($first->isDeleted);
    }

    public function testIsDeleted() {
        $first = $this->createUser();
        $this->assertNull($first->deleted_at);
        $this->assertFalse($first->isDeleted);
        $first->deleted_at = now();
        $this->assertTrue($first->isDeleted);
    }

    public function testSetPassword() {
        $first = new User();
        $first->setPassword('secret');
        $this->assertTrue(Hash::check('secret', $first->password));
    }

    public function testCheckPassword() {
        $first = new User();
        $password = Hash::make('secret');
        $first->password = $password;
        $this->assertTrue($first->checkPassword('secret'));
    }




}