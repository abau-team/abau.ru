<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Place;

class PlaceTest extends TestCase
{
    use RefreshDatabase;


    public function testGetLocalNameAttribute() {
        $testData = [
            'name' => 'Test',
        ];

        $testModel = new Place($testData);

        $this->assertTrue(
            __("place.places.{$testData['name']}.nominative") == $testModel->local_name
        );
    }

    public function testGetUrlIndexAttribute() {
        $testData = [
            'code' => 'test',
        ];

        $testModel = new Place($testData);

        $this->assertTrue(
            route('ad.search', ['place' => $testData['code']]) == $testModel->url_index
        );
    }

    public function testGetCapitalLabelAttribute() {
        $testData = [
            'capital' => Place::CAPITAL_MUNICIPAL_DISTRICT,
        ];

        $testModel = new Place($testData);

        $this->assertTrue(
            $testModel->capital_label == __('place.capital.municipal-district')
        );
    }

    public function testGetTypeLabelAttribute() {
        $testData = [
            'type' => Place::TYPE_REGION,
        ];

        $testModel = new Place($testData);

        $this->assertTrue(
            $testModel->type_label == __('place.types.region')
        );
    }



}
