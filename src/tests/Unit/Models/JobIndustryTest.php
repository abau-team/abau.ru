<?php
namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\JobIndustry;

class JobIndustryTest extends TestCase {

    use RefreshDatabase;

    public function testGetLocalNameAttribute() {
        $testData = [
            'name' => 'Test',
            'code' => 'test'
        ];

        $testModel = new JobIndustry($testData);

        $this->assertTrue(
            __("ad.job-industry.values.{$testData['code']}") == $testModel->local_name
        );
    }

}