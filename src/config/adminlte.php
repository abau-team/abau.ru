<?php

use App\AdminMenuFilter;

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Control panel - Abau.ru',

    'title_prefix' => '',

    'title_postfix' => ' - Abau.ru',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>Abau</b>RU',

    'logo_mini' => '<b>A</b>RU',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'green',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'voito',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        [
            'translate' => 'admin.menu.dashboard',
            'route' => 'admin.home',
            'icon' => 'dashboard',
        ],
        [
            'translate' => 'admin.menu.ads',
            'route' => 'admin.ad.listAll',
            'icon' => 'fas fa-ad',
        ],
        [
            'translate' => 'admin.menu.places',
            'route' => 'admin.place.listAll',
            'icon' => 'map',
        ],
        [
            'translate' => 'admin.menu.lists',
            'icon' => 'reorder',
            'submenu' => [
                [
                    'translate' => 'admin.menu.ad-categories',
                    'route' => 'admin.ad-category.listAll',
                    'icon' => 'reorder',
                ],
                [
                    'translate' => 'admin.menu.work',
                    'icon' => 'cogs',
                    'submenu' => [
                        [
                            'translate' => 'admin.menu.job-professional-areas',
                            'route' => 'admin.job-professional-area.listAll',
                        ],
                        [
                            'translate' => 'admin.menu.job-industries',
                            'route' => 'admin.job-industry.listAll',
                        ],
                    ]
                ],
            ],
        ],
        [
            'translate' => 'admin.menu.users',
            'icon' => 'users',
            'submenu' => [
                [
                    'translate' => 'admin.menu.listActive',
                    'route' => 'admin.user.listActive',
                    'icon' => 'user',
                ],
                [
                    'translate' => 'admin.menu.listBanned',
                    'route' => 'admin.user.listBanned',
                    'icon' => 'ban',
                ],
                [
                    'translate' => 'admin.menu.listDeleted',
                    'route' => 'admin.user.listDeleted',
                    'icon' => 'trash',
                ],
                [
                    'translate' => 'admin.menu.listAll',
                    'route' => 'admin.user.listAll',
                    'icon' => 'archive',
                ],
            ],
        ],
        [
            'translate' => 'admin.menu.contacts',
            'icon' => 'phone',
            'submenu' => [
                [
                    'translate' => 'admin.menu.listActive',
                    'route' => 'admin.contact.listActive',
                    'icon' => 'user',
                ],
                [
                    'translate' => 'admin.menu.listAll',
                    'route' => 'admin.contact.listAll',
                    'icon' => 'ban',
                ],
                [
                    'translate' => 'admin.menu.listUnActivated',
                    'route' => 'admin.contact.listUnActivated',
                    'icon' => 'trash',
                ],
                [
                    'translate' => 'admin.menu.listBanned',
                    'route' => 'admin.contact.listBanned',
                    'icon' => 'archive',
                ],
                [
                    'translate' => 'admin.menu.listDeleted',
                    'route' => 'admin.contact.listDeleted',
                    'icon' => 'archive',
                ],
                [
                    'translate' => 'admin.menu.tokens',
                    'icon' => 'phone',
                    'route' => 'admin.token.listAll',
                ],
            ],
        ],

        'Settings',
        [
            'translate' => 'admin.menu.clear-cache',
            'route' => 'admin.clearCache',
            'icon' => 'trash',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        AdminMenuFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2' => true,
        'chartjs' => true,
    ],
];
