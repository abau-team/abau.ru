<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Auth
 */
Route::get('/register', 'Auth\RegisterController@registerForm')->name('registerForm');
Route::post('/register', 'Auth\RegisterController@register')->name('register');
Route::get('/login', 'Auth\LoginController@loginForm')->name('loginForm');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/logout', 'Auth\LogoutController@logout')->name('logout');
Route::get('/activate/{key}', 'Auth\ActivateController@activateEmail')->name('activateEmail');
Route::get('/recovery', 'Auth\RecoveryController@recoveryForm')->name('recoveryForm');
Route::post('/recovery', 'Auth\RecoveryController@recovery')->name('recovery');
Route::get('/reset/{key}', 'Auth\ResetPasswordController@resetForm')->name('resetForm');
Route::post('/reset/{key}', 'Auth\ResetPasswordController@reset')->name('reset');


Route::get('/sitemap.xml', 'SitemapController@xml')->name('sitemap.xml');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/term', 'TermController@index')->name('term');


/**
 * Admin
 */
Route::namespace('Admin')->prefix('voito')->name('admin.')->middleware('admin')->group(function () {
    Route::get('/', 'IndexController@dashboard')->name('home');
    Route::get('/clear-cache', 'IndexController@clearCache')->name('clearCache');

    Route::prefix('/place')->name('place.')->group(function () {

        Route::get('/', 'PlaceController@listAll')->name('listAll');
        Route::post('/', 'PlaceController@create')->name('create');
        Route::get('/create', 'PlaceController@createForm')->name('createForm');
        Route::get('/{id}', 'PlaceController@show')->name('show');
        Route::put('/{id}', 'PlaceController@update')->name('update');
        Route::get('/{id}/edit', 'PlaceController@updateForm')->name('updateForm');
        Route::get('/{id}/destroy', 'PlaceController@destroy')->name('destroy');

        Route::get('/{id}/move', 'PlaceController@moveForm')->name('moveForm');
        Route::post('/{id}/move', 'PlaceController@move')->name('move');
        Route::get('/{id}/make-root', 'PlaceController@makeRoot')->name('make-root');
        Route::get('/{id}/set-active', 'PlaceController@setActive')->name('active');
        Route::get('/{id}/set-un-active', 'PlaceController@setUnActive')->name('un-active');
        Route::get('/{id}/up/{step?}', 'PlaceController@shiftUp')->name('up');
        Route::get('/{id}/down/{step?}', 'PlaceController@shiftDown')->name('down');
    });

    Route::prefix('/ad-category')->name('ad-category.')->group(function () {

        Route::get('/', 'AdCategoryController@listAll')->name('listAll');
        Route::post('/', 'AdCategoryController@create')->name('create');
        Route::get('/create', 'AdCategoryController@createForm')->name('createForm');
        Route::get('/{id}', 'AdCategoryController@show')->name('show');
        Route::put('/{id}', 'AdCategoryController@update')->name('update');
        Route::get('/{id}/edit', 'AdCategoryController@updateForm')->name('updateForm');
        Route::get('/{id}/destroy', 'AdCategoryController@destroy')->name('destroy');

        Route::get('/{id}/move', 'AdCategoryController@moveForm')->name('moveForm');
        Route::post('/{id}/move', 'AdCategoryController@move')->name('move');
        Route::get('/{id}/make-root', 'AdCategoryController@makeRoot')->name('make-root');
        Route::get('/{id}/set-active', 'AdCategoryController@setActive')->name('active');
        Route::get('/{id}/set-un-active', 'AdCategoryController@setUnActive')->name('un-active');
        Route::get('/{id}/up/{step?}', 'AdCategoryController@shiftUp')->name('up');
        Route::get('/{id}/down/{step?}', 'AdCategoryController@shiftDown')->name('down');
    });

    Route::prefix('/job-industry')->name('job-industry.')->group(function () {

        Route::get('/', 'JobIndustryController@listAll')->name('listAll');
        Route::post('/', 'JobIndustryController@create')->name('create');
        Route::get('/create', 'JobIndustryController@createForm')->name('createForm');
        Route::get('/{id}', 'JobIndustryController@show')->name('show');
        Route::put('/{id}', 'JobIndustryController@update')->name('update');
        Route::get('/{id}/edit', 'JobIndustryController@updateForm')->name('updateForm');
        Route::get('/{id}/destroy', 'JobIndustryController@destroy')->name('destroy');

        Route::get('/{id}/move', 'JobIndustryController@moveForm')->name('moveForm');
        Route::post('/{id}/move', 'JobIndustryController@move')->name('move');
        Route::get('/{id}/make-root', 'JobIndustryController@makeRoot')->name('make-root');
        Route::get('/{id}/up/{step?}', 'JobIndustryController@shiftUp')->name('up');
        Route::get('/{id}/down/{step?}', 'JobIndustryController@shiftDown')->name('down');
    });

    Route::prefix('/job-professional-area')->name('job-professional-area.')->group(function () {

        Route::get('/', 'JobProfessionalAreaController@listAll')->name('listAll');
        Route::post('/', 'JobProfessionalAreaController@create')->name('create');
        Route::get('/create', 'JobProfessionalAreaController@createForm')->name('createForm');
        Route::get('/{id}', 'JobProfessionalAreaController@show')->name('show');
        Route::put('/{id}', 'JobProfessionalAreaController@update')->name('update');
        Route::get('/{id}/edit', 'JobProfessionalAreaController@updateForm')->name('updateForm');
        Route::get('/{id}/destroy', 'JobProfessionalAreaController@destroy')->name('destroy');

        Route::get('/{id}/move', 'JobProfessionalAreaController@moveForm')->name('moveForm');
        Route::post('/{id}/move', 'JobProfessionalAreaController@move')->name('move');
        Route::get('/{id}/make-root', 'JobProfessionalAreaController@makeRoot')->name('make-root');
        Route::get('/{id}/up/{step?}', 'JobProfessionalAreaController@shiftUp')->name('up');
        Route::get('/{id}/down/{step?}', 'JobProfessionalAreaController@shiftDown')->name('down');
    });

    Route::prefix('/user')->name('user.')->group(function () {

        Route::get('/active', 'UserController@listActive')->name('listActive');
        Route::get('/all', 'UserController@listAll')->name('listAll');
        Route::get('/banned', 'UserController@listBanned')->name('listBanned');
        Route::get('/deleted', 'UserController@listDeleted')->name('listDeleted');

        Route::get('/create', 'UserController@createForm')->name('createForm');
        Route::post('/', 'UserController@create')->name('create');

        Route::get('/{id}', 'UserController@show')->name('show');

        Route::get('/{id}/edit', 'UserController@updateForm')->name('updateForm');
        Route::put('/{id}', 'UserController@update')->name('update');

        Route::get('/{id}/password', 'UserController@passwordForm')->name('passwordForm');
        Route::put('/{id}/password', 'UserController@password')->name('password');

        Route::get('/{id}/delete', 'UserController@delete')->name('delete');
        Route::get('/{id}/un-delete', 'UserController@unDelete')->name('unDelete');
        Route::get('/{id}/destroy', 'UserController@destroy')->name('destroy');
        Route::get('/{id}/ban', 'UserController@ban')->name('ban');
        Route::get('/{id}/un-ban', 'UserController@unBan')->name('unBan');
    });

    Route::prefix('/contacts')->name('contact.')->group(function () {
        Route::get('/active', 'ContactController@listActive')->name('listActive');
        Route::get('/all', 'ContactController@listAll')->name('listAll');
        Route::get('/unActivated', 'ContactController@listUnActivated')->name('listUnActivated');
        Route::get('/banned', 'ContactController@listBanned')->name('listBanned');
        Route::get('/deleted', 'ContactController@listDeleted')->name('listDeleted');
        Route::get('/user/{id}', 'ContactController@listByUser')->name('listByUser');
        Route::post('/user/{id}', 'ContactController@create')->name('create');
        Route::get('/user/{id}/create', 'ContactController@createForm')->name('createForm');
        Route::get('/{id}', 'ContactController@show')->name('show');
        Route::put('/{id}', 'ContactController@update')->name('update');
        Route::get('/{id}/update', 'ContactController@updateForm')->name('updateForm');

        Route::get('/{id}/delete', 'ContactController@delete')->name('delete');
        Route::get('/{id}/un-delete', 'ContactController@unDelete')->name('unDelete');
        Route::get('/{id}/destroy', 'ContactController@destroy')->name('destroy');
        Route::get('/{id}/ban', 'ContactController@ban')->name('ban');
        Route::get('/{id}/un-ban', 'ContactController@unBan')->name('unBan');
        Route::get('/{id}/activate', 'ContactController@activate')->name('activate');
        Route::get('/{id}/un-activate', 'ContactController@deactivate')->name('deactivate');
    });

    Route::prefix('/tokens')->name('token.')->group(function () {
        Route::get('/', 'TokenController@listAll')->name('listAll');
        Route::get('/user/{id}', 'TokenController@listByUser')->name('listByUser');
        Route::get('/contact/{id}', 'TokenController@listByContact')->name('listByContact');
        Route::get('/destroy/{type}/{value}', 'TokenController@destroy')->name('destroy');
    });

    Route::prefix('/ads')->name('ad.')->group(function () {
        Route::get('/index', 'AdController@listAll')->name('listAll');

        Route::get('/create', 'AdController@createForm')->name('createForm');
        Route::get('/create-vacancy', 'AdController@createFormVacancy')->name('createFormVacancy');
        Route::get('/create-resume', 'AdController@createFormResume')->name('createFormResume');
        Route::post('/create', 'AdController@create')->name('create');

        Route::get('/create/user/{user_id}', 'AdController@createFormForUser')->name('createFormForUser');
        Route::post('/create/user/{user_id}', 'AdController@createForUser')->name('createForUser');

        Route::get('/{id}/update', 'AdController@updateForm')->name('updateForm');
        Route::put('/{id}', 'AdController@update')->name('update');

        Route::get('/{id}', 'AdController@show')->name('show');

        Route::get('/{id}/delete', 'AdController@delete')->name('delete');
        Route::get('/{id}/un-delete', 'AdController@unDelete')->name('unDelete');
        Route::get('/{id}/destroy', 'AdController@destroy')->name('destroy');
        Route::get('/{id}/ban', 'AdController@ban')->name('ban');
        Route::get('/{id}/un-ban', 'AdController@unBan')->name('unBan');
        Route::get('/{id}/refresh', 'AdController@refresh')->name('refresh');
    });
});

// Личный кабинет
Route::get('/my', 'Ad\SearchController@my')->middleware('auth')->name('ad.my');
Route::get('/{id}', 'Ad\ViewController@show')->name('ad.view.short');
Route::get('/{id}/delete', 'Ad\ManageController@delete')->name('ad.delete');
Route::get('/{id}/un-delete', 'Ad\ManageController@unDelete')->name('ad.unDelete');
Route::get('/{id}/block', 'Ad\ManageController@block')->name('ad.block');
Route::get('/{id}/un-block', 'Ad\ManageController@unblock')->name('ad.unBlock');
Route::get('/{id}/edit', 'Ad\UpdateController@updateForm')->name('ad.updateForm');
Route::post('/{id}/edit', 'Ad\UpdateController@update')->name('ad.update');

// Добавление объявлений
Route::namespace('Ad')->prefix('add')->name('ad.create.')->group(function () {
    Route::get('/', 'CreateController@showForm')->name('form');
    Route::get('/{category}', 'CreateController@showForm')->name('formCategory');
    Route::post('/', 'CreateController@create')->name('create');
});

Route::prefix('{place}')->group(function () {

    /**
     * Поиск объявлений
     */
    Route::get('/', 'Ad\SearchController@all')->name('ad.search');
    Route::get('/{category}', 'Ad\SearchController@category')->name('ad.searchCategory');

    /**
     * Представления объявлений
     */
    Route::get('/{category}/{id}', 'Ad\ViewController@show')->name('ad.view.full');
});