<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->bigIncrements('photo_id');
            $table->string('path');
            $table->decimal('size');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('remastered_at')->nullable();
        });
        DB::statement('ALTER TABLE `photos` ADD `ip` VARBINARY(16)');

        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('ad_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->unsignedBigInteger('views')->default(0);
            $table->unsignedBigInteger('view_contact')->default(0);
            $table->unsignedTinyInteger('ad_category_id');
            $table->unsignedInteger('place_id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('blocked_at')->nullable();
            $table->timestamp('finished_at')->nullable();
            $table->timestamp('refreshed_at')->nullable();
            $table->string('title');
            $table->text('description');
            $table->string('price')->nullable();
            $table->unsignedTinyInteger('currency');
            $table->foreign('place_id')->references('place_id')->on('places')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('user_id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ad_category_id')->references('ad_category_id')->on('ad_categories')->onUpdate('cascade')->onDelete('cascade');
        });
        DB::statement('ALTER TABLE `ads` ADD `ip` VARBINARY(16)');

        Schema::create('photables', function (Blueprint $table) {
            $table->unsignedBigInteger('photo_id');
            $table->string('photable_type');
            $table->unsignedBigInteger('photable_id');
            $table->unsignedTinyInteger('order')->nullable();
            $table->foreign('photo_id')->references('photo_id')->on('photos')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('ads_contacts', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            $table->unsignedBigInteger('contact_id');
            $table->unsignedTinyInteger('order');
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('contact_id')->references('contact_id')->on('contacts')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('job_professional_areas', function (Blueprint $table) {
            $table->smallIncrements('job_professional_area_id');
            $table->string('name');
            $table->string('code', 50)->unique();
            $table->nestedSet();
        });

        Schema::create('job_industries', function (Blueprint $table) {
            $table->smallIncrements('job_industry_id');
            $table->string('name');
            $table->string('code', 50)->unique();
            $table->nestedSet();
        });

        Schema::create('job_skills', function (Blueprint $table) {
            $table->increments('job_skill_id');
            $table->string('name', 50)->unique();
            $table->string('code', 50);
        });

        Schema::create('ads_job_skills', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            $table->unsignedInteger('job_skill_id');
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('job_skill_id')->references('job_skill_id')->on('job_skills')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('ads_job_professional_areas', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            $table->unsignedSmallInteger('job_professional_area_id');
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('job_professional_area_id')->references('job_professional_area_id')->on('job_professional_areas')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('ads_job_industries', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            $table->unsignedSmallInteger('job_industry_id');
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('job_industry_id')->references('job_industry_id')->on('job_industries')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('job_drivers', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            $table->unsignedTinyInteger('type_driver');
            $table->date('date_issue')->nullable();
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('job_education', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            $table->unsignedTinyInteger('type');
            $table->unsignedSmallInteger('year');
            $table->string('name');
            $table->string('department')->nullable();
            $table->string('specialization');
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('job_languages', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            $table->unsignedTinyInteger('language');
            $table->unsignedTinyInteger('level');
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('job_experiences', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            $table->date('started_at');
            $table->unsignedTinyInteger('current_work')->nullable();
            $table->date('finished_at')->nullable();
            $table->string('organization');
            $table->unsignedInteger('place_id');
            $table->string('site');
            $table->unsignedSmallInteger('job_industry_id');
            $table->string('position');
            $table->text('duties');
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('job_industry_id')->references('job_industry_id')->on('job_industries')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('ad_vacancies', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            //$table->string('name');
            $table->string('company_name');
            $table->unsignedTinyInteger('schedule');
            $table->unsignedTinyInteger('employment');
            $table->string('schedule_comment')->nullable();
            $table->unsignedTinyInteger('experience');
            $table->unsignedTinyInteger('education');
            $table->unsignedTinyInteger('automobile')->nullable();
            $table->unsignedTinyInteger('sex')->nullable();
            $table->unsignedBigInteger('salary_from')->nullable();
            $table->unsignedBigInteger('salary_to')->nullable();
            $table->unsignedTinyInteger('age_from')->nullable();
            $table->unsignedTinyInteger('age_to')->nullable();
            $table->unsignedTinyInteger('non_resident')->nullable();
            $table->unsignedTinyInteger('remote_work')->nullable();
            $table->string('address');
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('ad_resumes', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('patronymic_name');
            $table->date('date_of_birth');
            $table->unsignedTinyInteger('sex');
            $table->unsignedTinyInteger('citizenship')->nullable();
            $table->unsignedTinyInteger('married')->nullable();
            $table->unsignedTinyInteger('children')->nullable();
            $table->unsignedTinyInteger('schedule');
            $table->string('schedule_comment')->nullable();
            $table->unsignedTinyInteger('employment');
            $table->unsignedTinyInteger('experience');
            $table->unsignedTinyInteger('education');
            $table->unsignedBigInteger('salary')->nullable();
            $table->unsignedTinyInteger('automobile')->nullable();
            $table->unsignedTinyInteger('ready_to_relocate')->nullable();
            $table->unsignedTinyInteger('ready_to_trip')->nullable();

            $table->foreign('ad_id')->references('ad_id')->on('ads')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_resumes');
        Schema::dropIfExists('ad_vacancies');
        Schema::dropIfExists('job_experience');
        Schema::dropIfExists('job_language');
        Schema::dropIfExists('job_education');
        Schema::dropIfExists('job_drivers');
        Schema::dropIfExists('job_professional_areas_ads');
        Schema::dropIfExists('job_industries');
        Schema::dropIfExists('job_professional_areas');
        Schema::dropIfExists('ads_contacts');
        DB::statement('ALTER TABLE `ads` DROP COLUMN `ip`');
        Schema::dropIfExists('ads');
        DB::statement('ALTER TABLE `photos` DROP COLUMN `ip`');
        Schema::dropIfExists('photos');
    }
}
