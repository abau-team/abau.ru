<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('contact_id');
            $table->unsignedInteger('user_id');
            $table->string('name', 50);
            $table->unsignedTinyInteger('type');
            $table->string('value', 100)->unique();
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('blocked_at')->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->foreign('user_id')->references('user_id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            //$table->unique(['type', 'value', 'deleted_at', 'blocked_at']);
        });
        DB::statement('ALTER TABLE `contacts` ADD `ip` VARBINARY(16)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `contacts` DROP COLUMN `ip`');
        Schema::dropIfExists('contacts');
    }
}
