<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->unsignedInteger('place_id')->nullable();
            $table->string('name', 60);
            $table->string('password', 100);
            $table->rememberToken();
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('last_active_at')->nullable();
            $table->timestamp('blocked_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->tinyInteger('admin')->nullable();
            $table->foreign('place_id')->references('place_id')->on('places')->onUpdate('cascade')->onDelete('set null');
        });
        DB::statement('ALTER TABLE `users` ADD `ip` VARBINARY(16)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `users` DROP COLUMN `ip`');
        Schema::dropIfExists('users');
    }
}
