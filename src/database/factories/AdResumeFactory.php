<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Ad;
use App\Models\AdCategory;
use App\Models\AdResume;
use Faker\Generator as Faker;

$factory->define(AdResume::class, function (Faker $faker) {

    return [
        'ad_id' => factory(Ad::class)->create([
            'ad_category_id' => AdCategory::whereCode('resumes')->first(['ad_category_id'])->ad_category_id,
        ])->ad_id,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'patronymic_name' => $faker->firstName,
        'date_of_birth' => $faker->date('Y-m-d', '-18 years'),
        'sex' => $faker->randomElement(array_keys(AdResume::getSexes())),
        'married' => $faker->boolean(70),
        'children' => $faker->boolean(25),
        'schedule' => $faker->randomElement(array_keys(AdResume::getSchedules())),
        'employment' => $faker->randomElement(array_keys(AdResume::getEmployments())),
        'schedule_comment' => $faker->optional(0.3)->sentence(3),
        'experience' => $faker->randomElement(array_keys(AdResume::getExperiences())),
        'education' => $faker->randomElement(array_keys(AdResume::getEducation())),
        'salary' => $faker->optional(0.8)->numberBetween(10, 350) . '000',
        'automobile' => $faker->boolean(25),
        'ready_to_relocate' => $faker->boolean(15),
        'ready_to_trip' => $faker->boolean(40),
        'citizenship' => $faker->numberBetween(1, 255)
    ];

});
