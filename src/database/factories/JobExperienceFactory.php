<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\JobExperience;
use App\Models\JobIndustry;
use App\Models\Place;
use Faker\Generator as Faker;

$factory->define(JobExperience::class, function (Faker $faker) {
    $start = $faker->date();
    return [
        'started_at' => $start,
        'current_work' => $faker->boolean,
        'finished_at' => $faker->date(),
        'organization' => $faker->company,
        'place_id' => Place::whereType(Place::TYPE_CITY)->get('place_id')->random()->place_id,
        'site' => $faker->domainName,
        'job_industry_id' => JobIndustry::whereIsLeaf()->get('job_industry_id')->random()->job_industry_id,
        'position' => $faker->word,
        'duties' => $faker->text,

    ];
});
