<?php

/* @var $factory Factory */

use App\Models\User;
use App\Models\Place;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(User::class, function (Faker $faker) {
    $registerDate = $faker->dateTimeBetween('-2 years');
    return [

        'place_id' => Place::whereType(Place::TYPE_CITY)->get(['place_id'])->random()->place_id,
        'name' => $faker->firstName,
        // secret
        'password' => '$2y$10$3ObkzvMbbFmdldJZX8UiUuEeG8cu.UiRHSsbtNJbuno50KC.g8b.O',
        'ip' => inet_pton($faker->optional(0.3, $faker->ipv4)->ipv6),
        'created_at' => $registerDate,
        'updated_at' => $faker->dateTimeBetween($registerDate),
        'last_active_at' => $faker->dateTimeBetween($registerDate),
        'blocked_at' => $faker->optional(0.05)->dateTimeBetween($registerDate),
        'deleted_at' => $faker->optional(0.05)->dateTimeBetween($registerDate),

    ];
});
