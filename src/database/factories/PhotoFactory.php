<?php

/* @var $factory Factory */

use App\Models\Photo;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Photo::class, function (Faker $faker) {
    return [
        'path' => $faker->imageUrl(),
        'size' => $faker->randomFloat(null, 1,100),
    ];
});
