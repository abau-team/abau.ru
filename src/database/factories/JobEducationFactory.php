<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\JobEducation;
use Faker\Generator as Faker;

$factory->define(JobEducation::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(array_flip(JobEducation::types())),
        'year' => $faker->year,
        'name' => $faker->company,
        'department' => $faker->word,
        'specialization' => $faker->word,
    ];
});
