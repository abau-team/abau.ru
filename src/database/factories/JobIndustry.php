<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\JobIndustry;
use Faker\Generator as Faker;

$factory->define(JobIndustry::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->unique()->regexify('[a-zA-Z0-9]{25}'),
    ];
});
