<?php

/* @var $factory Factory */

use App\Models\Place;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->defineAs(Place::class, 'country', function (Faker $faker) {
    return [
        'name' => $faker->country,
        'type' => Place::TYPE_COUNTRY,
        'code' => $faker->unique()->countryISOAlpha3,
        'active' => $faker->boolean(95),
    ];
});

$factory->defineAs(Place::class, 'region', function (Faker $faker) {
    return [
        'name' => $faker->city,
        'type' => Place::TYPE_REGION,
        'code' => $faker->unique()->regexify('[a-zA-Z0-9]{25}'),
        'active' => $faker->boolean(95),
    ];
});

$factory->defineAs(Place::class, 'city', function (Faker $faker) {
    return [
        'name' => $faker->city,
        'type' => Place::TYPE_CITY,
        'code' => $faker->unique()->regexify('[a-zA-Z0-9]{25}'),
        'active' => $faker->boolean(95),
    ];
});
