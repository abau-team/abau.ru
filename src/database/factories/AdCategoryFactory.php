<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\AdCategory;
use Faker\Generator as Faker;

$factory->define(AdCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'model' => $faker->optional(0.5)->name,
        'code' => $faker->unique()->regexify('[a-zA-Z0-9]{25}'),
        'active' => $faker->boolean(0.9),
    ];
});
