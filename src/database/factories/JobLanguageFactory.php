<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\JobLanguage;
use Faker\Generator as Faker;

$factory->define(JobLanguage::class, function (Faker $faker) {
    return [
        'language' => $faker->randomElement(array_flip(JobLanguage::languages())),
        'level' => $faker->randomElement(array_flip(JobLanguage::levels())),
    ];
});
