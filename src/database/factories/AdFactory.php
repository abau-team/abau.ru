<?php

/* @var $factory Factory */

use App\Models\Ad;
use App\Models\Place;
use App\Models\User;
use App\Models\AdCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Ad::class, function (Faker $faker) {
    $randomDigital = random_int(0, 100);
    $user = null;
    if ($randomDigital > 70) {
        $user['name'] = $faker->firstName;
        $user['phone'] = $faker->bothify('+7 (###) ###-##-##');
        $user['email'] = $faker->email;
    }

    $addDate = $faker->dateTimeBetween('-2 months');

    return [
        'user_id' => $user == null ? User::all(['user_id'])->random()->user_id : null,
        'place_id' => Place::whereType(Place::TYPE_CITY)->get(['place_id'])->random()->place_id,
        'ad_category_id' => AdCategory::whereIsLeaf()->get(['ad_category_id'])->random()->ad_category_id,
        'name' => $user['name'] ?? null,
        'email' => $user['email'] ?? null,
        'phone' => $user['phone'] ?? null,
        'title' => $faker->sentence(3),
        //'price' => $faker->bothify('## ### руб.'),
        'description' => $faker->paragraph,
        'currency' => $faker->randomElement([10, 20, 30]),
        'created_at' => $addDate,
        'updated_at' => $faker->dateTimeBetween($addDate),
        'deleted_at' => $faker->optional(0.05)->dateTimeBetween($addDate),
        'blocked_at' => $faker->optional(0.05)->dateTimeBetween($addDate),
        'refreshed_at' => $faker->optional(0.5)->dateTimeBetween($addDate),
        'views' => random_int(0, 1000),
        'view_contact' => random_int(0, 100),
        'ip' => inet_pton($faker->optional(0.2, $faker->ipv4)->ipv6),
    ];
});
