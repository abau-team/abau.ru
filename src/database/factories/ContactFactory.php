<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Contact;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    $registerDate = $faker->dateTimeBetween('-2 years');
    if (random_int(0, 10) > 5) {
        $type = Contact::TYPE_EMAIL;
        $value = $faker->unique()->email;
    } else {
        $type = Contact::TYPE_PHONE;
        $value = $faker->unique()->bothify('+7 (###) ###-##-##');
    }
    return [
        'name' => $faker->firstName,
        'user_id' => User::all(['user_id'])->random()->user_id,
        'type' => $type,
        'value' => $value,
        'created_at' => $registerDate,
        'activated_at' => $faker->dateTimeBetween($registerDate),
        'blocked_at' => $faker->optional(0.05)->dateTimeBetween($registerDate),
        'deleted_at' => $faker->optional(0.05)->dateTimeBetween($registerDate),
        'ip' => inet_pton($faker->optional(0.3, $faker->ipv4)->ipv6),
    ];
});
