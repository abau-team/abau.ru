<?php

/* @var $factory Factory */

use App\Models\JobSkill;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(JobSkill::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});
