<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Ad;
use App\Models\AdCategory;
use App\Models\AdVacancy;
use Faker\Generator as Faker;

$factory->define(AdVacancy::class, function (Faker $faker) {
    $salary = [];
    $salary['salary_from'] = $faker->optional(0.8)->numberBetween(10000, 200000);
    $salary['salary_to'] = $faker->optional(0.8)->numberBetween($salary['salary_from'], 200000);

    $ages = [];
    $ages['age_from'] = $faker->numberBetween(16, 80);
    $ages['age_to'] = $faker->numberBetween($ages['age_from'], 80);

    return [
        'ad_id' => factory(Ad::class)->create([
            'ad_category_id' => AdCategory::whereCode('vacancies')->first(['ad_category_id'])->ad_category_id,
        ])->ad_id,
        //'name' => $faker->firstName,
        'company_name' => $faker->company,
        'schedule' => $faker->randomElement(array_keys(AdVacancy::getSchedules())),
        'employment' => $faker->randomElement(array_keys(AdVacancy::getEmployments())),
        'schedule_comment' => $faker->optional(0.3)->sentence(3),
        'experience' => $faker->randomElement(array_keys(AdVacancy::getExperiences())),
        'education' => $faker->randomElement(array_keys(AdVacancy::getEducation())),
        'automobile' => $faker->boolean,
        'sex' => $faker->randomElement(array_keys(AdVacancy::getSexes())),
        'salary_from' => $salary['salary_from'],
        'salary_to' => $salary['salary_to'],
        'age_from' => $ages['age_from'],
        'age_to' => $ages['age_to'],
        'non_resident' => $faker->boolean,
        'remote_work' => $faker->boolean(10),
        'address' => $faker->address,
    ];
});
