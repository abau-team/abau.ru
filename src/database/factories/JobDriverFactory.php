<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\JobDriver;
use Faker\Generator as Faker;

$factory->defineAs(JobDriver::class, 'resume', function (Faker $faker) {
    return [
        'type_driver' => $faker->randomElement(array_flip(JobDriver::getDrivers())),
        'date_issue' => $faker->date()
    ];
});

$factory->defineAs(JobDriver::class, 'vacancy', function (Faker $faker) {
    return [
        'type_driver' => $faker->randomElement(array_flip(JobDriver::getDrivers())),
    ];
});