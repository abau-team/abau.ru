<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\JobProfessionalArea;
use Faker\Generator as Faker;

$factory->define(JobProfessionalArea::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->unique()->regexify('[a-zA-Z0-9]{25}'),
    ];
});
