<?php

use Illuminate\Database\Eloquent\Factory;

/* @var $factory Factory */

use App\Models\Token;
use Faker\Generator as Faker;

$factory->define(Token::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement([Token::TYPE_ACTIVATE, Token::TYPE_RECOVERY]),
    ];
});