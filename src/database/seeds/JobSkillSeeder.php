<?php

use Illuminate\Database\Seeder;
use App\Models\JobSkill;

class JobSkillSeeder extends Seeder {

    public function run() {
        factory(JobSkill::class, 100)->create();
    }

}