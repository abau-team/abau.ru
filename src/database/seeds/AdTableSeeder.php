<?php

use App\Models\AdResume;
use App\Models\AdVacancy;
use App\Models\JobDriver;
use App\Models\JobEducation;
use App\Models\JobExperience;
use App\Models\JobLanguage;
use App\Models\JobProfessionalArea;
use App\Models\JobSkill;
use App\Models\Photo;
use Illuminate\Database\Seeder;
use App\Models\JobIndustry;

class AdTableSeeder extends Seeder
{
    protected $jobIndustries;

    protected $jobProfessionalAreas;

    public function __construct()
    {
        $this->jobIndustries = JobIndustry::whereIsLeaf()->get()->keyBy('job_industry_id')->keys()->toArray();
        $this->jobProfessionalAreas = JobProfessionalArea::whereIsLeaf()->get()->keyBy('job_professional_area_id')->keys()->toArray();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(AdVacancy::class, 1000)->create()->each(function (AdVacancy $ad) {
            $this->fillModel($ad);
            factory(JobDriver::class, 'vacancy', random_int(0, 5))->create(['ad_id' => $ad->ad_id]);
            $ad->jobIndustries()->attach(Arr::random($this->jobIndustries));
        });
        factory(AdResume::class, 1000)->create()->each(function (AdResume $ad) {
            $this->fillModel($ad);
            factory(JobDriver::class, 'resume', random_int(0, 5))->create(['ad_id' => $ad->ad_id]);
            factory(JobEducation::class, random_int(0, 2))->create(['ad_id' => $ad->ad_id]);
            factory(JobExperience::class, random_int(0, 2))->create(['ad_id' => $ad->ad_id]);
            factory(JobLanguage::class, random_int(0, 2))->create(['ad_id' => $ad->ad_id]);
        });

    }

    /**
     * @param AdVacancy|AdResume $ad
     * @throws Exception
     */
    protected function fillModel(& $ad)
    {

        $ad->ad->photos()->saveMany(factory(Photo::class, random_int(0, 2))->create());

        if ($ad->ad->user_id) {
            $countContact = 1;
            foreach ($ad->ad->user->contacts as $contact) {
                if (random_int(0, 10) < 8) {
                    $ad->ad->contacts()->attach($contact->contact_id, ['order' => $countContact++]);
                }
            }
        }


        $ad->jobProfessionalAreas()->attach(Arr::random($this->jobProfessionalAreas));

        foreach (JobSkill::all(['job_skill_id'])->random(random_int(0,5))->toArray() as $jobSkill) {
            $ad->jobSkills()->attach($jobSkill);
        }
    }

}
