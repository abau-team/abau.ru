<?php

use Illuminate\Database\Seeder;

use App\Models\JobIndustry;

class JobIndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobIndustry::rebuildTree([
            ['name' => 'Автомобильный бизнес', 'code' => 'car_business'],
            ['name' => 'Гостиницы, рестораны, общепит, кейтеринг', 'code' => 'hotels_and_restaurants'],
            ['name' => 'Государственные организации', 'code' => 'governments'],
            ['name' => 'Добывающая отрасль', 'code' => 'mining'],
            ['name' => 'ЖКХ', 'code' => 'communal_services'],
            ['name' => 'Информационные технологии', 'code' => 'information_technology'],
            ['name' => 'Искусство, культура', 'code' => 'art_and_culture'],
            ['name' => 'Лесная промышленность, деревообработка', 'code' => 'timber_and_woodworking'],
            ['name' => 'Медицина, фармацевтика, аптеки', 'code' => 'medicine_pharmacy'],
            ['name' => 'Металлургия, металлообработка', 'code' => 'metallurgy'],
            ['name' => 'Нефть и газ', 'code' => 'oil_and_gas'],
            ['name' => 'Образовательные учреждения', 'code' => 'educational'],
            ['name' => 'Общественная деятельность, НКО', 'code' => 'social_activities'],
            ['name' => 'Перевозки, логистика, склад, ВЭД', 'code' => 'transportation_and_logistics'],
            ['name' => 'Продукты питания', 'code' => 'food'],
            ['name' => 'Промышленное оборудование', 'code' => 'industrial_and_equipment'],
            ['name' => 'Розничная торговля', 'code' => 'retail'],
            ['name' => 'СМИ, маркетинг, реклама', 'code' => 'media_and_marketing'],
            ['name' => 'Сельское хозяйство', 'code' => 'agriculture'],
            ['name' => 'Строительство, недвижимость', 'code' => 'construction_and_real_estate'],
            ['name' => 'Телекоммуникации, связь', 'code' => 'telecommunications'],
            ['name' => 'Услуги для бизнеса', 'code' => 'services_for_business'],
            ['name' => 'Услуги для населения', 'code' => 'public_services'],
            ['name' => 'Финансовый сектор', 'code' => 'financial'],
            ['name' => 'Химическое производство', 'code' => 'chemical_production'],
            ['name' => 'Энергетика', 'code' => 'power_industry'],
        ]);
        /*factory(JobIndustry::class, 20)->create()->each(function ($first) {
            factory(JobIndustry::class, 5)->create(['parent_id' => $first->job_industry_id]);
        });*/
    }
}
