<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Contact;
use App\Models\Token;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1)->create(['name' => 'Антон', 'admin' => 1, 'password' => '$2y$10$biei2Kw7NYgzRNdLFkKEheVIK8hwm8jN/I9bbeRNNuAoaEB1RJ2Au'])->each(function ($user) {
            factory(Contact::class, 1)->create(['user_id' => $user->user_id, 'name' => $user->name, 'type' => Contact::TYPE_PHONE, 'value' => '+7 (905) 011-55-22']);
            factory(Contact::class, 1)->create(['user_id' => $user->user_id, 'name' => $user->name, 'type' => Contact::TYPE_EMAIL, 'value' => 'antgolubev@gmail.com']);
        });
        factory(User::class, 10)->create()->each(function ($user) {
            factory(Contact::class, rand(1, 5))->create(['user_id' => $user->user_id])->each(function ($contact) {
                factory(Token::class, intdiv(random_int(0, 10), 7))->create(['contact_id' => $contact->contact_id]);
            });
        });
    }
}
