<?php

use App\Models\Place;
use Illuminate\Database\Seeder;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Place::rebuildTree([
            ['name' => 'Europe', 'type' => Place::TYPE_CONTINENT, 'code' => 'eu', 'active' => '1', 'children' => [
                ['name' => 'Eastern Europe', 'type' => Place::TYPE_SUBCONTINENT, 'code' => 'eu_east', 'active' => '1', 'children' => [
                    ['name' => 'Russia', 'type' => Place::TYPE_COUNTRY, 'code' => 'ru', 'active' => '1', 'children' => [
                        ['name' => 'Volga Federal District', 'type' => Place::TYPE_FEDERAL_DISTRICT, 'code' => 'ru_pfo', 'active' => '1', 'children' => [
                            ['name' => 'Nizhny Novgorod Oblast', 'type' => Place::TYPE_REGION, 'code' => 'nizhegorodskaya_oblast', 'active' => '1', 'children' => [
                                ['name' => 'Arzamas', 'type' => Place::TYPE_CITY, 'code' => 'arzamas', 'active' => '1'],
                                ['name' => 'Balakhna', 'type' => Place::TYPE_CITY, 'code' => 'balakhna', 'active' => '1'],
                                ['name' => 'Bogorodsk', 'type' => Place::TYPE_CITY, 'code' => 'bogorodsk', 'active' => '1'],
                                ['name' => 'Bor', 'type' => Place::TYPE_CITY, 'code' => 'bor', 'active' => '1'],
                                ['name' => 'Vetluga', 'type' => Place::TYPE_CITY, 'code' => 'vetluga', 'active' => '1'],
                                ['name' => 'Volodarsk', 'type' => Place::TYPE_CITY, 'code' => 'volodarsk', 'active' => '1'],
                                ['name' => 'Vorsma', 'type' => Place::TYPE_CITY, 'code' => 'vorsma', 'active' => '1'],
                                ['name' => 'Vyksa', 'type' => Place::TYPE_CITY, 'code' => 'vyksa', 'active' => '1'],
                                ['name' => 'Gorbatov', 'type' => Place::TYPE_CITY, 'code' => 'gorbatov', 'active' => '1'],
                                ['name' => 'Gorodets', 'type' => Place::TYPE_CITY, 'code' => 'gorodets', 'active' => '1'],
                                ['name' => 'Dzerzhinsk', 'type' => Place::TYPE_CITY, 'code' => 'dzerzhinsk', 'active' => '1'],
                                ['name' => 'Zavolzhye', 'type' => Place::TYPE_CITY, 'code' => 'zavolzhye', 'active' => '1'],
                                ['name' => 'Knyaginino', 'type' => Place::TYPE_CITY, 'code' => 'knyaginino', 'active' => '1'],
                                ['name' => 'Kstovo', 'type' => Place::TYPE_CITY, 'code' => 'kstovo', 'active' => '1'],
                                ['name' => 'Kulebaki', 'type' => Place::TYPE_CITY, 'code' => 'kulebaki', 'active' => '1'],
                                ['name' => 'Lukoyanov', 'type' => Place::TYPE_CITY, 'code' => 'lukoyanov', 'active' => '1'],
                                ['name' => 'Lyskovo', 'type' => Place::TYPE_CITY, 'code' => 'lyskovo', 'active' => '1'],
                                ['name' => 'Navashino', 'type' => Place::TYPE_CITY, 'code' => 'navashino', 'active' => '1'],
                                ['name' => 'Nizhny Novgorod', 'type' => Place::TYPE_CITY, 'capital' => Place::CAPITAL_FEDERAL_DISTRICT, 'code' => 'nizhny_novgorod', 'active' => '1'],
                                ['name' => 'Pavlovo', 'type' => Place::TYPE_CITY, 'code' => 'pavlovo', 'active' => '1'],
                                ['name' => 'Pervomaysk', 'type' => Place::TYPE_CITY, 'code' => 'pervomaysk', 'active' => '1'],
                                ['name' => 'Perevoz', 'type' => Place::TYPE_CITY, 'code' => 'perevoz', 'active' => '1'],
                                ['name' => 'Sarov', 'type' => Place::TYPE_CITY, 'code' => 'sarov', 'active' => '1'],
                                ['name' => 'Semyonov', 'type' => Place::TYPE_CITY, 'code' => 'semyonov', 'active' => '1'],
                                ['name' => 'Sergach', 'type' => Place::TYPE_CITY, 'code' => 'sergach', 'active' => '1'],
                                ['name' => 'Uren', 'type' => Place::TYPE_CITY, 'code' => 'uren', 'active' => '1'],
                                ['name' => 'Chkalovsk', 'type' => Place::TYPE_CITY, 'code' => 'chkalovsk', 'active' => '1'],
                                ['name' => 'Shakhunya', 'type' => Place::TYPE_CITY, 'code' => 'shakhunya', 'active' => '1'],
                            ]]
                        ]
                        ]]
                    ]]
                ],
            ]]], true);
        /*factory(Place::class, 'country', 2)->create()->each(function ($country) {
            factory(Place::class, 'region', 2)->create(['parent_id' => $country->place_id])->each(function ($region) {
                factory(Place::class, 'city', 15)->create(['parent_id' => $region->place_id]);
            });
        });*/
    }
}
