<?php

use Illuminate\Database\Seeder;

use App\Models\JobProfessionalArea;

class JobProfessionalAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobProfessionalArea::rebuildTree([
            ['name' => 'Автомобильный бизнес', 'code' => 'car_business'],
            ['name' => 'Административный персонал', 'code' => 'administrative_staff'],
            ['name' => 'Банки, инвестиции, лизинг', 'code' => 'banks_investments_leasing'],
            ['name' => 'Безопасность', 'code' => 'security'],
            ['name' => 'Бухгалтерия, управленческий учет, финансы предприятия', 'code' => 'accounting_management_accounting_finance_companies'],
            ['name' => 'Высший менеджмент', 'code' => 'top_management'],
            ['name' => 'Государственная служба, некоммерческие организации', 'code' => 'public_service_non_profit'],
            ['name' => 'Добыча сырья', 'code' => 'extraction_of_raw_materials'],
            ['name' => 'Домашний персонал', 'code' => 'domestic_staff'],
            ['name' => 'Закупки', 'code' => 'purchases'],
            ['name' => 'Инсталляция и сервис', 'code' => 'installation_and_service'],
            ['name' => 'Информационные технологии, интернет, телеком', 'code' => 'information_technology_internet_telecom'],
            ['name' => 'Искусство, развлечения, масс-медиа', 'code' => 'arts_entertainment_media'],
            ['name' => 'Консультирование', 'code' => 'counseling'],
            ['name' => 'Маркетинг, реклама, PR', 'code' => 'marketing_advertising_pr'],
            ['name' => 'Медицина, фармацевтика', 'code' => 'medicine_pharma'],
            ['name' => 'Наука, образование', 'code' => 'science_education'],
            ['name' => 'Начало карьеры, студенты', 'code' => 'early_career_students'],
            ['name' => 'Продажи', 'code' => 'sales'],
            ['name' => 'Производство', 'code' => 'production'],
            ['name' => 'Рабочий персонал', 'code' => 'working_staff'],
            ['name' => 'Спортивные клубы, фитнес, салоны красоты', 'code' => 'sports_clubs_fitness_beauty_salons'],
            ['name' => 'Страхование', 'code' => 'insurance'],
            ['name' => 'Строительство, недвижимость', 'code' => 'construction_real_estate'],
            ['name' => 'Транспорт, логистика', 'code' => 'transport_logistics'],
            ['name' => 'Туризм, гостиницы, рестораны', 'code' => 'tourism_hotels_restaurants'],
            ['name' => 'Управление персоналом, тренинги', 'code' => 'hr_management_training'],
            ['name' => 'Юристы', 'code' => 'lawyers'],
        ]);
        /*factory(JobProfessionalArea::class, 20)->create()->each(function ($first) {
            factory(JobProfessionalArea::class, 5)->create(['parent_id' => $first->job_professional_area_id]);
        });*/
    }
}
