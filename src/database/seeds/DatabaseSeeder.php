<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlaceSeeder::class);
        $this->call(JobIndustrySeeder::class);
        $this->call(AdCategorySeeder::class);
        $this->call(JobProfessionalAreaSeeder::class);
        $this->call(JobSkillSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AdTableSeeder::class);
    }
}
