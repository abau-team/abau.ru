<?php

use Illuminate\Database\Seeder;

use App\Models\AdCategory;

class AdCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdCategory::rebuildTree([
            ['name' => 'Job', 'code' => 'job', 'children' => [
                ['name' => 'Vacancy', 'model' => \App\Models\AdVacancy::class, 'code' => 'vacancies'],
                ['name' => 'Resume', 'model' => \App\Models\AdResume::class, 'code' => 'resumes']
            ]]
        ]);
        /*factory(AdCategory::class, 5)->create()->each(function ($first) {
            factory(AdCategory::class, 3)->create(['parent_id' => $first->ad_category_id]);
        });*/
    }
}
