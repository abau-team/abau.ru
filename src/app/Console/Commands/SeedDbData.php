<?php

namespace App\Console\Commands;

use App\Models\AdCategory;
use App\Models\Contact;
use App\Models\JobIndustry;
use App\Models\JobProfessionalArea;
use App\Models\Place;
use App\Models\User;
use DatabaseSeeder;
use Illuminate\Console\Command;

class SeedDbData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed begin data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->seedAdCategory();
        $this->seedJobIndustry();
        $this->seedJobProfessionalArea();
        $this->seedPlace();

        $this->createAdmin();

        $this->info('Seeding complete');
        return true;

    }

    protected function createAdmin()
    {
        $user = User::create([
            'name' => 'Антон',
            'password' => '$2y$10$biei2Kw7NYgzRNdLFkKEheVIK8hwm8jN/I9bbeRNNuAoaEB1RJ2Au'
        ]);
        $user->setAdmin();
        $user->contacts()->saveMany([
            new Contact(['name' => 'Антон', 'type' => Contact::TYPE_PHONE, 'value' => '+7 (905) 011-55-22']),
            new Contact(['name' => 'Антон', 'type' => Contact::TYPE_EMAIL, 'value' => 'antgolubev@gmail.com']),
        ]);
    }

    protected function seedPlace() {
        Place::rebuildTree([
            ['name' => 'Europe', 'type' => Place::TYPE_CONTINENT, 'code' => 'eu', 'active' => '1', 'children' => [
                ['name' => 'Eastern Europe', 'type' => Place::TYPE_SUBCONTINENT, 'code' => 'eu_east', 'active' => '1', 'children' => [
                    ['name' => 'Russia', 'type' => Place::TYPE_COUNTRY, 'code' => 'ru', 'active' => '1', 'children' => [
                        ['name' => 'Volga Federal District', 'type' => Place::TYPE_FEDERAL_DISTRICT, 'code' => 'ru_pfo', 'active' => '1', 'children' => [
                            ['name' => 'Nizhny Novgorod Oblast', 'type' => Place::TYPE_REGION, 'code' => 'nizhegorodskaya_oblast', 'active' => '1', 'children' => [
                                ['name' => 'Arzamas', 'type' => Place::TYPE_CITY, 'code' => 'arzamas', 'active' => '1'],
                                ['name' => 'Balakhna', 'type' => Place::TYPE_CITY, 'code' => 'balakhna', 'active' => '1'],
                                ['name' => 'Bogorodsk', 'type' => Place::TYPE_CITY, 'code' => 'bogorodsk', 'active' => '1'],
                                ['name' => 'Bor', 'type' => Place::TYPE_CITY, 'code' => 'bor', 'active' => '1'],
                                ['name' => 'Vetluga', 'type' => Place::TYPE_CITY, 'code' => 'vetluga', 'active' => '1'],
                                ['name' => 'Volodarsk', 'type' => Place::TYPE_CITY, 'code' => 'volodarsk', 'active' => '1'],
                                ['name' => 'Vorsma', 'type' => Place::TYPE_CITY, 'code' => 'vorsma', 'active' => '1'],
                                ['name' => 'Vyksa', 'type' => Place::TYPE_CITY, 'code' => 'vyksa', 'active' => '1'],
                                ['name' => 'Gorbatov', 'type' => Place::TYPE_CITY, 'code' => 'gorbatov', 'active' => '1'],
                                ['name' => 'Gorodets', 'type' => Place::TYPE_CITY, 'code' => 'gorodets', 'active' => '1'],
                                ['name' => 'Dzerzhinsk', 'type' => Place::TYPE_CITY, 'code' => 'dzerzhinsk', 'active' => '1'],
                                ['name' => 'Zavolzhye', 'type' => Place::TYPE_CITY, 'code' => 'zavolzhye', 'active' => '1'],
                                ['name' => 'Knyaginino', 'type' => Place::TYPE_CITY, 'code' => 'knyaginino', 'active' => '1'],
                                ['name' => 'Kstovo', 'type' => Place::TYPE_CITY, 'code' => 'kstovo', 'active' => '1'],
                                ['name' => 'Kulebaki', 'type' => Place::TYPE_CITY, 'code' => 'kulebaki', 'active' => '1'],
                                ['name' => 'Lukoyanov', 'type' => Place::TYPE_CITY, 'code' => 'lukoyanov', 'active' => '1'],
                                ['name' => 'Lyskovo', 'type' => Place::TYPE_CITY, 'code' => 'lyskovo', 'active' => '1'],
                                ['name' => 'Navashino', 'type' => Place::TYPE_CITY, 'code' => 'navashino', 'active' => '1'],
                                ['name' => 'Nizhny Novgorod', 'type' => Place::TYPE_CITY, 'capital' => Place::CAPITAL_FEDERAL_DISTRICT, 'code' => 'nizhny_novgorod', 'active' => '1'],
                                ['name' => 'Pavlovo', 'type' => Place::TYPE_CITY, 'code' => 'pavlovo', 'active' => '1'],
                                ['name' => 'Pervomaysk', 'type' => Place::TYPE_CITY, 'code' => 'pervomaysk', 'active' => '1'],
                                ['name' => 'Perevoz', 'type' => Place::TYPE_CITY, 'code' => 'perevoz', 'active' => '1'],
                                ['name' => 'Sarov', 'type' => Place::TYPE_CITY, 'code' => 'sarov', 'active' => '1'],
                                ['name' => 'Semyonov', 'type' => Place::TYPE_CITY, 'code' => 'semyonov', 'active' => '1'],
                                ['name' => 'Sergach', 'type' => Place::TYPE_CITY, 'code' => 'sergach', 'active' => '1'],
                                ['name' => 'Uren', 'type' => Place::TYPE_CITY, 'code' => 'uren', 'active' => '1'],
                                ['name' => 'Chkalovsk', 'type' => Place::TYPE_CITY, 'code' => 'chkalovsk', 'active' => '1'],
                                ['name' => 'Shakhunya', 'type' => Place::TYPE_CITY, 'code' => 'shakhunya', 'active' => '1'],
                            ]]
                        ]
                        ]]
                    ]]
                ],
            ]]], true);
    }

    protected function seedJobIndustry() {
        JobIndustry::rebuildTree([
            ['name' => 'Автомобильный бизнес', 'code' => 'car_business'],
            ['name' => 'Гостиницы, рестораны, общепит, кейтеринг', 'code' => 'hotels_and_restaurants'],
            ['name' => 'Государственные организации', 'code' => 'governments'],
            ['name' => 'Добывающая отрасль', 'code' => 'mining'],
            ['name' => 'ЖКХ', 'code' => 'communal_services'],
            ['name' => 'Информационные технологии', 'code' => 'information_technology'],
            ['name' => 'Искусство, культура', 'code' => 'art_and_culture'],
            ['name' => 'Лесная промышленность, деревообработка', 'code' => 'timber_and_woodworking'],
            ['name' => 'Медицина, фармацевтика, аптеки', 'code' => 'medicine_pharmacy'],
            ['name' => 'Металлургия, металлообработка', 'code' => 'metallurgy'],
            ['name' => 'Нефть и газ', 'code' => 'oil_and_gas'],
            ['name' => 'Образовательные учреждения', 'code' => 'educational'],
            ['name' => 'Общественная деятельность, НКО', 'code' => 'social_activities'],
            ['name' => 'Перевозки, логистика, склад, ВЭД', 'code' => 'transportation_and_logistics'],
            ['name' => 'Продукты питания', 'code' => 'food'],
            ['name' => 'Промышленное оборудование', 'code' => 'industrial_and_equipment'],
            ['name' => 'Розничная торговля', 'code' => 'retail'],
            ['name' => 'СМИ, маркетинг, реклама', 'code' => 'media_and_marketing'],
            ['name' => 'Сельское хозяйство', 'code' => 'agriculture'],
            ['name' => 'Строительство, недвижимость', 'code' => 'construction_and_real_estate'],
            ['name' => 'Телекоммуникации, связь', 'code' => 'telecommunications'],
            ['name' => 'Услуги для бизнеса', 'code' => 'services_for_business'],
            ['name' => 'Услуги для населения', 'code' => 'public_services'],
            ['name' => 'Финансовый сектор', 'code' => 'financial'],
            ['name' => 'Химическое производство', 'code' => 'chemical_production'],
            ['name' => 'Энергетика', 'code' => 'power_industry'],
        ]);
    }

    protected function seedAdCategory() {
        AdCategory::rebuildTree([
            ['name' => 'Job', 'code' => 'job', 'children' => [
                ['name' => 'Vacancy', 'model' => \App\Models\AdVacancy::class, 'code' => 'vacancies'],
                ['name' => 'Resume', 'model' => \App\Models\AdResume::class, 'code' => 'resumes']
            ]]
        ]);
    }

    protected function seedJobProfessionalArea() {
        JobProfessionalArea::rebuildTree([
            ['name' => 'Автомобильный бизнес', 'code' => 'car_business'],
            ['name' => 'Административный персонал', 'code' => 'administrative_staff'],
            ['name' => 'Банки, инвестиции, лизинг', 'code' => 'banks_investments_leasing'],
            ['name' => 'Безопасность', 'code' => 'security'],
            ['name' => 'Бухгалтерия, управленческий учет, финансы предприятия', 'code' => 'accounting_management_accounting_finance_companies'],
            ['name' => 'Высший менеджмент', 'code' => 'top_management'],
            ['name' => 'Государственная служба, некоммерческие организации', 'code' => 'public_service_non_profit'],
            ['name' => 'Добыча сырья', 'code' => 'extraction_of_raw_materials'],
            ['name' => 'Домашний персонал', 'code' => 'domestic_staff'],
            ['name' => 'Закупки', 'code' => 'purchases'],
            ['name' => 'Инсталляция и сервис', 'code' => 'installation_and_service'],
            ['name' => 'Информационные технологии, интернет, телеком', 'code' => 'information_technology_internet_telecom'],
            ['name' => 'Искусство, развлечения, масс-медиа', 'code' => 'arts_entertainment_media'],
            ['name' => 'Консультирование', 'code' => 'counseling'],
            ['name' => 'Маркетинг, реклама, PR', 'code' => 'marketing_advertising_pr'],
            ['name' => 'Медицина, фармацевтика', 'code' => 'medicine_pharma'],
            ['name' => 'Наука, образование', 'code' => 'science_education'],
            ['name' => 'Начало карьеры, студенты', 'code' => 'early_career_students'],
            ['name' => 'Продажи', 'code' => 'sales'],
            ['name' => 'Производство', 'code' => 'production'],
            ['name' => 'Рабочий персонал', 'code' => 'working_staff'],
            ['name' => 'Спортивные клубы, фитнес, салоны красоты', 'code' => 'sports_clubs_fitness_beauty_salons'],
            ['name' => 'Страхование', 'code' => 'insurance'],
            ['name' => 'Строительство, недвижимость', 'code' => 'construction_real_estate'],
            ['name' => 'Транспорт, логистика', 'code' => 'transport_logistics'],
            ['name' => 'Туризм, гостиницы, рестораны', 'code' => 'tourism_hotels_restaurants'],
            ['name' => 'Управление персоналом, тренинги', 'code' => 'hr_management_training'],
            ['name' => 'Юристы', 'code' => 'lawyers'],
        ]);
    }
}
