<?php

namespace App\Mail;

use App\Models\Contact;
use App\Models\Token;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactRecoveryEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Token
     */
    public $token;

    /**
     * @var Contact
     */
    public $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Token $token, Contact $contact)
    {
        $this->token = $token;
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->contact->value, $this->contact->name)
            ->subject(__('contact.email.recovery.subject'))
            ->markdown('email.emailRecovery');
    }
}
