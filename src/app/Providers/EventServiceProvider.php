<?php

namespace App\Providers;

use App\Events\ContactNew;
use App\Events\ContactRecovery;
use App\Listeners\SendRecoveryNotification;
use App\Listeners\SendVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ContactNew::class => [
            SendVerificationNotification::class,
        ],
        ContactRecovery::class => [
            SendRecoveryNotification::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
