<?php

namespace App;

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class AdminMenuFilter implements FilterInterface
{
    /**
     * @param array $item
     * @param Builder $builder
     * @return mixed
     */
    public function transform($item, Builder $builder)
    {
        if (isset($item['translate'])) {
            $item['text'] = __($item['translate']);
        }

        return $item;
    }
}