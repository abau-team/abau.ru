<?php

namespace App\Listeners;

use App\Events\ContactRecovery;
use App\Mail\ContactRecoveryEmail;
use App\Models\Contact;
use Mail;

class SendRecoveryNotification
{

    /**
     * Handle the event.
     *
     * @param ContactRecovery $event
     * @return void
     */
    public function handle(ContactRecovery $event)
    {
        $token = $event->contact->createRecoveryToken();
        if ($event->contact->type == Contact::TYPE_PHONE) {
            // TODO добавить отправку ссылки на телефон
            //$event->contact->activated_at = now();
        } elseif ($event->contact->type == Contact::TYPE_EMAIL) {
            Mail::send(new ContactRecoveryEmail($token, $event->contact));
        }
    }
}
