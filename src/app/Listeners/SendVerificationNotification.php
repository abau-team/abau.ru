<?php

namespace App\Listeners;

use App\Events\ContactNew;
use App\Models\Contact;
use App\Mail\VerificationEmail;
use Mail;

class SendVerificationNotification
{

    /**
     * Handle the event.
     *
     * @param ContactNew $event
     * @return void
     */
    public function handle(ContactNew $event)
    {
        if ($event->contact->type == Contact::TYPE_PHONE) {
            // TODO добавить отправку ссылки на телефон
            $event->contact->activated_at = now();
        } elseif ($event->contact->type == Contact::TYPE_EMAIL) {
            $token = $event->contact->createActivationToken();
            Mail::send(new VerificationEmail($token, $event->contact));
        }
    }
}
