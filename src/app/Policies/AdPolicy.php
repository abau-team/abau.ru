<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Ad;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->is_admin) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the ad.
     *
     * @param User $user
     * @param Ad $ad
     * @return mixed
     */
    public function update(User $user, Ad $ad)
    {
        return $user->user_id == $ad->user_id;
    }

    public function admin(User $user, Ad $ad)
    {
        return false;
    }

}
