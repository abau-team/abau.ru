<?php

namespace App\Http\Requests\Admin;

use App\Models\Place;
use App\Http\Requests\AbstractRequest;
use Illuminate\Validation\Rule;

class PlaceStore extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:100',
            'code' => 'required|min:2|max:40|unique:places,code',
            'type' => [
                'required',
                Rule::in(array_flip(Place::getTypes()))
            ],
            'capital' => [
                Rule::in(array_flip(Place::getCapitals()))
            ],
            'active' => 'boolean'
        ];
    }
}
