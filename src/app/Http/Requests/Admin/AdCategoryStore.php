<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\AbstractRequest;

class AdCategoryStore extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:50',
            'model' => 'max:50',
            'code' => 'required|min:2|max:40|unique:ad_categories,code',
            'active' => 'boolean'
        ];
    }
}
