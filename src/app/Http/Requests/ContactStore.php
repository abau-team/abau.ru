<?php

namespace App\Http\Requests;

use App\Models\Contact;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class ContactStore extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:40',
            'type' => ['required', Rule::in(array_flip(Contact::getTypes()))],
            'value' => 'required'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->sometimes('value', 'email', function ($input) {
            return $input->type == Contact::TYPE_EMAIL;
        });
        $validator->sometimes('value', 'regex:/^\+[0-9]{1,4}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/', function ($input) {
            return $input->type == Contact::TYPE_PHONE;
        });
    }
}
