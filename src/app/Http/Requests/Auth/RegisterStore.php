<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\AbstractRequest;

class RegisterStore extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:40',
            'email' => 'required|email|unique:contacts,value',
            'phone' => 'required|unique:contacts,value|regex:/^\+[0-9]{1,4}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/',
            'password' => 'required|min:2|max:40|confirmed',
            'term-agree' => 'accepted'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('user.registerForm.validation.name.required'),
            'name.min' => __('user.registerForm.validation.name.min'),
            'name.max' => __('user.registerForm.validation.name.max'),

            'email.required' => __('user.registerForm.validation.email.required'),
            'email.unique' => __('user.registerForm.validation.email.unique'),
            'email.email' => __('user.registerForm.validation.email.email'),

            'phone.required' => __('user.registerForm.validation.phone.required'),
            'phone.unique' => __('user.registerForm.validation.phone.unique'),
            'phone.regex' => __('user.registerForm.validation.phone.regex'),

            'password.required' => __('user.registerForm.validation.password.required'),
            'password.min' => __('user.registerForm.validation.password.min'),
            'password.max' => __('user.registerForm.validation.password.max'),
            'password.confirmed' => __('user.registerForm.validation.password.confirmed'),

            'term-agree.accepted' => __('user.registerForm.validation.term-agree.accepted'),
        ];
    }

}
