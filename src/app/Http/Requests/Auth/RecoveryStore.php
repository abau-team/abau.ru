<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\AbstractRequest;
use App\Models\Contact;

/**
 * Class RecoveryStore
 * @package App\Http\Requests\Auth
 *
 * @property-read string $login
 */
class RecoveryStore extends AbstractRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'login.required' => __('user.recovery-form.validation.login.required'),
            'login.string' => __('user.recovery-form.validation.login.string'),
            'login.exist' => __('user.recovery-form.validation.login.exist'),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function prepareForValidation()
    {
        $login = $this->request->get('login');
        if (!filter_var($login, FILTER_VALIDATE_EMAIL)) {
            $this->request->set('login', Contact::preparePhone($login));
        }
    }
}
