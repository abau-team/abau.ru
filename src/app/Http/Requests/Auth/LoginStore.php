<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\AbstractRequest;
use App\Models\Contact;

/**
 * Class LoginStore
 * @package App\Http\Requests\Auth
 *
 * @property-read string $login
 * @property-read string $password
 * @property-read string $remember
 */
class LoginStore extends AbstractRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required|string',
            'password' => 'required|string',
            'remember' => 'boolean',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'login.required' => __('user.login-form.validation.login.required'),
            'login.string' => __('user.login-form.validation.login.string'),
            'login.exist' => __('user.login-form.validation.login.exist'),

            'password.required' => __('user.login-form.validation.password.required'),
            'password.string' => __('user.login-form.validation.password.string'),

            'remember.boolean' => __('user.login-form.validation.remember.boolean'),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function prepareForValidation()
    {
        if ($this->request->get('remember') == 'on')
            $this->request->set('remember', true);

        $login = $this->request->get('login');
        if (!filter_var($login, FILTER_VALIDATE_EMAIL)) {
            $this->request->set('login', Contact::preparePhone($login));
        }
    }
}
