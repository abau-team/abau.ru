<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\AbstractRequest;

/**
 * Class ResetStore
 * @package App\Http\Requests\Auth
 *
 * @property-read string $password
 */
class ResetStore extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:2|max:40|confirmed',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.required' => __('user.reset-form.validation.password.required'),
            'password.min' => __('user.reset-form.validation.password.min'),
            'password.max' => __('user.reset-form.validation.password.max'),
            'password.confirmed' => __('user.reset-form.validation.password.confirmed'),
        ];
    }

}
