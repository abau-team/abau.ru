<?php

namespace App\Http\Requests;

use App\Models\Ad;
use App\Models\AdCategory;
use App\Models\AdVacancy;
use App\Models\Contact;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

/**
 * Class AdStore
 * @package App\Http\Requests
 *
 * @property string|null $name
 * @property string|null $email
 * @property string|null $phone
 *
 * @property integer $ad_category_id
 * @property integer $place_id
 * @property integer $currency
 *
 * @property string $title
 * @property string $description
 *
 * @property integer|null $job_industry
 * @property integer|null $job_professional_area
 * @property integer|null $schedule
 * @property integer|null $education
 * @property integer|null $experience
 * @property array $driver_licence
 * @property string $job_skill
 * @property string|null $schedule_comment
 * @property boolean|null $automobile
 * @property boolean|null $sex
 * @property float|null $salary_from
 * @property float|null $salary
 * @property float|null $salary_to
 * @property boolean|null $non_resident
 * @property string|null $address
 * @property integer $age_from
 * @property integer $age_to
 * @property boolean|null $remote_work
 * @property boolean|null $want_register
 * @property UploadedFile[]|null $photo
 */
class AdStore extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:2|max:40',
            'email' => 'email',
            'phone' => 'regex:/^\+[0-9]{1,4}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/',


            'ad_category_id' => 'required|exists:ad_categories,ad_category_id',
            'place_id' => 'required|exists:places,place_id',

            'currency' => [
                'required',
                Rule::in(array_flip(Ad::currencies()))
            ],

            'title' => 'required|min:3|max:70|string',
            'description' => 'required|string',


            'job_industry' => 'exists:job_industries,job_industry_id',
            'job_professional_area' => 'exists:job_professional_areas,job_professional_area_id',
            'job_skill' => 'string|nullable',
            'schedule' => [
                Rule::in(array_flip(AdVacancy::getSchedules()))
            ],
            'education' => [
                Rule::in(array_flip(AdVacancy::getEducation()))
            ],
            'experience' => [
                Rule::in(array_flip(AdVacancy::getExperiences()))
            ],
            'schedule_comment' => 'string|max:255|nullable',
            'automobile' => 'boolean',
            'sex' => [
                Rule::in(array_flip(AdVacancy::getSexes()))
            ],
            'salary_from' => 'numeric',
            'salary' => 'numeric',
            'salary_to' => 'numeric',
            'non_resident' => 'boolean',
            'address' => 'max:255',
            'age_from' => 'between:14,100|lte:age_to',
            'age_to' => 'between:14,100',
            'photos.*' => 'file|mimes:jpg,jpeg,png|max:5000',

            'day_of_birth' => 'numeric|between:1,31',
            'month_of_birth' => 'numeric|between:1,12',
            'year_of_birth' => 'numeric|between:1930,2006',
            'date_of_birth' => 'date',

            'remote_work' => 'boolean',
            'want_register' => 'boolean',
            'driver_licence.*' => 'boolean',
            

        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->sometimes(['email', 'phone'], 'required', function () {
            return !auth()->check();
        });

        $validator->sometimes(['name', 'job_professional_area_id', 'schedule', 'education', 'experience'], 'required', function ($input) {
            $category = AdCategory::orWhere('code', '=', ['vacancies', 'resumes'])->get(['ad_category_id'])->toArray();
            return in_array($input->category, $category);
        });

        $validator->sometimes(['job_industry_id', 'address'], 'required', function ($input) {
            $category = AdCategory::where('code', '=', ['vacancies'])->get(['ad_category_id'])->toArray();
            return in_array($input->category, $category);
        });

        $validator->sometimes(['first_name', 'last_name', 'date_of_birth', 'day_of_birth', 'month_of_birth', 'year_of_birth', 'sex'], 'required', function ($input) {
            $category = AdCategory::where('code', '=', ['resumes'])->get(['ad_category_id'])->toArray();
            return in_array($input->category, $category);
        });

        $validator->sometimes(['photo'], 'max:10', function ($input) {
            $category = AdCategory::where('code', '=', ['vacancies'])->get(['ad_category_id'])->toArray();
            return in_array($input->category, $category);
        });

        $validator->sometimes(['photo'], 'max:3', function ($input) {
            $category = AdCategory::where('code', '=', ['resumes'])->get(['ad_category_id'])->toArray();
            return in_array($input->category, $category);
        });
    }

    /**
     * @inheritDoc
     */
    protected function prepareForValidation()
    {
        if ($this->request->get('remote_work') == 'on')
            $this->request->set('remote_work', true);

        if ($this->request->get('want_register') == 'on')
            $this->request->set('want_register', true);

        if ($this->request->get('non_resident') == 'on')
            $this->request->set('non_resident', true);

        if ($this->request->get('automobile') == 'on')
            $this->request->set('automobile', true);

        $driverLicenses = $this->request->get('driver_licence');
        if ($driverLicenses) {
            foreach ($driverLicenses as $key => $driverLicence) {
                $driverLicenses[$key] = true;
            }
            $this->request->set('driver_licence', $driverLicenses);
        }

        if ($this->request->has('day_of_birth') && $this->request->has('month_of_birth') && $this->request->has('year_of_birth')) {
            $day = $this->request->get('day_of_birth');
            $month = $this->request->get('month_of_birth');
            $year = $this->request->get('year_of_birth');

            $this->request->set('date_of_birth', "{$year}-{$month}-{$day}");
        }

        // Prepare phone to validation
        $this->request->set('phone', Contact::preparePhone($this->request->get('phone')));
    }
}