<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractRequest extends FormRequest {

    /**
     * @return array
     */
    abstract public function rules();

    public function onlyInRules()
    {
        $only = [];
        foreach ($this->rules() as $key => $rule) {
            $only[] = $key;
            if (is_array($rule) && in_array('confirmed', $rule)) {
                $only[] = $key . '_confirmation';
            }
            if(is_string($rule) && preg_match('/confirmed/', $rule) > 0) {
                $only[] = $key . '_confirmation';
            }
        }
        return $this->only($only);
    }

}