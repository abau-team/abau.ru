<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;

class SetDefaultPlaceForUrls
{
    public function handle(Request $request, Closure $next)
    {
        URL::defaults(['place' => $request->route('place') ?? session('place', 'nizhny_novgorod')]);

        return $next($request);
    }
}