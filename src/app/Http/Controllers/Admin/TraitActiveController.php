<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\RedirectResponse;

trait TraitActiveController
{
    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function setActive(int $id)
    {
        $this->findModel($id)->update(['active' => 1]);

        return back()->with('success', __('admin.flash.status-s'));

    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function setUnActive(int $id)
    {
        $this->findModel($id)->update(['active' => 0]);

        return back()->with('success', __('admin.flash.status-s'));
    }
}