<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Artisan;
use Illuminate\Http\RedirectResponse;

class IndexController extends Controller
{

    public function dashboard() {
        return view('admin.dashboard');
    }

    /**
     * @return RedirectResponse
     */
    public function clearCache() {
        Artisan::call('cache:clear');
        return redirect()->route('admin.home')->with('success', __('admin.flash.cache-s'));
    }

}
