<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use DataTables;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdController extends Controller
{

    /**
     * @param Request $request
     * @return Factory|JsonResponse|View
     * @throws \Exception
     */
    public function listAll(Request $request)
    {


        if ($request->ajax()) {

            $query = Ad::with(['user'])->select();
            $status = $request->get('status');
            $place = $request->get('place_id');
            $user = $request->get('user_id');
            $category = $request->get('ad_category_id');

            switch ($status) {
                case 'banned':
                    $query->whereBanned();
                    break;
                case 'deleted':
                    $query->whereRemoved();
                    break;
                case 'all':
                    break;
                default:
                    $query->active();
                    break;
            }

            if ($place) {
                $query->wherePlaceId($place);
            }

            if ($user) {
                $query->whereUserId($user);
            }

            if ($category) {
                $query->whereAdCategoryId($category);
            }


            return DataTables::eloquent($query)
                ->addColumn('place', function (Ad $ad) {
                    if ($ad->place) {
                        $route = route('admin.ad.listAll', ['place_id' => $ad->place_id]);
                        return "<a href=\"{$route}\">{$ad->place->name}</a>";
                    } else {
                        return '';
                    }
                })->editColumn('user_id', function (Ad $ad) {
                    if ($ad->user_id) {
                        $route = route('admin.ad.listAll', ['user_id' => $ad->user_id]);
                        return "<a href=\"{$route}\">{$ad->user_id}</a>";
                    } else {
                        return '';
                    }
                })->editColumn('view_contact', function (Ad $ad) {
                    return "{$ad->view_contact} / {$ad->views}";
                })->editColumn('ad_category_id', function (Ad $ad) {
                    $route = route('admin.ad.listAll', ['ad_category_id' => $ad->ad_category_id]);
                    return "<a href=\"{$route}\">{$ad->category->name}</a>";
                })->editColumn('refreshed_at', function (Ad $ad) {
                    return view('admin.ad._refreshButton', ['ad' => $ad]);
                })->editColumn('blocked_at', function (Ad $ad) {
                    return view('admin.ad._blockedButton', ['ad' => $ad]);
                })->editColumn('deleted_at', function (Ad $ad) {
                    return view('admin.ad._deletedButton', ['ad' => $ad]);
                })->addColumn('buttons', function (Ad $ad) {
                    return view('admin.ad._buttons', ['ad' => $ad]);
                })->rawColumns(['place', 'user_id', 'view_contact', 'ad_category_id', 'refreshed_at', 'blocked_at', 'deleted_at', 'buttons'])
                ->make(true);

        }

        return view('admin.ad.listAll');
    }

    public function createForm()
    {
        return view('admin.ad.createForm');
    }

    public function createFormVacancy()
    {
        return view('admin.ad.createFormVacancy');
    }

    public function createFormResume()
    {
        return view('admin.ad.createFormResume');
    }

}