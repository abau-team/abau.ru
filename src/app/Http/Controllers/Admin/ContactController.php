<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContactStore;
use App\Models\Contact;
use App\Models\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class ContactController extends Controller
{

    /**
     * @param int $id
     * @return Contact|Collection
     */
    protected function findModel(int $id): Contact
    {
        return Contact::findOrFail($id);
    }

    /**
     * Display active contacts
     *
     * @return Factory|View
     */
    public function listActive()
    {
        $models = Contact::with(['user', 'ads', 'tokens'])->active()->paginate(50);
        return view('admin.contact.listActive', ['models' => $models]);
    }

    public function listAll()
    {
        $models = Contact::with(['user', 'ads', 'tokens'])->paginate(50);
        return view('admin.contact.listAll', ['models' => $models]);
    }

    public function listUnActivated()
    {
        $models = Contact::with(['user', 'ads', 'tokens'])->whereNotActivate()->paginate(50);
        return view('admin.contact.listUnActivated', ['models' => $models]);
    }

    public function listBanned()
    {
        $models = Contact::with(['user', 'ads', 'tokens'])->whereBanned()->paginate(50);
        return view('admin.contact.listBanned', ['models' => $models]);
    }

    public function listDeleted()
    {
        $models = Contact::with(['user', 'ads', 'tokens'])->WhereRemoved()->paginate(50);
        return view('admin.contact.listDeleted', ['models' => $models]);
    }

    public function listByUser(int $id)
    {
        $user = User::findOrFail($id);
        $models = Contact::with(['user', 'ads', 'tokens'])->whereUserId($user->user_id)->paginate(50);
        return view('admin.contact.listByUser', [
            'models' => $models,
            'user' => $user
        ]);
    }

    public function createForm(int $id)
    {
        $user = User::findOrFail($id);
        return view('admin.contact.createForm', [
            'user' => $user
        ]);
    }

    public function create(ContactStore $request, int $id)
    {
        User::findOrFail($id)->contacts()->create($request->onlyInRules());
        return redirect()->route('admin.contact.listActive')->with('success', __('admin.flash.create-s'));
    }

    public function show(int $id)
    {
        $model = $this->findModel($id);
        return view('admin.contact.show', [
            'model' => $model
        ]);
    }

    public function updateForm(int $id)
    {
        $model = $this->findModel($id);
        return view('admin.contact.updateForm', [
            'model' => $model
        ]);
    }

    public function update(ContactStore $request, int $id)
    {
        $this->findModel($id)->update($request->onlyInRules());
        return redirect()->route('admin.contact.listActive')->with('success', __('admin.flash.update-s'));
    }

    public function delete(int $id)
    {
        $this->findModel($id)->markDeleted();
        return redirect()->route('admin.contact.listActive')->with('success', __('admin.flash.delete-s'));
    }

    public function unDelete(int $id)
    {
        $this->findModel($id)->markUnDeleted();
        return redirect()->route('admin.contact.listActive')->with('success', __('admin.flash.update-s'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(int $id)
    {
        $this->findModel($id)->delete();
        return redirect()->route('admin.contact.listActive')->with('success', __('admin.flash.update-s'));
    }

    public function ban(int $id)
    {
        $this->findModel($id)->ban();
        return redirect()->route('admin.contact.listActive')->with('success', __('admin.flash.update-s'));
    }

    public function unBan(int $id)
    {
        $this->findModel($id)->unBan();
        return redirect()->route('admin.contact.listActive')->with('success', __('admin.flash.update-s'));
    }

    public function activate(int $id)
    {
        $this->findModel($id)->activate();
        return redirect()->route('admin.contact.listActive')->with('success', __('admin.flash.update-s'));
    }

    public function deactivate(int $id)
    {
        $this->findModel($id)->deactivate();
        return redirect()->route('admin.contact.listActive')->with('success', __('admin.flash.update-s'));
    }
}
