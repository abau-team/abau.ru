<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use App\Models\Token;
use App\Models\User;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class TokenController extends Controller
{
    /**
     * Show token's list
     *
     * @return Factory|View
     */
    public function listAll()
    {
        $models = Token::with(['contact'])->paginate(50);

        return view('admin.token.listAll', [
            'models' => $models
        ]);
    }

    /**
     * Show token's list by user
     *
     * @param $id
     * @return Factory|View
     */
    public function listByUser($id)
    {
        $user = User::findOrFail($id);
        $models = Token::leftJoin('contacts', 'tokens.contact_id', '=', 'contacts.contact_id')
            ->where(['contacts.user_id' => $id])->paginate(50);

        return view('admin.token.listByUser', [
            'models' => $models,
            'user' => $user,
        ]);
    }

    /**
     * Show token's list by contact
     *
     * @param $id
     * @return Factory|View
     */
    public function listByContact($id)
    {
        $contact = Contact::findOrFail($id);
        $models = Token::with(['contact'])->whereContactId($id)->paginate(50);

        return view('admin.token.listByContact', [
            'models' => $models,
            'contact' => $contact
        ]);
    }

    /**
     * Delete token
     *
     * @param int $type
     * @param string $value
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(int $type, string $value)
    {
        Token::whereType($type)->whereValue($value)->firstOrFail()->delete();
        return redirect()->route('admin.token.listAll')->with('success', __('admin.flash.update-s'));
    }
}
