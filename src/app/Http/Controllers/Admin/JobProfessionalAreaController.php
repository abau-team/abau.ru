<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\JobProfessionalAreaStore;
use App\Models\JobProfessionalArea;
use Illuminate\Http\RedirectResponse;

class JobProfessionalAreaController extends NestedController
{

    /**
     * @var string
     */
    protected $primaryKey = "job_professional_area_id";

    /**
     * @var string
     */
    protected $entity = "job-professional-area";

    /**
     * @var JobProfessionalArea
     */
    protected $className = JobProfessionalArea::class;

    /**
     * @var string
     */
    protected $view = 'admin.nested-list';

    /**
     * @var string
     */
    protected $route = 'admin.job-professional-area';

    /**
     * @param JobProfessionalAreaStore $request
     * @return RedirectResponse
     */
    public function create(JobProfessionalAreaStore $request)
    {
        JobProfessionalArea::create($request->all());
        return redirect()->route($this->route . ".listAll")->with('success', __('admin.flash.create-s'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param JobProfessionalAreaStore $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(JobProfessionalAreaStore $request, int $id)
    {
        $model = $this->findModel($id);
        $model->update($request->all());
        return redirect()->route($this->route . '.listAll')->with('success', __('admin.flash.update-s'));
    }
}
