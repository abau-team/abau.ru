<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\JobIndustryStore;
use App\Models\JobIndustry;
use Illuminate\Http\RedirectResponse;

class JobIndustryController extends NestedController
{

    /**
     * @var string
     */
    protected $primaryKey = "job_industry_id";

    /**
     * @var string
     */
    protected $entity = "job-industry";

    /**
     * @var JobIndustry
     */
    protected $className = JobIndustry::class;

    /**
     * @var string
     */
    protected $view = 'admin.nested-list';

    /**
     * @var string
     */
    protected $route = 'admin.job-industry';


    /**
     * @param JobIndustryStore $request
     * @return RedirectResponse
     */
    public function create(JobIndustryStore $request)
    {
        JobIndustry::create($request->all());
        return redirect()->route($this->route . ".listAll")->with('success', __('admin.flash.create-s'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param JobIndustryStore $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(JobIndustryStore $request, int $id)
    {
        $model = $this->findModel($id);
        $model->update($request->all());
        return redirect()->route($this->route . '.listAll')->with('success', __('admin.flash.update-s'));
    }

}
