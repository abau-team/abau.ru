<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

abstract class NestedController extends Controller
{

    /**
     * @var string
     */
    protected $primaryKey;

    /**
     * @var string
     */
    protected $entity;

    /**
     * @var Model
     */
    protected $className;

    /**
     * @var string
     */
    protected $view;

    /**
     * @var string
     */
    protected $route;

    /**
     * @param int $id
     * @return Model
     */
    protected function findModel(int $id)
    {
        return $this->className::findOrFail($id);
    }

    /**
     * @param int $id
     * @return array
     */
    protected function getDataForMoveForm(int $id)
    {
        return $this->className::dataForDropdown($id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function listAll()
    {
        $models = $this->className::defaultOrder()->get()->toTree();

        return view($this->view . ".listAll", [
            'models' => $models,
            'entity' => $this->entity,
            'route' => $this->route,
            'primaryKey' => $this->primaryKey
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createForm()
    {
        return view($this->view . ".createForm", [
            'entity' => $this->entity,
            'route' => $this->route
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id)
    {
        $model = $this->findModel($id);
        return view($this->view . ".show", [
            'model' => $model,
            'entity' => $this->entity,
            'route' => $this->route,
            'primaryKey' => $this->primaryKey
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function updateForm(int $id)
    {
        $model = $this->findModel($id);
        return view($this->view . ".updateForm", [
            'model' => $model,
            'entity' => $this->entity,
            'route' => $this->route,
            'primaryKey' => $this->primaryKey
        ]);
    }

    /**
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $this->findModel($id)->delete();
        return redirect()->route($this->route . ".listAll")->with('success', __('admin.flash.delete-s'));
    }

    /**
     * @param int $id
     * @param int $step
     * @return RedirectResponse
     */
    public function shiftDown(int $id, int $step = null)
    {
        if ($this->findModel($id)->down($step)) {
            return back()->with('success', __('admin.flash.move-s'));
        }
        return back()->with('error', __('admin.flash.move-e'));
    }

    /**
     * @param int $id
     * @param int $step
     * @return RedirectResponse
     */
    public function shiftUp(int $id, int $step = null)
    {
        if ($this->findModel($id)->up($step)) {
            return back()->with('success', __('admin.flash.move-s'));
        }
        return back()->with('error', __('admin.flash.move-e'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function makeRoot(int $id)
    {
        $this->findModel($id)->saveAsRoot();
        return back()->with('success', __('admin.flash.move-s'));
    }

    public function moveForm(int $id)
    {
        $model = $this->findModel($id);
        $data = $this->getDataForMoveForm($id);
        return view($this->view . ".moveForm", [
            'model' => $model,
            'data' => $data,
            'entity' => $this->entity,
            'route' => $this->route,
            'primaryKey' => $this->primaryKey
        ]);
    }


    /**
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function move(int $id, Request $request)
    {
        $model = $this->findModel($id);
        $request->validate([
            'parent_id' => "required|exists:{$this->className::getTableName()},{$this->className::getPrKey()}"
        ]);
        $parent = $this->findModel($request->parent_id);
        $parent->appendNode($model);
        return redirect()->route($this->route . ".listAll")->with('success', __('admin.flash.move-s'));
    }
}