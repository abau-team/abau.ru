<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AdminUserWithoutPasswordStore;
use App\Http\Requests\PasswordStore;
use App\Models\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests\Admin\AdminUserStore;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * @param $id
     * @return User|Collection
     */
    protected function findModel($id): User
    {
        return User::findOrFail($id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function listActive()
    {
        $models = User::with(['place', 'contacts', 'tokens', 'ads'])->active()->paginate(50);

        return view('admin.user.listActive', [
            'models' => $models
        ]);
    }

    /**
     * Display a listing of the all resource.
     *
     * @return Factory|View
     */
    public function listAll()
    {
        $models = User::with(['place', 'contacts', 'tokens', 'ads'])->paginate(50);

        return view('admin.user.listAll', [
            'models' => $models
        ]);
    }

    /**
     * Display a listing of the banned resource.
     *
     * @return Factory|View
     */
    public function listBanned()
    {
        $models = User::with(['place', 'contacts', 'tokens', 'ads'])->whereBanned()->paginate(50);

        return view('admin.user.listBanned', [
            'models' => $models
        ]);
    }

    /**
     * Display a listing of the deleted resource.
     *
     * @return Factory|View
     */
    public function listDeleted()
    {
        $models = User::with(['place', 'contacts', 'tokens', 'ads'])->whereRemoved()->paginate(50);

        return view('admin.user.listDeleted', [
            'models' => $models
        ]);
    }

    public function createForm()
    {
        return view('admin.user.createForm');
    }

    public function create(AdminUserStore $request)
    {
        User::create($request->onlyInRules());
        return redirect()->route('admin.user.listActive')->with('success', __('admin.flash.create-s'));
    }

    public function show(int $id)
    {
        $model = $this->findModel($id);
        return view('admin.user.show', [
            'model' => $model
        ]);
    }

    public function updateForm(int $id)
    {
        $model = $this->findModel($id);
        return view('admin.user.updateForm', [
            'model' => $model
        ]);
    }

    public function update(AdminUserWithoutPasswordStore $request, int $id)
    {
        $this->findModel($id)->update($request->onlyInRules());
        return redirect()->route('admin.user.listActive')->with('success', __('admin.flash.update-s'));
    }

    public function passwordForm(int $id)
    {
        $model = $this->findModel($id);
        return view('admin.user.passwordForm', [
            'model' => $model
        ]);
    }

    public function password(PasswordStore $request, int $id)
    {
        $model = $this->findModel($id);
        $data = $request->onlyInRules();
        $model->setPassword($data['password']);
        $model->save();
        return redirect()->route('admin.user.listActive')->with('success', __('admin.flash.password-s'));
    }

    public function delete(int $id)
    {
        $this->findModel($id)->markDeleted();
        return redirect()->route('admin.user.listActive')->with('success', __('admin.flash.delete-s'));
    }

    public function unDelete(int $id)
    {
        $this->findModel($id)->markUnDeleted();
        return redirect()->route('admin.user.listActive')->with('success', __('admin.flash.unDelete-s'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(int $id)
    {
        $this->findModel($id)->delete();
        return redirect()->route('admin.user.listActive')->with('success', __('admin.flash.destroy-s'));
    }

    public function ban(int $id)
    {
        $this->findModel($id)->ban();
        return redirect()->route('admin.user.listActive')->with('success', __('admin.flash.ban-s'));
    }

    public function unBan(int $id)
    {
        $this->findModel($id)->unBan();
        return redirect()->route('admin.user.listActive')->with('success', __('admin.flash.unBan-s'));
    }
}
