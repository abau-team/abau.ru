<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AdCategoryStore;
use App\Models\AdCategory;
use Illuminate\Http\RedirectResponse;

class AdCategoryController extends NestedController
{

    use TraitActiveController;

    /**
     * @var string
     */
    protected $primaryKey = "ad_category_id";

    /**
     * @var string
     */
    protected $entity = "ad-category";

    /**
     * @var AdCategory
     */
    protected $className = AdCategory::class;

    /**
     * @var string
     */
    protected $view = 'admin.nested-list';

    /**
     * @var string
     */
    protected $route = 'admin.ad-category';


    /**
     * @param AdCategoryStore $request
     * @return RedirectResponse
     */
    public function create(AdCategoryStore $request)
    {
        AdCategory::create($request->all());
        return redirect()->route($this->route . ".listAll")->with('success', __('admin.flash.create-s'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdCategoryStore $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(AdCategoryStore $request, int $id)
    {
        $model = $this->findModel($id);
        $model->update($request->all());
        return redirect()->route($this->route . '.listAll')->with('success', __('admin.flash.update-s'));
    }
}
