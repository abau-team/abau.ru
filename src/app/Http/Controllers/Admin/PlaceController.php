<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PlaceStore;
use App\Models\Place;
use Illuminate\Http\RedirectResponse;

class PlaceController extends NestedController
{

    use TraitActiveController;

    /**
     * @var string
     */
    protected $primaryKey = "place_id";

    /**
     * @var string
     */
    protected $entity = "place";

    /**
     * @var Place
     */
    protected $className = Place::class;

    /**
     * @var string
     */
    protected $view = 'admin.nested-list';

    /**
     * @var string
     */
    protected $route = 'admin.place';


    /**
     * @param PlaceStore $request
     * @return RedirectResponse
     */
    public function create(PlaceStore $request)
    {
        $this->className::create($request->all());
        return redirect()->route($this->route . ".listAll")->with('success', __('admin.flash.create-s'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PlaceStore $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(PlaceStore $request, int $id)
    {
        $model = $this->findModel($id);
        $model->update($request->all());
        return redirect()->route($this->route . '.listAll')->with('success', __('admin.flash.update-s'));
    }

}
