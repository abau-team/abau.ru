<?php

namespace App\Http\Controllers\Auth;

use App\Events\ContactNew;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterStore;
use App\Models\User;
use Hash;
use Illuminate\Http\RedirectResponse;

class RegisterController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function registerForm()
    {
        return view('auth.registerForm');
    }

    /**
     * @param RegisterStore $request
     * @return RedirectResponse
     */
    public function register(RegisterStore $request)
    {
        $user = User::create([
            'name' => $request->name,
            'password' => Hash::make($request->password),
        ]);

        $email = $user->addContactEmail($request->email);
        $phone = $user->addContactPhone($request->phone);

        event(new ContactNew($email));
        event(new ContactNew($phone));

        return redirect()->route('loginForm')->with('success', __('user.registerForm.flash-success'));

    }

}
