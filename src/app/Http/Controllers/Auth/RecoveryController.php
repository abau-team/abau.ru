<?php

namespace App\Http\Controllers\Auth;

use App\Events\ContactRecovery;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RecoveryStore;
use App\Models\Contact;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class RecoveryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @return Factory|View
     */
    public function recoveryForm() {
        return view('auth.recoveryForm');
    }

    /**
     * @param RecoveryStore $request
     * @return RedirectResponse
     */
    public function recovery(RecoveryStore $request) {
        $contact = Contact::whereValue($request->login)->first();

        if ($contact == null) {
            // Отправляем для исключения перебора логинов
            return redirect()->route('loginForm')->with('success', __('user.recovery-form.flash-success'));
        }

        if ($contact->user->isBanned || $contact->isBanned) {
            return redirect()->route('loginForm')->with('error', __('user.recovery-form.flash-error-blocked'));
        }

        event(new ContactRecovery($contact));

        return redirect()->route('loginForm')->with('success', __('user.recovery-form.flash-success'));



    }
}
