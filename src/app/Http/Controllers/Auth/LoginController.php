<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginStore;
use App\Models\Contact;
use Hash;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    use ThrottlesLogins;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function loginForm()
    {
        return view('auth.loginForm');
    }

    public function login(LoginStore $request)
    {

        $contact = Contact::whereValue($request->login)->first();

        if ($contact == null) {
            throw ValidationException::withMessages([
                'login' => [__('user.login-form.validation.login.error')],
            ]);
        }
        $user = $contact->user;

        if (Hash::check($request->password, $user->password)) {
            Auth::login($user, $request->remember);
            return redirect()->route('home');
        } else {
            throw ValidationException::withMessages([
                'login' => [__('user.login-form.validation.login.error')],
            ]);
        }

    }


}
