<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Token;
use Exception;
use Illuminate\Http\RedirectResponse;

class ActivateController extends Controller
{

    /**
     * @param string $key
     * @return RedirectResponse
     * @throws Exception
     */
    public function activateEmail(string $key)
    {
        $token = Token::whereValue($key)->first();

        if ($token) {
            $token->contact->activated_at = now();
            $token->delete();
            return redirect()->route('loginForm')->with('success', __('contact.flash.activate-s'));
        } else {
            return redirect()->route('loginForm')->with('error', __('contact.flash.activate-f'));
        }

    }

}