<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResetStore;
use App\Models\Token;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ResetPasswordController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param string $key
     * @return Factory|RedirectResponse|View
     */
    public function resetForm(string $key) {
        $token = $this->findToken($key);

        if ($token == null) {
            return redirect()->route('loginForm')->with('error', __('user.reset-form.flash-error-no'));
        }

        if ($token->is_expired) {
            return redirect()->route('loginForm')->with('error', __('user.reset-form.flash-error-expired'));
        }

        return view('auth.resetForm', ['key' => $key]);
    }

    /**
     * @param ResetStore $request
     * @param string $key
     * @return RedirectResponse
     * @throws Exception
     */
    public function reset(ResetStore $request, string $key) {
        $token = $this->findToken($key);
        $user = $token->contact->user;
        $user->setPassword($request->password);
        if ($user->save() && $token->delete()) {
            return redirect()->route('loginForm')->with('success', __('user.reset-form.flash-success'));
        }
        return abort(500);

    }

    /**
     * @param string $key
     * @return Token
     */
    protected function findToken(string $key):Token {
        return Token::whereValue($key)->first();
    }
}
