<?php

namespace App\Http\Controllers\Ad;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use App\Models\AdCategory;
use App\Models\Place;

class ViewController extends Controller
{

    public function show(string $place, string $category, int $id)
    {

        $model = Ad::with(['photos', 'user'])
            ->leftJoin('places', 'ads.place_id', '=', 'places.place_id')
            ->leftJoin('ad_categories', 'ads.ad_category_id', '=', 'ad_categories.ad_category_id')
            ->where([
                'places.code' => $place,
                'ad_categories.code' => $category
            ])
            ->findOrFail($id);

        session(['place' => $place]);

        $model->increment('views');

        return view("ad.view.{$category}", ['model' => $model]);
        /*if ($model->category->code == 'vacancies') {
            return view('ad.view.vacancy', ['model' => $model->adVacancy]);
        } elseif ($model->category->code == 'resumes') {
            return view('ad.view.resume', ['model' => $model->adResume]);
        }*/
    }

    /**
     * @param string $place
     * @return Place
     */
    protected function checkPlace(string $place): Place
    {
        return Place::whereCode($place)->firstOrFail();
    }

    /**
     * @param string $category
     * @return AdCategory
     */
    protected function checkCategory(string $category): AdCategory
    {
        return AdCategory::whereCode($category)->firstOrFail();
    }

}