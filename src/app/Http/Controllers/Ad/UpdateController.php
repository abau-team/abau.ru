<?php

namespace App\Http\Controllers\Ad;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdStore;
use App\Models\Ad;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\View;

class UpdateController extends Controller {

    /**
     * @param int $id
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function updateForm(int $id)
    {
        $model = $this->findAd($id);
        $this->authorize('update', $model);

        return view("ad.update.{$model->category->code}");
    }

    public function update(AdStore $request, $id) {

    }


    /**
     * @param int $id
     * @return Ad|Ad[]|Collection|Model
     */
    protected function findAd(int $id)
    {
        return Ad::findOrFail($id);
    }
}