<?php

namespace App\Http\Controllers\Ad;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;

class ManageController extends Controller
{

    /**
     * @param $id
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function delete(int $id)
    {
        $model = $this->findAd($id);
        $this->authorize('update', $model);
        $model->markDeleted();
        return back();
    }

    /**
     * @param int $id
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function unDelete(int $id)
    {
        $model = $this->findAd($id);
        $this->authorize('update', $model);
        $model->markUnDeleted();
        return back();
    }

    /**
     * @param $id
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function block(int $id)
    {
        $model = $this->findAd($id);
        $this->authorize('admin', $model);
        $model->ban();
        return back();
    }

    /**
     * @param int $id
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function unblock(int $id) {
        $model = $this->findAd($id);
        $this->authorize('admin', $model);
        $model->unBan();
        return back();
    }

    /**
     * @param int $id
     * @return Ad|Ad[]|Collection|Model
     */
    protected function findAd(int $id)
    {
        return Ad::findOrFail($id);
    }

}