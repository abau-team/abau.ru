<?php

namespace App\Http\Controllers\Ad;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdStore;
use App\Models\Ad;
use App\Models\AdCategory;
use App\Models\Place;
use Exception;
use Illuminate\Http\RedirectResponse;

class CreateController extends Controller
{

    public function showForm(string $category = null)
    {

        if ($category == 'resume') {
            return view('ad.create.resumes');
        } elseif ($category == 'vacancy') {
            return view('ad.create.vacancies');
        } elseif ($category == null) {
            return view('ad.create.index');
        } else {
            abort(404);
        }
    }

    public function showResumeForm()
    {

        return view('ad.create-resume');
    }

    public function showVacancyForm()
    {

        return view('ad.create-vacancy');
    }

    /**
     * @param AdStore $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function create(AdStore $request)
    {
        $model = new Ad();
        $model->initialSave($request);
        return redirect()->route('ad.view.full', ['id' => $model->ad_id, 'category' => $model->category->code]);
    }

    /**
     * @param string $category
     * @return AdCategory
     */
    protected function checkCategory(string $category): AdCategory
    {
        return AdCategory::whereCode($category)->firstOrFail();
    }

}