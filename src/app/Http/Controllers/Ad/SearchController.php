<?php

namespace App\Http\Controllers\Ad;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use App\Models\AdCategory;
use App\Models\Place;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Http\Request;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;

class SearchController extends Controller
{
    protected $perPage = 10;
    protected $paginationLabel = 'page';

    public function my(Request $request)
    {
        $ads = Ad::whereUserId(auth()->id())->with(['photos', 'category']);

        $q = $request->get('q');

        if ($q) {
            $q = str_replace(['\\', '%', '_'], ['\\\\', '\%', '\_'], $q);
            $ads->where(function (Builder $query) use ($q) {
                $query->where('title', 'like', "%{$q}%")
                    ->orWhere('description', 'like', "%{$q}%");
            });
        }

        $place = $request->get('place');
        if ($place) {
            $ads->leftJoin('places', 'ads.place_id', '=', 'places.place_id')
                ->where(['places.code' => $place]);
        }

        $category = $request->get('category');
        if ($category) {
            $ads->leftJoin('ad_categories', 'ads.ad_category_id', '=', 'ad_categories.ad_category_id')
                ->where(['ad_categories.code' => $category]);
        }

        return view('ad.search.my', ['ads' => $ads->paginate($this->perPage)]);
    }

    /**
     * @param Request $request
     * @param string $place
     * @return Factory|RedirectResponse|View
     */
    public function all(Request $request, string $place)
    {
        if ($request->has('place') || $request->has('category')) {
            return $this->checkRedirect($request, $place);
        }


        $place = $this->checkPlace($place);

        $ads = Ad::wherePlaceId($place->place_id)
            ->with(['photos', 'category']);

        $q = $request->get('q');

        if ($q) {
            $q = str_replace(['\\', '%', '_'], ['\\\\', '\%', '\_'], $q);
            $ads->where(function (Builder $query) use ($q) {
                $query->where('title', 'like', "%{$q}%")
                    ->orWhere('description', 'like', "%{$q}%");
            });
        }

        return view('ad.search.index', ['ads' => $ads->paginate($this->perPage), 'place' => $place]);
    }

    /**
     * @param Request $request
     * @param string $place
     * @param string $category
     * @return Factory|RedirectResponse|View
     */
    public function category(Request $request, string $place, string $category)
    {
        if ($request->has('place') || $request->has('category')) {
            return $this->checkRedirect($request, $place, $category);
        }

        $placeModel = $this->checkPlace($place);
        $categoryModel = $this->checkCategory($category);

        $ads = Ad::whereAdCategoryId($categoryModel->ad_category_id)
            ->wherePlaceId($placeModel->place_id)
            ->with(['photos']);

        $q = $request->get('q');

        if ($q) {
            $q = str_replace(['\\', '%', '_'], ['\\\\', '\%', '\_'], $q);
            $ads->where(function (Builder $query) use ($q) {
                $query->where('title', 'like', "%{$q}%")
                    ->orWhere('description', 'like', "%{$q}%");
            });
        }

        if ($category == 'resumes') {
            $ads = $this->resumes($request, $ads);
        } elseif ($category = 'vacancies') {
            $ads = $this->vacancies($request, $ads);
        } else {
            abort(404);
        }

        //$ads = $this->addPagination($ads->get());

        return view('ad.search.list', ['ads' => $ads->paginate($this->perPage), 'place' => $placeModel, 'category' => $categoryModel]);
    }

    /**
     * @param Request $request
     * @param Builder $ads
     * @return Builder
     */
    protected function resumes(Request $request, Builder $ads): Builder
    {
        $ads->leftJoin('ad_resumes', 'ads.ad_id', '=', 'ad_resumes.ad_id');

        $jobProfessionalArea = $request->get('job_professional_area');
        if ($jobProfessionalArea) {
            $ads->leftJoin('ads_job_professional_areas', 'ads.ad_id', '=', 'ads_job_professional_areas.ad_id')
                ->leftJoin('job_professional_areas', 'job_professional_areas.job_professional_area_id', '=', 'ads_job_professional_areas.job_professional_area_id')
                ->where(['job_professional_areas.code' => $jobProfessionalArea]);
        }

        $schedule = $request->get('schedule');
        if ($schedule) {
            $ads->where(['ad_resumes.schedule' => $schedule]);
        }

        $employment = $request->get('employment');
        if ($employment) {
            $ads->where(['ad_resumes.employment' => $employment]);
        }

        $sex = $request->get('sex');
        if ($sex) {
            $ads->where(['ad_resumes.sex' => $sex]);
        }

        $automobile = $request->get('automobile');
        if ($automobile) {
            $ads->where('ad_resumes.automobile', '>', 0);
        }

        $salaryTo = $request->get('salary');
        if ($salaryTo) {
            $ads->where(DB::raw("(CASE WHEN ads.currency = " . Ad::CURRENCY_DOLLAR . " THEN ad_resumes.salary * 65 WHEN ads.currency = " . Ad::CURRENCY_EURO . " THEN ad_resumes.salary * 75 ELSE ad_resumes.salary END)"), '<=', $salaryTo);
        }

        $salaryNotNull = $request->get('salaryNotNull');
        if ($salaryNotNull) {
            $ads->where('ad_resumes.salary', '>', 0);
        }

        return $ads->with(['adResume']);
    }


    /**
     * @param Request $request
     * @param Builder|Ad $ads
     * @return Builder|Ad
     */
    protected function vacancies(Request $request, Builder $ads): Builder
    {
        $ads->with(['adVacancy'])->leftJoin('ad_vacancies', 'ads.ad_id', '=', 'ad_vacancies.ad_id');

        $jobIndustry = $request->get('job_industry');
        if ($jobIndustry) {
            $ads->leftJoin('ads_job_industries', 'ads.ad_id', '=', 'ads_job_industries.ad_id')
                ->leftJoin('job_industries', 'job_industries.job_industry_id', '=', 'ads_job_industries.job_industry_id')
                ->where(['job_industries.code' => $jobIndustry]);
        }

        $jobProfessionalArea = $request->get('job_professional_area');
        if ($jobProfessionalArea) {
            $ads->leftJoin('ads_job_professional_areas', 'ads.ad_id', '=', 'ads_job_professional_areas.ad_id')
                ->leftJoin('job_professional_areas', 'job_professional_areas.job_professional_area_id', '=', 'ads_job_professional_areas.job_professional_area_id')
                ->where(['job_professional_areas.code' => $jobProfessionalArea]);
        }

        $schedule = $request->get('schedule');
        if ($schedule) {
            $ads->where(['ad_vacancies.schedule' => $schedule]);
        }

        $employment = $request->get('employment');
        if ($employment) {
            $ads->where(['ad_vacancies.employment' => $employment]);
        }

        $experience = $request->get('experience');
        if ($experience) {
            $ads->where('ad_vacancies.experience', '<=', $experience);
        }

        $remoteWork = $request->get('remote_work');
        if ($remoteWork) {
            $ads->whereNotNull('ad_vacancies.remote_work');
        }

        $salaryFrom = $request->get('salary');
        if ($salaryFrom) {
            $ads->where(function (Builder $query) use ($salaryFrom) {
                $query->where(DB::raw("(CASE WHEN ads.currency = " . Ad::CURRENCY_DOLLAR . " THEN ad_vacancies.salary_from * 65 WHEN ads.currency = " . Ad::CURRENCY_EURO . " THEN ad_vacancies.salary_from * 75 ELSE ad_vacancies.salary_from END)"), '>=', $salaryFrom)
                    ->orWhere(DB::raw("(CASE WHEN ads.currency = " . Ad::CURRENCY_DOLLAR . " THEN ad_vacancies.salary_to * 65 WHEN ads.currency = " . Ad::CURRENCY_EURO . " THEN ad_vacancies.salary_to * 75 ELSE ad_vacancies.salary_to END)"), '>=', $salaryFrom);
            });
        }

        return $ads;
    }

    protected function addPagination(Collection $ads)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage($this->paginationLabel);
        return new LengthAwarePaginator($ads->forPage($currentPage, $this->perPage), count($ads), $this->perPage, $currentPage, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
            'pageName' => $this->paginationLabel,
        ]);
    }

    /**
     * @param Request $request
     * @param string|null $place
     * @param string|null $category
     * @return RedirectResponse
     */
    protected function checkRedirect(Request $request, string $place = null, string $category = null)
    {
        $data = $request->all();

        $data['place'] = $request->get('place', $place);
        $data['category'] = $request->get('category', $category);

        if ($data['place'] && $data['category']) {
            return redirect()->route('ad.searchCategory', $data);
        } else {
            return redirect()->route('ad.search', $data);
        }


    }

    /**
     * @param string $place
     * @return Place
     */
    protected function checkPlace(string $place): Place
    {
        $place = Place::whereCode($place)->firstOrFail();
        session(['place' => $place->code]);
        return $place;
    }

    /**
     * @param string $category
     * @return AdCategory
     */
    protected function checkCategory(string $category): AdCategory
    {
        return AdCategory::whereCode($category)->firstOrFail();
    }

}