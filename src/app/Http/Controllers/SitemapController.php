<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\AdCategory;
use App\Models\Place;

class SitemapController extends Controller
{

    protected $pages = [];

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function xml()
    {
        $pages = cache('sitemap-pages', function () {

            $this->generatePages();
            $this->generatePlaces();
            $this->generateAds();

            $pages = $this->pages;
            cache(['sitemap-pages' => $pages], 15 * 60);
            return $pages;
        });

        $response = view('sitemap.xml', ['pages' => $pages]);

        return response($response)->header('Content-Type', 'text/xml');
    }

    protected function generatePages()
    {
        $this->pages = array_merge($this->pages, [
            [
                'title' => 'Главная',
                'loc' => route('home'),
                'lastmod' => date('c', filemtime(__DIR__ . "/../../../resources/views/welcome.blade.php")),
                'changefreq' => 'monthly',
                'priority' => '1.0'
            ],
            [
                'title' => 'Правила',
                'loc' => route('term'),
                'lastmod' => date('c', filemtime(__DIR__ . "/../../../resources/views/term/index.blade.php")),
                'changefreq' => 'monthly',
                'priority' => '0.4'
            ],
            [
                'title' => 'Регистрация',
                'loc' => route('registerForm'),
                'lastmod' => date('c', filemtime(__DIR__ . "/../../../resources/views/auth/registerForm.blade.php")),
                'changefreq' => 'monthly',
                'priority' => '0.1'
            ],
            [
                'title' => 'Авторизация',
                'loc' => route('loginForm'),
                'lastmod' => date('c', filemtime(__DIR__ . "/../../../resources/views/auth/loginForm.blade.php")),
                'changefreq' => 'monthly',
                'priority' => '0.1'
            ],
            [
                'title' => 'Восстановление доступа',
                'loc' => route('recoveryForm'),
                'lastmod' => date('c', filemtime(__DIR__ . "/../../../resources/views/auth/recoveryForm.blade.php")),
                'changefreq' => 'monthly',
                'priority' => '0.1'
            ],
        ]);
    }

    protected function generatePlaces()
    {
        // TODO: Переделать
        $places = Place::whereIsLeaf()->orderBy('capital', 'desc')->orderBy('_lft')->get();
        $categories = AdCategory::whereIsLeaf()->orderBy('_lft')->get();
        foreach ($places as $place) {
            /** @var Place $place */

            $ad = Ad::wherePlaceId($place->place_id)->active()->first('created_at');
            if ($ad) {

                $this->pages[] = [
                    'title' => trans("ad.ad-search.index.seo.title", ['place_name' => $place->name_prepositional]),
                    'loc' => route('ad.search', ['place' => $place->code]),
                    'lastmod' => $ad->created_at->tz('Europe/Moscow')->toAtomString(),
                    'changefreq' => 'daily',
                    'priority' => '0.3',
                ];

                foreach ($categories as $category) {
                    /** @var AdCategory $category */
                    $ad = Ad::wherePlaceId($place->place_id)->whereAdCategoryId($category->ad_category_id)->active()->first('created_at');
                    if ($ad) {

                        $this->pages[] = [
                            'title' => trans("ad.ad-search.{$category->code}.seo.title", ['place_name' => $place->name_prepositional]),
                            'loc' => route('ad.searchCategory', ['place' => $place->code, 'category' => $category->code]),
                            'lastmod' => $ad->created_at->tz('Europe/Moscow')->toAtomString(),
                            'changefreq' => 'daily',
                            'priority' => '0.3',
                        ];
                    }
                }
            }

        }
    }

    protected function generateAds()
    {
        $ads = Ad::active()->get();
        foreach ($ads as $ad) {
            $this->pages[] = [
                'title' => $ad->title,
                'loc' => $ad->url,
                'lastmod' => $ad->created_at->tz('Europe/Moscow')->toAtomString(),
                'changefreq' => 'monthly',
                'priority' => '0.1',
            ];
        }
    }


}
