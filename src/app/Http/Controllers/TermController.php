<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;

class TermController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('term.index');
    }
}
