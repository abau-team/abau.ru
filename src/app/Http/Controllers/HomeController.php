<?php

namespace App\Http\Controllers;

use App\Models\Place;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $places = Place::whereIsLeaf()->orderBy('capital', 'desc')->orderBy('_lft')->get();

        return view('welcome', ['places' => $places]);
    }
}
