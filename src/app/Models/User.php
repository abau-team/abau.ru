<?php

namespace App\Models;

use App\Models\Scopes\OrderCreatedDescScope;
use App\Models\Traits\TraitBan;
use App\Models\Traits\TraitDelete;
use App\Models\Traits\TraitIp;
use Eloquent;
use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany as HasManyAlias;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Request;

/**
 * App\User
 *
 * @property int $user_id
 * @property string $name
 * @property string $password
 * @property int|null $place_id
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $last_active_at
 * @property string|null $blocked_at
 * @property string|null $deleted_at
 * @property integer|null $admin
 * @property string $ip
 * @property-read bool $isDeleted
 * @property Place|null $place
 * @property Contact[]|null $contacts
 * @property Contact[]|null $phones
 * @property Contact[]|null $emails
 * @property Ad[]|null $ads
 * @property Token[]|null $tokens
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereBlockedAt($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLastActiveAt($value)
 * @method static Builder|User wherePasswordHash($value)
 * @method static Builder|User wherePlaceId($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User active()
 * @mixin Eloquent
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereUserId($value)
 * @property-read bool $ip_string
 * @property-read bool $is_banned
 * @property-read bool $is_deleted
 * @method static Builder|User whereBanned()
 * @method static Builder|User whereIp($value)
 * @method static Builder|User whereNotBanned()
 * @method static Builder|User whereNotRemoved()
 * @method static Builder|User whereRemoved()
 * @property-read bool $is_admin
 * @method static Builder|User whereAdmin($value)
 */
class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;
    use Notifiable, TraitBan, TraitDelete, TraitIp;

    protected $primaryKey = "user_id";

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'place_id', 'password',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'deleted_at' => 'datetime',
        'blocked_at' => 'datetime',
        'last_active_at' => 'datetime'
    ];

    /**
     * Scope a query to only include active users.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive($query)
    {
        return $query->where([
            'deleted_at' => null,
            'blocked_at' => null,
        ]);
    }

    /**
     * @return bool
     */
    public function getIsAdminAttribute()
    {
        return $this->admin == 1;
    }

    /**
     * @return bool
     */
    public function setAdmin()
    {
        $this->admin = 1;
        return $this->save();
    }

    /**
     * @return bool
     */
    public function setNotAdmin()
    {
        $this->admin = null;
        return $this->save();
    }

    /**
     * @param $text
     */
    public function setPassword($text)
    {
        $this->password = Hash::make($text);
    }

    /**
     * @param $text
     * @return bool
     */
    public function checkPassword($text)
    {
        return Hash::check($text, $this->password);
    }

    /**
     * Get the place for the user
     *
     * @return Place|BelongsTo|null
     */
    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id');
    }

    /**
     * Get the contacts for the user
     *
     * @return Contact[]|HasManyAlias|null
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class, 'user_id');
    }

    /**
     * @return Contact[]|HasManyAlias|null
     */
    public function phones()
    {
        return $this->contacts()->where('type', '=', Contact::TYPE_PHONE);
    }

    /**
     * @return HasManyAlias|Contact[]|null
     */
    public function emails()
    {
        return $this->contacts()->where('type', '=', Contact::TYPE_EMAIL);
    }

    /**
     * @return Ad[]|HasManyAlias|null
     */
    public function ads()
    {
        return $this->hasMany(Ad::class, 'user_id');
    }

    /**
     * @return Token[]|HasManyThrough|null
     */
    public function tokens()
    {
        return $this->hasManyThrough(Token::class, Contact::class, 'user_id', 'contact_id', 'user_id', 'contact_id');
    }

    /**
     * @param string $value
     * @param string|null $name
     * @return Contact|Model
     */
    public function addContactEmail(string $value, string $name = null)
    {
        return $this->contacts()->create([
            'type' => Contact::TYPE_EMAIL,
            'name' => $name ?? $this->name,
            'value' => $value
        ]);
    }


    /**
     * @param string $value
     * @param string|null $name
     * @return Contact|Model
     */
    public function addContactPhone(string $value, string $name = null)
    {
        return $this->contacts()->create([
            'type' => Contact::TYPE_PHONE,
            'name' => $name ?? $this->name,
            'value' => $value
        ]);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrderCreatedDescScope());

        static::creating(function (User $model) {
            $model->created_at = $model->created_at ?? now();
            $model->ip = $model->ip ?? inet_pton(Request::ip());
        });
    }
}
