<?php

namespace App\Models;

use App\Models\Interfaces\HasAdGeneral;
use App\Models\Traits\AdGeneralTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class JobLanguage
 *
 * @package App\Models
 * @property int $language
 * @property int $level
 * @property-read string $language_label
 * @property-read string $level_label
 * @property int $ad_id
 * @property-read Ad $ad
 * @method static Builder|JobLanguage newModelQuery()
 * @method static Builder|JobLanguage newQuery()
 * @method static Builder|JobLanguage query()
 * @method static Builder|JobLanguage whereAdId($value)
 * @method static Builder|JobLanguage whereLanguage($value)
 * @method static Builder|JobLanguage whereLevel($value)
 * @mixin Eloquent
 */
class JobLanguage extends Model implements
    HasAdGeneral
{
    use AdGeneralTrait;

    protected $primaryKey = 'ad_id';
    public $incrementing = false;

    public $timestamps = false;

    const LANGUAGE_RUSSIAN = 1;

    const LEVEL_NATIVE = 1;

    protected $fillable = ['language', 'level'];

    public static function languages()
    {
        return [
            static::LANGUAGE_RUSSIAN => __('job-language.language.russian'),
        ];
    }

    /**
     * @return string
     */
    public function getLanguageLabelAttribute(): string
    {
        $languages = static::languages();
        return $languages[$this->language] ?? null;
    }

    public static function levels()
    {
        return [
            static::LEVEL_NATIVE => __('job-language.level.native'),
        ];
    }

    /**
     * @return string
     */
    public function getLevelLabelAttribute(): string
    {
        $levels = static::languages();
        return $levels[$this->level] ?? null;
    }
}
