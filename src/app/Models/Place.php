<?php

namespace App\Models;

use App\Models\Interfaces\HasLocalName;
use App\Models\Traits\NestedTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany as HasManyAlias;
use Kalnoy\Nestedset\Collection;
use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use Str;

/**
 * App\Place
 *
 * @property-read Collection|Place[] $children
 * @property-read Place $parent
 * @property-write mixed $parent_id
 * @method static Builder|Place d()
 * @method static QueryBuilder|Place newModelQuery()
 * @method static QueryBuilder|Place newQuery()
 * @method static QueryBuilder|Place query()
 * @mixin \Eloquent
 * @property int $place_id
 * @property string $name
 * @property-read string $local_name
 * @property-read string $capital_label
 * @property-read string $type_label
 * @property-read string $url_index
 * @property int $type
 * @property int $capital
 * @property string $code
 * @property int $active
 * @property int $_lft
 * @property int $_rgt
 * @property-read string $typeLabel
 * @method static Builder|Place whereActive($value)
 * @method static Builder|Place whereName($value)
 * @method static Builder|Place whereCode($value)
 * @method static Builder|Place wherePlaceId($value)
 * @method static Builder|Place whereLft($value)
 * @method static Builder|Place whereParentId($value)
 * @method static Builder|Place whereRgt($value)
 * @method static Builder|Place whereCapital($value)
 * @method static Builder|Place whereType($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read string $name_genitive
 * @property-read string $name_nominative
 * @property-read string $name_prepositional
 */
class Place extends Model implements
    HasLocalName
{
    use NodeTrait, NestedTrait;

    protected $primaryKey = 'place_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    const TYPE_CONTINENT = 1;
    const TYPE_SUBCONTINENT = 5;
    const TYPE_COUNTRY = 10;
    const TYPE_FEDERAL_DISTRICT = 20;
    const TYPE_REGION = 30;
    const TYPE_MUNICIPAL_DISTRICT = 40;
    const TYPE_CITY = 50;
    const TYPE_CITY_DISTRICT = 60;

    const CAPITAL_CONTINENT = 1;
    const CAPITAL_SUBCONTINENT = 5;
    const CAPITAL_COUNTRY = 10;
    const CAPITAL_FEDERAL_DISTRICT = 20;
    const CAPITAL_REGION = 30;
    const CAPITAL_MUNICIPAL_DISTRICT = 40;
    const CAPITAL_NONE = null;

    /**
     * Назначаем именна константам TYPE
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_CONTINENT => __('place.types.continent'),
            self::TYPE_SUBCONTINENT => __('place.types.subcontinent'),
            self::TYPE_COUNTRY => __('place.types.country'),
            self::TYPE_FEDERAL_DISTRICT => __('place.types.federal-district'),
            self::TYPE_REGION => __('place.types.region'),
            self::TYPE_MUNICIPAL_DISTRICT => __('place.types.municipal-district'),
            self::TYPE_CITY => __('place.types.city'),
            self::TYPE_CITY_DISTRICT => __('place.types.city-district')
        ];
    }

    /**
     * Получаем имя спрятанное за константой TYPE
     * @param string $default значение по умолчанию, если ничего не найдется
     * @return string
     */
    public function getTypeLabelAttribute($default = null)
    {
        $types = self::getTypes();
        return isset($types[$this->type]) ? $types[$this->type] : $default;
    }

    /**
     * Назначаем именна константам CAPITAL
     * @return array
     */
    public static function getCapitals()
    {
        return [
            self::CAPITAL_NONE => __('place.capital.none'),
            self::CAPITAL_CONTINENT => __('place.capital.continent'),
            self::CAPITAL_SUBCONTINENT => __('place.capital.subcontinent'),
            self::CAPITAL_COUNTRY => __('place.capital.country'),
            self::CAPITAL_FEDERAL_DISTRICT => __('place.capital.federal-district'),
            self::CAPITAL_REGION => __('place.capital.region'),
            self::CAPITAL_MUNICIPAL_DISTRICT => __('place.capital.municipal-district'),
        ];
    }

    /**
     * Получаем имя спрятанное за константой CAPITAL
     * @param string $default значение по умолчанию, если ничего не найдется
     * @return string
     */
    public function getCapitalLabelAttribute($default = null)
    {
        $capitals = self::getCapitals();
        return isset($capitals[$this->capital]) ? $capitals[$this->capital] : $default;
    }

    /**
     * @return string
     */
    public function getLocalNameAttribute()
    {
        return __("place.places.$this->name.nominative");
    }

    /**
     * @return string
     */
    public function getNameNominativeAttribute()
    {
        return __("place.places.$this->name.nominative");
    }

    /**
     * @return string
     */
    public function getNameGenitiveAttribute()
    {
        return __("place.places.$this->name.genitive");
    }

    /**
     * @return string
     */
    public function getNamePrepositionalAttribute()
    {
        return __("place.places.$this->name.prepositional");
    }

    /**
     * Url index page
     *
     * @return string
     */
    public function getUrlIndexAttribute()
    {
        return route('ad.search', ['place' => $this->code]);
    }

    /**
     * @var array
     */
    protected $fillable = ['type', 'name', 'active', 'code', 'capital'];

    /**
     * @var array
     */
    protected $attributes = [
        'active' => false,
        'capital' => self::CAPITAL_NONE,
        'type' => self::TYPE_CITY,
    ];

    /**
     * Get users for the place
     *
     * @return HasManyAlias
     */
    public function users()
    {
        return $this->hasMany(User::class, 'place_id');
    }

    public static function boot()
    {
        parent::boot();

        static::saving(function (Place $model) {
            $model->code = $model->code ?? Str::slug($model->name);
        });
    }

}