<?php

namespace App\Models;

use App\Models\Interfaces\HasAdGeneral;
use App\Models\Interfaces\HasDriveLicensesAttribute;
use App\Models\Interfaces\HasJobIndustry;
use App\Models\Interfaces\HasJobProfessionalArea;
use App\Models\Interfaces\HasJobSkills;
use App\Models\Interfaces\HasPriceAttribute;
use App\Models\Traits\AdGeneralTrait;
use App\Models\Traits\DriverAttributeTrait;
use App\Models\Traits\JobIndustryTrait;
use App\Models\Traits\JobProfessionalAreaTrait;
use App\Models\Traits\JobSkillsTrait;
use App\Models\Traits\WorkTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdVacancy
 *
 * @property int $ad_id
 * @property string $name
 * @property int $schedule
 * @property int $employment
 * @property string $schedule_comment
 * @property int $experience
 * @property int $education
 * @property int|null $need_automobile
 * @property int|null $sex
 * @property int|null $age_from
 * @property int|null $age_to
 * @property int|null $non_resident
 * @property string $address
 * @method static Builder|AdVacancy newModelQuery()
 * @method static Builder|AdVacancy newQuery()
 * @method static Builder|AdVacancy query()
 * @method static Builder|AdVacancy whereAdId($value)
 * @method static Builder|AdVacancy whereAddress($value)
 * @method static Builder|AdVacancy whereAgeFrom($value)
 * @method static Builder|AdVacancy whereAgeTo($value)
 * @method static Builder|AdVacancy whereName($value)
 * @method static Builder|AdVacancy whereNeedAutomobile($value)
 * @method static Builder|AdVacancy whereNeedEducation($value)
 * @method static Builder|AdVacancy whereNeedExperience($value)
 * @method static Builder|AdVacancy whereNonResident($value)
 * @method static Builder|AdVacancy whereSchedule($value)
 * @method static Builder|AdVacancy whereScheduleComment($value)
 * @method static Builder|AdVacancy whereSex($value)
 * @mixin Eloquent
 * @property string $company_name
 * @property int|null $automobile
 * @property int|null $salary_from
 * @property int|null $salary_to
 * @property-read Ad $ad
 * @property-read Collection|JobDriver[] $driveLicenses
 * @property-read string $education_label
 * @property-read string $employment_label
 * @property-read string $experience_label
 * @property-read string $schedule_label
 * @property-read string $sex_label
 * @property-read \Kalnoy\Nestedset\Collection|JobIndustry[] $jobIndustries
 * @property-read \Kalnoy\Nestedset\Collection|JobProfessionalArea[] $jobProfessionalAreas
 * @property-read Collection|JobSkill[] $jobSkills
 * @method static Builder|AdVacancy whereAutomobile($value)
 * @method static Builder|AdVacancy whereCompanyName($value)
 * @method static Builder|AdVacancy whereEducation($value)
 * @method static Builder|AdVacancy whereEmployment($value)
 * @method static Builder|AdVacancy whereExperience($value)
 * @method static Builder|AdVacancy whereSalaryFrom($value)
 * @method static Builder|AdVacancy whereSalaryTo($value)
 * @property int|null $remote_work
 * @property-read mixed $job_industry
 * @property-read mixed $job_professional_area
 * @property-read mixed $price
 * @method static Builder|AdVacancy whereRemoteWork($value)
 * @property-read string $drive_license
 * @property-read string $job_skill
 */
class AdVacancy extends Model implements
    HasJobIndustry,
    HasJobProfessionalArea,
    HasAdGeneral,
    HasDriveLicensesAttribute,
    HasJobSkills,
    HasPriceAttribute
{

    use WorkTrait,
        JobIndustryTrait,
        JobProfessionalAreaTrait,
        AdGeneralTrait,
        DriverAttributeTrait,
        JobSkillsTrait;

    protected $primaryKey = 'ad_id';
    public $incrementing = false;

    public $timestamps = false;

    public $with = ['ad', 'jobProfessionalAreas', 'jobIndustries'];

    const SCHEDULE_FULL_TIME = 10;
    const SCHEDULE_SHIFT_TIME = 20;
    const SCHEDULE_FREE_TIME = 30;
    const SCHEDULE_LONG_SHIFT_TIME = 40;

    const EMPLOYMENT_GENERAL = 10;
    const EMPLOYMENT_PROJECT = 20;
    const EMPLOYMENT_INTERN = 30;

    const EXPERIENCE_WITHOUT = 1;
    const EXPERIENCE_FROM_1 = 10;
    const EXPERIENCE_FROM_3 = 30;
    const EXPERIENCE_FROM_5 = 50;

    const EDUCATION_NONE = 0;
    const EDUCATION_PRIMARY_SCHOOL = 10;
    const EDUCATION_HIGH_SCHOOL = 20;
    const EDUCATION_VOCATIONAL_SCHOOL = 30;
    const EDUCATION_UNIVERSITY = 40;
    const EDUCATION_MASTERS_DEGREE = 50;
    const EDUCATION_DOCTORATE_DEGREE = 60;

    const SEX_MALE = 10;
    const SEX_FEMALE = 20;

    protected $fillable = [
        'company_name',
        'schedule',
        'employment',
        'schedule_comment',
        'experience',
        'education',
        'automobile',
        'sex',
        'salary_from',
        'salary_to',
        'age_from',
        'age_to',
        'non_resident',
        'address',
        'remote_work'
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function (AdVacancy $model) {
            $model->ad->price = $model->price;
            $model->ad->save();
        });
    }

    public function getPriceAttribute()
    {
        if ($this->salary_from && $this->salary_to) {
            return trans('ad.vacancy.salary-value-both', [
                'salary_from' => $this->ad->getCurrencyWithSymbol($this->salary_from),
                'salary_to' => $this->ad->getCurrencyWithSymbol($this->salary_to),
            ]);
        } elseif ($this->salary_from) {
            return trans('ad.vacancy.salary-value-from', ['salary_from' => $this->ad->getCurrencyWithSymbol($this->salary_from)]);
        } elseif ($this->salary_to) {
            return trans('ad.vacancy.salary-value-to', ['salary_to' => $this->ad->getCurrencyWithSymbol($this->salary_to)]);
        } else {
            return trans('ad.vacancy.salary-value-no');
        }
    }

}
