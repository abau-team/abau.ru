<?php

namespace App\Models;

use App\Models\Interfaces\HasAdGeneral;
use App\Models\Interfaces\HasDriveLicensesAttribute;
use App\Models\Interfaces\HasJobEducations;
use App\Models\Interfaces\HasJobExperiences;
use App\Models\Interfaces\HasJobLanguage;
use App\Models\Interfaces\HasJobProfessionalArea;
use App\Models\Interfaces\HasJobSkills;
use App\Models\Interfaces\HasPriceAttribute;
use App\Models\Traits\AdGeneralTrait;
use App\Models\Traits\DriverAttributeTrait;
use App\Models\Traits\JobEducationsTrait;
use App\Models\Traits\JobExperiencesTrait;
use App\Models\Traits\JobLanguageTrait;
use App\Models\Traits\JobProfessionalAreaTrait;
use App\Models\Traits\JobSkillsTrait;
use App\Models\Traits\WorkTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\AdResume
 *
 * @property int $ad_id
 * @property string $first_name
 * @property string $last_name
 * @property string $patronymic_name
 * @property string $date_of_birth
 * @property int $sex
 * @property int $citizenship
 * @property int $married
 * @property int $children
 * @property int $schedule
 * @property string|null $schedule_comment
 * @property int $employment
 * @property int $experience
 * @property int $education
 * @property int|null $salary
 * @property int|null $automobile
 * @property int|null $ready_to_relocate
 * @property int|null $ready_to_trip
 * @property-read Ad $ad
 * @property-read Collection|JobDriver[] $driveLicenses
 * @property-read mixed $age
 * @property-read string $education_label
 * @property-read string $employment_label
 * @property-read string $experience_label
 * @property-read string $schedule_label
 * @property-read string $sex_label
 * @property-read Collection|JobEducation[] $jobEducations
 * @property-read Collection|JobExperience[] $jobExperiences
 * @property-read Collection|JobLanguage[] $jobLanguages
 * @property-read \Kalnoy\Nestedset\Collection|JobProfessionalArea[] $jobProfessionalAreas
 * @property-read Collection|JobSkill[] $jobSkills
 * @method static Builder|AdResume newModelQuery()
 * @method static Builder|AdResume newQuery()
 * @method static Builder|AdResume query()
 * @method static Builder|AdResume whereAdId($value)
 * @method static Builder|AdResume whereAutomobile($value)
 * @method static Builder|AdResume whereChildren($value)
 * @method static Builder|AdResume whereCitizenship($value)
 * @method static Builder|AdResume whereDateOfBirth($value)
 * @method static Builder|AdResume whereEducation($value)
 * @method static Builder|AdResume whereEmployment($value)
 * @method static Builder|AdResume whereExperience($value)
 * @method static Builder|AdResume whereFirstName($value)
 * @method static Builder|AdResume whereLastName($value)
 * @method static Builder|AdResume whereMarried($value)
 * @method static Builder|AdResume wherePatronymicName($value)
 * @method static Builder|AdResume whereReadyToRelocate($value)
 * @method static Builder|AdResume whereReadyToTrip($value)
 * @method static Builder|AdResume whereSalary($value)
 * @method static Builder|AdResume whereSchedule($value)
 * @method static Builder|AdResume whereScheduleComment($value)
 * @method static Builder|AdResume whereSex($value)
 * @mixin Eloquent
 * @property-read mixed $job_professional_area
 * @property-read mixed $price
 * @property-read string $drive_license
 * @property-read string $job_skill
 */
class AdResume extends Model implements
    HasJobProfessionalArea,
    HasAdGeneral,
    HasDriveLicensesAttribute,
    HasJobSkills,
    HasJobEducations,
    HasJobExperiences,
    HasJobLanguage,
    HasPriceAttribute
{

    use WorkTrait,
        JobProfessionalAreaTrait,
        AdGeneralTrait,
        DriverAttributeTrait,
        JobSkillsTrait,
        JobEducationsTrait,
        JobExperiencesTrait,
        JobLanguageTrait;

    protected $primaryKey = 'ad_id';
    public $incrementing = false;

    public $timestamps = false;

    public $with = ['ad', 'jobProfessionalAreas'];

    protected $fillable = [
        'first_name',
        'last_name',
        'patronymic_name',
        'date_of_birth',
        'sex',
        'citizenship',
        'married',
        'children',
        'schedule',
        'schedule_comment',
        'employment',
        'experience',
        'education',
        'salary',
        'automobile',
        'ready_to_relocate',
        'ready_to_trip'
    ];

    const SCHEDULE_FULL_TIME = 10;
    const SCHEDULE_SHIFT_TIME = 20;
    const SCHEDULE_FREE_TIME = 30;
    const SCHEDULE_LONG_SHIFT_TIME = 40;

    const EMPLOYMENT_GENERAL = 10;
    const EMPLOYMENT_PROJECT = 20;
    const EMPLOYMENT_INTERN = 30;

    const EXPERIENCE_WITHOUT = 1;
    const EXPERIENCE_FROM_1 = 10;
    const EXPERIENCE_FROM_3 = 30;
    const EXPERIENCE_FROM_5 = 50;

    const EDUCATION_NONE = 0;
    const EDUCATION_PRIMARY_SCHOOL = 10;
    const EDUCATION_HIGH_SCHOOL = 20;
    const EDUCATION_VOCATIONAL_SCHOOL = 30;
    const EDUCATION_UNIVERSITY = 40;
    const EDUCATION_MASTERS_DEGREE = 50;
    const EDUCATION_DOCTORATE_DEGREE = 60;

    const SEX_MALE = 10;
    const SEX_FEMALE = 20;

    public function getAgeAttribute()
    {
        return floor((time() - strtotime($this->date_of_birth)) / 31556926);
    }


    public static function boot()
    {
        parent::boot();

        static::saving(function (AdResume $model) {
            $model->ad->price = $model->price;
            $model->ad->save();
        });
    }

    public function getPriceAttribute()
    {
        return trans_choice("ad.resume.salary-value", $this->salary, ['salary' => $this->ad->getCurrencyWithSymbol($this->salary)]);
    }

    public static function getMonths()
    {
        return [
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'Ноябрь',
            12 => 'Декабрь',
        ];
    }


}
