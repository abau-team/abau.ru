<?php

namespace App\Models;

use App\Models\Interfaces\HasLocalName;
use App\Models\Traits\NestedTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\Collection;
use Kalnoy\Nestedset\NodeTrait;
use Str;

/**
 * App\Models\JobProfessionalArea
 *
 * @property int $job_professional_area_id
 * @property string $name
 * @property-read string $local_name
 * @property string $code
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @method static Builder|JobProfessionalArea newModelQuery()
 * @method static Builder|JobProfessionalArea newQuery()
 * @method static Builder|JobProfessionalArea query()
 * @method static Builder|JobProfessionalArea whereCode($value)
 * @method static Builder|JobProfessionalArea whereJobProfessionalAreaId($value)
 * @method static Builder|JobProfessionalArea whereLft($value)
 * @method static Builder|JobProfessionalArea whereName($value)
 * @method static Builder|JobProfessionalArea whereParentId($value)
 * @method static Builder|JobProfessionalArea whereRgt($value)
 * @mixin Eloquent
 * @property-read Collection|JobProfessionalArea[] $children
 * @property-read JobProfessionalArea|null $parent
 * @method static Builder|JobProfessionalArea d()
 */
class JobProfessionalArea extends Model implements HasLocalName
{
    use NodeTrait, NestedTrait;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $primaryKey = 'job_professional_area_id';

    /**
     * @return string
     */
    public function getLocalNameAttribute()
    {
        return __("job-professional-area.values.{$this->code}");
    }

    protected $fillable = ['name', 'code'];

    public static function boot()
    {
        parent::boot();

        static::saving(function (JobProfessionalArea $model) {
            $model->code = $model->code ?? Str::slug($model->name);
        });
    }

    /**
     * @return mixed
     */
    public static function getTree()
    {
        return static::get()->toTree();
    }
}
