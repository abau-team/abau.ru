<?php

namespace App\Models;

use App\Models\Interfaces\HasAdGeneral;
use App\Models\Traits\AdGeneralTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\JobEducation
 *
 * @property int $ad_id
 * @property int $type
 * @property int $year
 * @property string $name
 * @property string|null $department
 * @property string $specialization
 * @property-read Ad $ad
 * @property-read mixed $type_label
 * @method static Builder|JobEducation newModelQuery()
 * @method static Builder|JobEducation newQuery()
 * @method static Builder|JobEducation query()
 * @method static Builder|JobEducation whereAdId($value)
 * @method static Builder|JobEducation whereDepartment($value)
 * @method static Builder|JobEducation whereName($value)
 * @method static Builder|JobEducation whereSpecialization($value)
 * @method static Builder|JobEducation whereType($value)
 * @method static Builder|JobEducation whereYear($value)
 * @mixin Eloquent
 */
class JobEducation extends Model implements HasAdGeneral
{
    use AdGeneralTrait;

    protected $primaryKey = 'ad_id';
    public $incrementing = false;

    public $timestamps = false;

    const TYPE_UNIVERSITY = 10;
    const TYPE_UNIVERSITY_UNCOMPLETED = 15;
    const TYPE_VOCATIONAL_SCHOOL = 20;
    const TYPE_REFRESHER_COURSE = 30;

    public static function types()
    {
        return [
            static::TYPE_UNIVERSITY => __('job-education.types.university'),
            static::TYPE_UNIVERSITY_UNCOMPLETED => __('job-education.types.university-uncompleted'),
            static::TYPE_VOCATIONAL_SCHOOL => __('job-education.types.vocational-school'),
            static::TYPE_REFRESHER_COURSE => __('job-education.types.refresher-course'),
        ];
    }

    protected $fillable = ['type', 'year', 'name', 'department', 'specialization'];

    public function getTypeLabelAttribute()
    {
        $types = static::types();
        return $types[$this->type] ?? null;
    }


    //
}
