<?php

namespace App\Models;

use App\Models\Scopes\OrderCreatedDescScope;
use App\Models\Traits\TraitActivate;
use App\Models\Traits\TraitBan;
use App\Models\Traits\TraitDelete;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany as HasManyAlias;
use Illuminate\Support\Carbon;
use Request;

/**
 * App\Models\Contact
 *
 * @property int $contact_id
 * @property int $user_id
 * @property string $name
 * @property int $type
 * @property string $value
 * @property-read string $type_label
 * @property Carbon|null $created_at
 * @property string|null $deleted_at
 * @property string|null $blocked_at
 * @property string|null $activated_at
 * @property string $ip
 * @property User|null $user
 * @method static Builder|Contact newModelQuery()
 * @method static Builder|Contact newQuery()
 * @method static Builder|Contact query()
 * @method static Builder|Contact whereActivatedAt($value)
 * @method static Builder|Contact whereBlockedAt($value)
 * @method static Builder|Contact whereCreatedAt($value)
 * @method static Builder|Contact whereDeletedAt($value)
 * @method static Builder|Contact whereId($value)
 * @method static Builder|Contact whereType($value)
 * @method static Builder|Contact whereUserId($value)
 * @method static Builder|Contact whereValue($value)
 * @mixin Eloquent
 * @method static Builder|Contact whereContactId($value)
 * @method static Builder|Contact whereName($value)
 * @method static Builder|Contact active()
 * @property-read Collection|Ad[] $ads
 * @property-read bool $is_activated
 * @property-read bool $is_banned
 * @property-read bool $is_deleted
 * @property-read Collection|Token[] $tokens
 * @method static Builder|Contact whereActivate()
 * @method static Builder|Contact whereBanned()
 * @method static Builder|Contact whereIp($value)
 * @method static Builder|Contact whereNotActivate()
 * @method static Builder|Contact whereNotBanned()
 * @method static Builder|Contact whereNotRemoved()
 * @method static Builder|Contact whereRemoved()
 */
class Contact extends Model
{
    use TraitDelete, TraitBan, TraitActivate;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $primaryKey = 'contact_id';

    const TYPE_EMAIL = 1;
    const TYPE_PHONE = 10;

    /**
     * Назначаем именна константам TYPE
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_EMAIL => __('contact.types.email'),
            self::TYPE_PHONE => __('contact.types.phone'),
        ];
    }

    /**
     * Получаем имя спрятанное за константой TYPE
     * @param string $default значение по умолчанию, если ничего не найдется
     * @return string
     */
    public function getTypeLabelAttribute($default = null)
    {
        $types = self::getTypes();
        return isset($types[$this->type]) ? $types[$this->type] : $default;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'value',
    ];


    /**
     * Scope a query to only include active contacts.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive($query)
    {
        return $query->where([
            'deleted_at' => null,
            'blocked_at' => null,
        ])->whereNotNull('activated_at');
    }

    /**
     * Get the post that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the contacts for the user
     *
     * @return HasManyAlias
     */
    public function tokens()
    {
        return $this->hasMany(Token::class, 'contact_id');
    }

    /**
     * @return BelongsToMany
     */
    public function ads()
    {
        return $this->belongsToMany(Ad::class, 'ads_contacts', 'contact_id', 'ad_id');
    }

    /**
     * @return Token|Model
     */
    public function createActivationToken()
    {
        return $this->tokens()->create([
            'type' => Token::TYPE_ACTIVATE,
        ]);
    }

    /**
     * @return Token|Model
     */
    public function createRecoveryToken()
    {
        return $this->tokens()->create([
            'type' => Token::TYPE_RECOVERY,
        ]);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OrderCreatedDescScope());

        static::creating(function (Contact $model) {
            $model->created_at = $model->created_at ?? now();
            $model->ip = $model->ip ?? inet_pton(Request::ip());
        });
    }

    /**
     * @param string $string
     * @return string
     */
    public static function preparePhone(string $string): string
    {
        $string = str_split(strrev(preg_replace('~[^0-9]+~', '', $string)));
        $phone = '';
        foreach ($string as $key => $char) {
            if ($key == 2 || $key == 4)
                $phone .= "-{$char}";
            elseif ($key == 7)
                $phone .= " ){$char}";
            elseif ($key == 10) {
                if ($char == '8' && !key_exists($key + 1, $string)) {
                    $char = '7';
                }
                $phone .= "( {$char}";
            } else $phone .= $char;
        }
        $phone .= '+';
        return strrev($phone);
    }


}
