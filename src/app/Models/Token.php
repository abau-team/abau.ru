<?php

namespace App\Models;

use App\Models\Traits\HasCompositePrimaryKey;
use Eloquent;
use Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo as BelongsToAlias;
use Illuminate\Support\Carbon;

/**
 * App\Models\Token
 *
 * @property int $contact_id
 * @property int $type
 * @property string $value
 * @property-read string $type_label
 * @property Carbon|null $created_at
 * @property Contact|null $contact
 * @property-read bool $is_expired
 * @method static Builder|Token newModelQuery()
 * @method static Builder|Token newQuery()
 * @method static Builder|Token query()
 * @method static Builder|Token whereContactId($value)
 * @method static Builder|Token whereCreatedAt($value)
 * @method static Builder|Token whereId($value)
 * @method static Builder|Token whereType($value)
 * @method static Builder|Token whereValue($value)
 * @mixin Eloquent
 */
class Token extends Model
{
    use HasCompositePrimaryKey;

    const TYPE_ACTIVATE = 10;
    const TYPE_RECOVERY = 30;

    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $primaryKey = ['type', 'value'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
    ];

    protected $with = ['contact'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
    ];

    /**
     * Получаем имя спрятанное за константой TYPE
     * @param string $default значение по умолчанию, если ничего не найдется
     * @return string
     */
    public function getTypeLabelAttribute($default = null)
    {
        $types = self::getTypes();
        return isset($types[$this->type]) ? $types[$this->type] : $default;
    }

    /**
     * @return BelongsToAlias|Contact
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }


    public static function boot()
    {
        parent::boot();

        static::creating(function (Token $model) {
            $model->created_at = now();
            if ($model->contact->type == Contact::TYPE_EMAIL) {
                $model->value = Str::random(20) . '-' . time();
            } elseif ($model->contact->type == Contact::TYPE_PHONE) {
                $model->value = Str::upper(Str::random(6));
            }
        });
    }

    public function getIsExpiredAttribute():bool {
        // Добавить проверку времени
        return false;
    }

    /**
     * Назначаем именна константам TYPE
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_ACTIVATE => __('token.types.activate'),
            self::TYPE_RECOVERY => __('token.types.recovery'),
        ];
    }
}
