<?php

namespace App\Models;

use App\Models\Interfaces\HasAdGeneral;
use App\Models\Traits\AdGeneralTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class JobDriver
 *
 * @package App\Models
 * @property string $driver_label
 * @property-read integer $issue
 * @property string $date_issue
 * @property int $ad_id
 * @property int $type_driver
 * @property-read Ad $ad
 * @method static Builder|JobDriver newModelQuery()
 * @method static Builder|JobDriver newQuery()
 * @method static Builder|JobDriver query()
 * @method static Builder|JobDriver whereAdId($value)
 * @method static Builder|JobDriver whereDateIssue($value)
 * @method static Builder|JobDriver whereTypeDriver($value)
 * @mixin Eloquent
 */
class JobDriver extends Model implements
    HasAdGeneral
{
    use AdGeneralTrait;

    protected $primaryKey = 'ad_id';
    public $incrementing = false;

    public $timestamps = false;

    const DRIVE_A = 1;
    const DRIVE_A1 = 5;
    const DRIVE_B = 10;
    const DRIVE_B1 = 15;
    const DRIVE_BE = 11;
    const DRIVE_C = 20;
    const DRIVE_C1 = 25;
    const DRIVE_CE = 21;
    const DRIVE_C1E = 26;
    const DRIVE_D = 30;
    const DRIVE_DE = 31;
    const DRIVE_D1 = 35;
    const DRIVE_D1E = 36;
    const DRIVE_M = 40;
    const DRIVE_TM = 50;
    const DRIVE_TB = 55;

    /**
     * @return array
     */
    public static function getDrivers(): array
    {
        return [
            static::DRIVE_A => 'A',
            static::DRIVE_A1 => 'A1',
            static::DRIVE_B => 'B',
            static::DRIVE_B1 => 'B1',
            static::DRIVE_BE => 'BE',
            static::DRIVE_C => 'C',
            static::DRIVE_C1 => 'C1',
            static::DRIVE_CE => 'CE',
            static::DRIVE_C1E => 'C1E',
            static::DRIVE_D => 'D',
            static::DRIVE_DE => 'DE',
            static::DRIVE_D1 => 'D1',
            static::DRIVE_D1E => 'D1E',
            static::DRIVE_M => 'M',
            static::DRIVE_TM => 'Tm',
            static::DRIVE_TB => 'Tb',
        ];
    }

    protected $fillable = ['type_driver'];

    /**
     * @return string
     */
    public function getDriverLabelAttribute(): string
    {
        $drivers = static::getDrivers();
        return $drivers[$this->type_driver] ?? null;
    }

    public function getIssueAttribute()
    {
        return floor((time() - strtotime($this->date_issue)) / 31556926);
    }

    /**
     * @param string $key
     * @return int
     */
    public static function getDriverKey(string $key):int {
        $drivers = array_flip(static::getDrivers());
        return $drivers[$key] ?? null;
    }

}
