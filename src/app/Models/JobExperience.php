<?php

namespace App\Models;

use App\Models\Traits\AdGeneralTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\JobExperience
 *
 * @property int $ad_id
 * @property string $started_at
 * @property int|null $current_work
 * @property string|null $finished_at
 * @property string $organization
 * @property int $place_id
 * @property string $site
 * @property int $job_industry_id
 * @property string $position
 * @property string $duties
 * @property-read Ad $ad
 * @method static Builder|JobExperience newModelQuery()
 * @method static Builder|JobExperience newQuery()
 * @method static Builder|JobExperience query()
 * @method static Builder|JobExperience whereAdId($value)
 * @method static Builder|JobExperience whereCurrentWork($value)
 * @method static Builder|JobExperience whereDuties($value)
 * @method static Builder|JobExperience whereFinishedAt($value)
 * @method static Builder|JobExperience whereJobIndustryId($value)
 * @method static Builder|JobExperience whereOrganization($value)
 * @method static Builder|JobExperience wherePlaceId($value)
 * @method static Builder|JobExperience wherePosition($value)
 * @method static Builder|JobExperience whereSite($value)
 * @method static Builder|JobExperience whereStartedAt($value)
 * @mixin Eloquent
 * @property-read mixed $interval
 * @property-read JobIndustry $jobIndustry
 * @property-read Place $place
 */
class JobExperience extends Model
{
    use AdGeneralTrait;

    protected $primaryKey = 'ad_id';
    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ['started_at', 'current_work', 'finished_at', 'organization', 'place_id', 'site', 'job_industry_id', 'position', 'duties'];

    public function getIntervalAttribute()
    {
        if ($this->finished_at == null || $this->current_work) {
            return trans('job-experience.intervalValueNow', ['date' => date('M Y', strtotime($this->started_at))]);
        } else {
            return trans('job-experience.intervalValue', [
                'from' => date('M Y', strtotime($this->started_at)),
                'to' => date('M Y', strtotime($this->finished_at))
            ]);
        }
    }

    /**
     * @return Ad|null|BelongsTo
     */
    public function ad()
    {
        return $this->belongsTo(Ad::class, 'ad_id');
    }

    /**
     * @return JobIndustry|null|BelongsTo
     */
    public function jobIndustry()
    {
        return $this->belongsTo(JobIndustry::class, 'job_industry_id');
    }

    /**
     * @return Place|null|BelongsTo
     */
    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id');
    }
}
