<?php

namespace App\Models\Traits;

use App\Models\JobEducation;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Trait JobEducationsTrait
 * @package App\Models\Traits
 *
 * @property JobEducation[]|null $jobEducations
 */
trait JobEducationsTrait
{

    /**
     * @return JobEducation[]|null|HasMany
     */
    public function jobEducations()
    {
        return $this->hasMany(JobEducation::class, 'ad_id', 'ad_id');
    }

}