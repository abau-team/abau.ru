<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait TraitActivate
 * @package App\Models\Traits
 *
 * @property-read bool $isActivated
 * @method static Builder|Model whereActivate()
 * @method static Builder|Model whereNotActivate()
 */
trait TraitActivate
{
    /**
     * Check Activate Status
     *
     * @return bool
     */
    public function getIsActivatedAttribute()
    {
        return $this->activated_at != null;
    }

    /**
     * Set Activate Status
     *
     * @return bool
     */
    public function activate()
    {
        if (!$this->isActivated) {
            $this->activated_at = now();
            return $this->save();
        }
        return false;
    }

    /**
     * Set Deactivate Status
     *
     * @return bool
     */
    public function deactivate()
    {
        if ($this->isActivated) {
            $this->activated_at = null;
            return $this->save();
        }
        return false;
    }

    /**
     * Scope a query to only include deleted contacts.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeWhereActivate($query) {
        return $query->whereNotNull('activated_at');
    }

    /**
     * Scope a query to only include deleted contacts.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeWhereNotActivate($query) {
        return $query->whereNull('activated_at');
    }

}