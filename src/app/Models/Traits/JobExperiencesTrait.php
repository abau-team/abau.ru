<?php

namespace App\Models\Traits;

use App\Models\JobExperience;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Trait JobExperiencesTrait
 * @package App\Models\Traits
 *
 * @property JobExperience[]|null $jobEducations
 */
trait JobExperiencesTrait
{

    /**
     * @return JobExperience[]|null|HasMany
     */
    public function jobExperiences()
    {
        return $this->hasMany(JobExperience::class, 'ad_id', 'ad_id');
    }

}