<?php

namespace App\Models\Traits;

use App\Models\JobIndustry;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Trait JobIndustryTrait
 * @package App\Models\Traits
 *
 * @property JobIndustry[]|null $jobIndustries
 * @property-read string $jobIndustry
 */
trait JobIndustryTrait
{

    /**
     * @return JobIndustry[]|null|BelongsToMany
     */
    public function jobIndustries()
    {
        return $this->belongsToMany(JobIndustry::class, 'ads_job_industries', 'ad_id', 'job_industry_id');
    }

    public function getJobIndustryAttribute() {
        $industries = [];
        foreach ($this->jobIndustries as $industry) {
            $industries[] = $industry->local_name;
        }
        return implode(', ', $industries);
    }

}