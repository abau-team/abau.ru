<?php

namespace App\Models\Traits;

use App\Models\JobLanguage;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Trait JobLanguageTrait
 * @package App\Models\Traits
 *
 * @property JobLanguage[]|null $driveLicenses
 */
trait JobLanguageTrait {

    /**
     * @return HasMany|JobLanguage[]|null
     */
    public function jobLanguages() {
        return $this->hasMany(JobLanguage::class, 'ad_id', 'ad_id');
    }

}