<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait TraitBan
 * @package App\Models\Traits
 *
 * @property-read bool $isBanned
 * @method static Builder|Model whereBanned()
 * @method static Builder|Model whereNotBanned()
 */
trait TraitBan
{
    /**
     * Check Banned Status
     *
     * @return bool
     */
    public function getIsBannedAttribute() {
        return $this->blocked_at != null;
    }

    /**
     * @return bool
     */
    public function ban()
    {
        if (!$this->isBanned) {
            $this->blocked_at = now();
            return $this->save();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function unBan()
    {
        if ($this->isBanned) {
            $this->blocked_at = null;
            return $this->save();
        }
        return false;
    }

    /**
     * Scope a query to only include deleted contacts.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeWhereBanned($query) {
        return $query->whereNotNull('blocked_at');
    }

    /**
     * Scope a query to only include deleted contacts.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeWhereNotBanned($query) {
        return $query->whereNull('blocked_at');
    }

}