<?php

namespace App\Models\Traits;

use App\Models\JobSkill;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Trait JobSkillsTrait
 * @package App\Models\Traits
 *
 * @property JobSkill[]|null $jobIndustries
 * @property-read string $jobSkill
 */
trait JobSkillsTrait
{

    /**
     * @return JobSkill[]|null|BelongsToMany
     */
    public function jobSkills()
    {
        return $this->belongsToMany(JobSkill::class, 'ads_job_skills', 'ad_id', 'job_skill_id');
    }

    /**
     * @return string
     */
    public function getJobSkillAttribute() {
        $array = [];
        foreach ($this->jobSkills as $value) {
            $array[] = $value->name;
        }
        return implode(', ', $array);
    }

}