<?php

namespace App\Models\Traits;

use App\Models\JobDriver;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Trait DriverAttributeTrait
 * @package App\Models\Traits
 *
 * @property JobDriver[]|null $driveLicenses
 * @property-read string $driveLicense
 */
trait DriverAttributeTrait {

    /**
     * @return HasMany|JobDriver[]|null
     */
    public function driveLicenses() {
        return $this->hasMany(JobDriver::class, 'ad_id', 'ad_id');
    }

    /**
     * @return string
     */
    public function getDriveLicenseAttribute() {
        $array = [];
        foreach ($this->driveLicenses as $value) {
            $array[] = $value->driver_label;
        }
        return implode(', ', $array);
    }
}