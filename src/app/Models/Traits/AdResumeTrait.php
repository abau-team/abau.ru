<?php

namespace App\Models\Traits;

use App\Models\AdResume;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait AdResumeTrait
 * @package App\Models\Traits
 *
 * @property AdResume $resume
 */
trait AdResumeTrait
{
    /**
     * @return BelongsTo|AdResume|null
     */
    public function resume()
    {
        return $this->belongsTo(AdResume::class,  'ad_id');
    }

}