<?php

namespace App\Models\Traits;

use App\Models\Ad;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait AdGeneralTrait
 * @package App\Models\Traits
 *
 * @property Ad|null $ad
 */
trait AdGeneralTrait
{
    /**
     * @return Ad|null|BelongsTo
     */
    public function ad()
    {
        return $this->belongsTo(Ad::class,  'ad_id');
    }

}