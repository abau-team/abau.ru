<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Str;

/**
 * Trait NestedTrait
 * @package App\Models\Traits
 *
 *
 * @method static Builder|Model whereIsLeaf()
 * @method static Builder|Model defaultOrder()
 */
trait NestedTrait {

    /**
     * @return string
     */
    public static function getPrKey() {
        return Str::snake(class_basename(self::class)) . "_id";
    }

    /**
     * @return string
     */
    public static function getTableName() {
        return Str::snake(Str::pluralStudly(class_basename(self::class)));
    }


    /**
     * @param null $id
     * @return array
     */
    public static function dataForDropdown($id = null) {
        if ($id) {
            $models = self::defaultOrder()->whereNotDescendantOf($id)->where(self::getPrKey(), '!=', $id)->get()->toTree();
        } else {
            $models = self::defaultOrder()->get()->toTree();
        }

        $array = [];
        $loop = function (array & $array, $models, string $prefix = null) use (&$loop) {
            foreach ($models as $model) {
                /* @var self $model */
                $array[$model->{$model->primaryKey}] = $prefix . $model->local_name;
                $loop($array, $model->children, $prefix . "-");
            }
        };
        $loop($array, $models);

        return $array;

    }
}