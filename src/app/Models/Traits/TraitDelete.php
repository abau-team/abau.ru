<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait TraitBan
 * @package App\Models\Traits
 *
 * @property-read bool $isDeleted
 * @method static Builder|Model WhereRemoved()
 * @method static Builder|Model WhereNotRemoved()
 */
trait TraitDelete {

    /**
     * Check Deleted Status
     *
     * @return bool
     */
    public function getIsDeletedAttribute() {
        return $this->deleted_at != null;
    }



    /**
     * @return bool
     */
    public function markDeleted() {
        if (!$this->isDeleted) {
            $this->deleted_at = now();
            return $this->save();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function markUnDeleted() {
        if ($this->isDeleted) {
            $this->deleted_at = null;
            return $this->save();
        }
        return false;
    }

    /**
     * Scope a query to only include deleted contacts.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeWhereRemoved($query) {
        return $query->whereNotNull('deleted_at');
    }

    /**
     * Scope a query to only include deleted contacts.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeWhereNotRemoved($query) {
        return $query->whereNull('deleted_at');
    }

}