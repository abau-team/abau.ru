<?php

namespace App\Models\Traits;

/**
 * Trait WorkEmployment
 * @package App\Models\Traits
 *
 * @property-read string $employment_label
 * @property-read string $schedule_label
 * @property-read string $education_label
 * @property-read string $experience_label
 */
trait WorkTrait
{

    /**
     * Назначаем именна константам SCHEDULE
     * @return array
     */
    public static function getSchedules()
    {
        return [
            static::SCHEDULE_FULL_TIME => __('ad.schedules.full-time'),
            static::SCHEDULE_SHIFT_TIME => __('ad.schedules.shift-time'),
            static::SCHEDULE_FREE_TIME => __('ad.schedules.free-time'),
            static::SCHEDULE_LONG_SHIFT_TIME => __('ad.schedules.long-shift-time'),
        ];
    }

    /**
     * Получаем имя спрятанное за константой SCHEDULE
     * @param string $default значение по умолчанию, если ничего не найдется
     * @return string
     */
    public function getScheduleLabelAttribute($default = null)
    {
        $schedules = static::getSchedules();
        return $schedules[$this->schedule] ?? $default;
    }

    /**
     * Назначаем именна константам EMPLOYMENT
     * @return array
     */
    public static function getEmployments()
    {
        return [
            static::EMPLOYMENT_GENERAL => __('ad.employments.general'),
            static::EMPLOYMENT_PROJECT => __('ad.employments.project'),
            static::EMPLOYMENT_INTERN => __('ad.employments.intern'),
        ];
    }

    /**
     * Получаем имя спрятанное за константой EMPLOYMENT
     * @param string $default значение по умолчанию, если ничего не найдется
     * @return string
     */
    public function getEmploymentLabelAttribute($default = null)
    {
        $employments = static::getEmployments();
        return $employments[$this->employment] ?? $default;
    }


    /**
     * Назначаем именна константам EXPERIENCE
     * @return array
     */
    public static function getExperiences()
    {
        return [
            static::EXPERIENCE_WITHOUT => __('ad.experiences.without'),
            static::EXPERIENCE_FROM_1 => __('ad.experiences.from_1'),
            static::EXPERIENCE_FROM_3 => __('ad.experiences.from_3'),
            static::EXPERIENCE_FROM_5 => __('ad.experiences.from_5'),
        ];
    }

    /**
     * Получаем имя спрятанное за константой EXPERIENCE
     * @param string $default значение по умолчанию, если ничего не найдется
     * @return string
     */
    public function getExperienceLabelAttribute($default = null)
    {
        $experiences = static::getExperiences();
        return $experiences[$this->experience] ?? $default;
    }


    /**
     * Назначаем именна константам EDUCATION
     * @return array
     */
    public static function getEducation()
    {
        return [
            //static::EDUCATION_NONE => __('ad.educations.none'),
            static::EDUCATION_PRIMARY_SCHOOL => __('ad.educations.primary-school'),
            static::EDUCATION_HIGH_SCHOOL => __('ad.educations.high-school'),
            static::EDUCATION_VOCATIONAL_SCHOOL => __('ad.educations.vocational-school'),
            static::EDUCATION_UNIVERSITY => __('ad.educations.university'),
            //static::EDUCATION_MASTERS_DEGREE => __('ad.educations.masters-degree'),
            //static::EDUCATION_DOCTORATE_DEGREE => __('ad.educations.doctorate-degree'),
        ];
    }

    /**
     * Получаем имя спрятанное за константой EDUCATION
     * @param string $default значение по умолчанию, если ничего не найдется
     * @return string
     */
    public function getEducationLabelAttribute($default = null)
    {
        $educations = static::getEducation();
        return $educations[$this->education] ?? $default;
    }

    /**
     * Назначаем именна константам SEX
     * @return array
     */
    public static function getSexes(): array
    {
        return [
            static::SEX_FEMALE => __('ad.sex.female'),
            static::SEX_MALE => __('ad.sex.male'),
        ];
    }

    /**
     * Получаем имя спрятанное за константой SEX
     * @return string
     */
    public function getSexLabelAttribute(): string
    {
        $sexes = static::getSexes();
        return $sexes[$this->sex] ?? null;
    }

}