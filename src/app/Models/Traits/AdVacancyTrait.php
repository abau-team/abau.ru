<?php

namespace App\Models\Traits;

use App\Models\AdVacancy;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait AdVacancyTrait
 * @package App\Models\Traits
 *
 * @property AdVacancy|null $vacancy
 */
trait AdVacancyTrait
{
    /**
     * @return BelongsTo|AdVacancy|null
     */
    public function vacancy()
    {
        return $this->belongsTo(AdVacancy::class,  'ad_id');
    }

}