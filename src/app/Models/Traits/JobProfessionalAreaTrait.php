<?php

namespace App\Models\Traits;

use App\Models\JobProfessionalArea;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Trait JobProfessionalAreaTrait
 * @package App\Models\Traits
 *
 * @property JobProfessionalArea[]|null $jobProfessionalAreas
 * @property-read string $jobProfessionalArea
 */
trait JobProfessionalAreaTrait
{

    /**
     * @return JobProfessionalArea[]|null|BelongsToMany
     */
    public function jobProfessionalAreas()
    {
        return $this->belongsToMany(JobProfessionalArea::class, 'ads_job_professional_areas', 'ad_id', 'job_professional_area_id');
    }

    public function getJobProfessionalAreaAttribute() {
        $areas = [];
        foreach ($this->jobProfessionalAreas as $professionalArea) {
            $areas[] = $professionalArea->local_name;
        }
        return implode(', ', $areas);
    }

}