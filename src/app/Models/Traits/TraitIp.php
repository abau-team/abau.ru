<?php

namespace App\Models\Traits;

/**
 * Trait TraitBan
 * @package App\Models\Traits
 *
 * @property-read bool $ipString
 */
trait TraitIp
{
    /**
     * Check Banned Status
     *
     * @return bool
     */
    public function getIpStringAttribute() {
        return inet_ntop($this->ip);
    }

}