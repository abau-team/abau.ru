<?php

namespace App\Models;

use App\Models\Interfaces\HasLocalName;
use App\Models\Traits\NestedTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\Collection;
use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use Str;

/**
 * App\Models\JobIndustry
 *
 * @property int $job_industry_id
 * @property string $name
 * @property string $code
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property-read Collection|JobIndustry[] $children
 * @property-read string $local_name
 * @property-read JobIndustry|null $parent
 * @method static Builder|JobIndustry d()
 * @method static QueryBuilder|JobIndustry newModelQuery()
 * @method static QueryBuilder|JobIndustry newQuery()
 * @method static QueryBuilder|JobIndustry query()
 * @method static Builder|JobIndustry whereCode($value)
 * @method static Builder|JobIndustry whereJobIndustryId($value)
 * @method static Builder|JobIndustry whereLft($value)
 * @method static Builder|JobIndustry whereName($value)
 * @method static Builder|JobIndustry whereParentId($value)
 * @method static Builder|JobIndustry whereRgt($value)
 * @mixin Eloquent
 */
class JobIndustry extends Model implements HasLocalName
{
    use NodeTrait, NestedTrait;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $primaryKey = 'job_industry_id';

    /**
     * @return string
     */
    public function getLocalNameAttribute() {
        return __("job-industry.values.{$this->code}");
    }

    protected $fillable = ['name', 'code'];

    public static function boot()
    {
        parent::boot();

        static::saving(function (JobIndustry $model) {
            $model->code = $model->code ?? Str::slug($model->name);
        });
    }

    /**
     * @return mixed
     */
    public static function getTree()
    {
        return static::get()->toTree();
    }
}
