<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Photo
 *
 * @property int $photo_id
 * @property string $path
 * @property float $size
 * @property string $created_at
 * @property string|null $remastered_at
 * @property mixed|null $ip
 * @method static Builder|Photo newModelQuery()
 * @method static Builder|Photo newQuery()
 * @method static Builder|Photo query()
 * @method static Builder|Photo whereCreatedAt($value)
 * @method static Builder|Photo whereIp($value)
 * @method static Builder|Photo wherePath($value)
 * @method static Builder|Photo wherePhotoId($value)
 * @method static Builder|Photo whereRemasteredAt($value)
 * @method static Builder|Photo whereSize($value)
 * @mixin Eloquent
 */
class Photo extends Model
{
    protected $primaryKey = 'photo_id';
    public $timestamps = false;

    public function ads() {
    }

    protected $fillable = ['path', 'size'];
}
