<?php

namespace App\Models;

use App\Http\Requests\AdStore;
use App\Models\Interfaces\HasDriveLicensesAttribute;
use App\Models\Interfaces\HasJobIndustry;
use App\Models\Interfaces\HasJobProfessionalArea;
use App\Models\Interfaces\HasJobSkills;
use App\Models\Scopes\OrderCreatedDescScope;
use App\Models\Traits\TraitBan;
use App\Models\Traits\TraitDelete;
use App\Models\Traits\TraitIp;
use DB;
use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\File;
use Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Request;
use Storage;

/**
 * App\Models\Ad
 *
 * @property int $ad_id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $phone
 * @property int $views
 * @property int $view_contact
 * @property int|null $ad_category_id
 * @property int|null $user_id
 * @property int|null $category_id
 * @property int|null $place_id
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property string|null $finished_at
 * @property string|null $refreshed_at
 * @property string $title
 * @property string $description
 * @property mixed $price
 * @property User|null $user
 * @property Place|null $place
 * @property AdCategory|null $category
 * @property Contact[]|null $contacts
 * @property Photo[]|Collection|null $photos
 * @method static Builder|Ad newModelQuery()
 * @method static Builder|Ad newQuery()
 * @method static Builder|Ad query()
 * @method static Builder|Ad whereCategoryId($value)
 * @method static Builder|Ad whereCreatedAt($value)
 * @method static Builder|Ad whereDescription($value)
 * @method static Builder|Ad whereFinishedAt($value)
 * @method static Builder|Ad whereId($value)
 * @method static Builder|Ad wherePlaceId($value)
 * @method static Builder|Ad wherePrice($value)
 * @method static Builder|Ad whereRefreshedAt($value)
 * @method static Builder|Ad whereTitle($value)
 * @method static Builder|Ad whereUpdatedAt($value)
 * @method static Builder|Ad whereUserId($value)
 * @mixin Eloquent
 * @method static Builder|Ad whereAdCategoryId($value)
 * @method static Builder|Ad whereAdId($value)
 * @method static Builder|Ad whereEmail($value)
 * @method static Builder|Ad wherePhone($value)
 * @method static Builder|Ad whereViewContact($value)
 * @method static Builder|Ad whereViews($value)
 * @method static Builder|Ad active()
 * @property Carbon|null $deleted_at
 * @property Carbon|null $blocked_at
 * @property int $currency
 * @property mixed|null $ip
 * @property-read AdResume $adResume
 * @property-read AdVacancy $adVacancy
 * @property-read mixed $currency_label
 * @property-read bool $ip_string
 * @property-read bool $is_banned
 * @property-read bool $is_deleted
 * @method static Builder|Ad whereBanned()
 * @method static Builder|Ad whereBlockedAt($value)
 * @method static Builder|Ad whereCurrency($value)
 * @method static Builder|Ad whereDeletedAt($value)
 * @method static Builder|Ad whereIp($value)
 * @method static Builder|Ad whereName($value)
 * @method static Builder|Ad whereNotBanned()
 * @method static Builder|Ad whereNotRemoved()
 * @method static Builder|Ad whereRemoved()
 * @property-read mixed $url
 */
class Ad extends Model
{
    use TraitBan, TraitDelete, TraitIp;

    const STATUS_ACTIVE = 'active';
    const STATUS_BLOCKED = 'blocked';
    const STATUS_DELETED = 'deleted';

    const CURRENCY_ROUBLE = 10;
    const CURRENCY_DOLLAR = 20;
    const CURRENCY_EURO = 30;

    public static function currencies()
    {
        return [
            static::CURRENCY_ROUBLE => 'руб.',
            static::CURRENCY_DOLLAR => '$',
            static::CURRENCY_EURO => '€',
        ];
    }

    public function getCurrencyLabelAttribute()
    {
        $currencies = static::currencies();
        return $currencies[$this->currency] ?? null;
    }

    /**
     * @param $sum
     * @return string|null
     */
    public function getCurrencyWithSymbol($sum)
    {
        if ($sum == null) {
            return null;
        } else {
            $sum = number_format($sum, 0, '', ' ');
        }

        if ($this->currency == static::CURRENCY_ROUBLE) {
            return "{$sum} {$this->currency_label}";
        } else {
            return "{$this->currency_label}{$sum}";
        }
    }

    protected $primaryKey = 'ad_id';

    public $timestamps = false;

    public $with = [
        'place',
        'category'
    ];

    protected $fillable = ['ad_category_id', 'place_id', 'title', 'description', 'currency', 'currency', 'name', 'phone', 'email'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'blocked_at' => 'datetime',
        'finished_at' => 'datetime',
        'refreshed_at' => 'datetime',
    ];

    /**
     * The "booting" method of the model.
     * @return void*, orderable: false
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OrderCreatedDescScope());

        static::saving(function (Ad $model) {
            $model->created_at = $model->created_at ?? now();
            $model->ip = $model->ip ?? inet_pton(Request::ip());
        });
    }

    public function getUrlAttribute()
    {
        return route('ad.view.full', ['place' => $this->place->code, 'category' => $this->category->code, 'id' => $this->ad_id]);
    }

    /* Scope a query to only include active users.
    *
    * @param Builder $query
    * @return Builder
    */
    public function scopeActive(Builder $query)
    {
        return $query->where([
            'ads.deleted_at' => null,
            'ads.blocked_at' => null,
        ]);
    }

    public function setAdCategoryIdAttribute($value)
    {
        if (!isset($this->attributes['ad_category_id'])) {
            $this->attributes['ad_category_id'] = $value;
        }
    }

    /**
     * @return User|null|BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /**
     * @return Place|null|BelongsTo
     */
    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id');
    }

    /**
     * @return AdCategory|null|BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(AdCategory::class, 'ad_category_id');
    }

    /**
     * @return Contact[]|null|BelongsToMany
     */
    public function contacts()
    {
        return $this->belongsToMany(Contact::class, 'ads_contacts', 'ad_id', 'contact_id');
    }

    /**
     * @return Photo[]|null|BelongsToMany
     */
    public function photos()
    {
        return $this->morphToMany(Photo::class, 'photable', 'photables', 'photable_id', 'photo_id')->withPivot(['order']);
    }

    /**
     * @return AdVacancy|null|BelongsTo
     */
    public function adVacancy()
    {
        return $this->belongsTo(AdVacancy::class, 'ad_id');
    }

    /**
     * @return AdResume|null|BelongsTo
     */
    public function adResume()
    {
        return $this->belongsTo(AdResume::class, 'ad_id');
    }

    /**
     * @return AdVacancy|AdResume|null
     */
    public function getAdModel()
    {
        $category = AdCategory::find($this->ad_category_id);

        if ($category == null || $category->model == null) {
            return null;
        }

        $adModel = $category->model::find($this->ad_id);
        if ($adModel) {
            return $adModel;
        }

        $adModel = new $category->model();
        $adModel->ad_id = $this->ad_id;
        return $adModel;
    }

    /**
     * @param AdStore $request
     * @return bool
     * @throws Exception
     */
    public function initialSave(AdStore $request): bool
    {

        DB::beginTransaction();

        $this->fill($request->all());


        if (auth()->check()) {
            $user = User::find(auth()->id());
            $this->user()->associate($user);
        } elseif ($request->want_register) {
            $password = Str::random(6);
            $user = new User(['name' => $request->name]);
            $user->setPassword($password);
            $user->save();
            $user->addContactPhone($request->phone);
            $user->addContactEmail($request->email);
        }
        $this->save();

        if ($request->hasFile('photos')) {

            foreach ($request->file('photos') as $file) {
                if ($file->isValid()) {

                    $path = Storage::disk('public')->putFile(date('Y-m-d'), new File($file->path()));
                    $this->photos()->create([
                        'path' => asset('storage/' . $path),
                        'size' => Storage::disk('public')->size($path),
                    ]);
                    // TODO Добавить очередь
                }
            }

        }


        $adModel = $this->getAdModel();
        $adModel->fill($request->all());

        if ($request->has('job_professional_area') && $adModel instanceof HasJobProfessionalArea) {
            $adModel->jobProfessionalAreas()->attach($request->job_professional_area);
        }

        if ($request->has('job_industry') && $adModel instanceof HasJobIndustry) {
            $adModel->jobIndustries()->attach($request->job_industry);
        }

        if ($request->has('driver_licence') && $adModel instanceof HasDriveLicensesAttribute) {
            foreach ($request->driver_licence as $key => $driverLicense) {
                $adModel->driveLicenses()->create(['type_driver' => JobDriver::getDriverKey($key)]);
            }
        }

        if ($request->has('job_skill') && $adModel instanceof HasJobSkills) {
            $jobSkills = array_unique(array_map('trim', explode(",", $request->job_skill)));
            foreach ($jobSkills as $value) {
                if ($jobSkill = JobSkill::whereName($value)->first()) {
                    $adModel->jobSkills()->attach($jobSkill->job_skill_id);
                    unset($jobSkill);
                } else {
                    $adModel->jobSkills()->create(['name' => $value]);
                }
            }
        }

        $adModel->save();

        DB::commit();

        return true;
    }

}
