<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Str;

/**
 * App\Models\JobSkill
 *
 * @property int $job_skill_id
 * @property string $name
 * @property string $code
 * @method static Builder|JobSkill newModelQuery()
 * @method static Builder|JobSkill newQuery()
 * @method static Builder|JobSkill query()
 * @method static Builder|JobSkill whereCode($value)
 * @method static Builder|JobSkill whereJobSkillId($value)
 * @method static Builder|JobSkill whereName($value)
 * @mixin Eloquent
 */
class JobSkill extends Model
{

    /**
     * @var string
     */
    protected $primaryKey = 'job_skill_id';

    public $timestamps = false;

    protected $fillable = ['name'];

    public static function boot()
    {
        parent::boot();

        static::saving(function (JobSkill $model) {
            $model->code = $model->code ?? Str::slug($model->name);
        });
    }
}
