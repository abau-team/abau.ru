<?php

namespace App\Models;

use App\Models\Interfaces\HasLocalName;
use App\Models\Traits\NestedTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\Collection;
use Kalnoy\Nestedset\NodeTrait;
use Str;

/**
 * App\Models\AdCategory
 *
 * @property int $ad_category_id
 * @property string $name
 * @property string $model
 * @property string $code
 * @property integer $active
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property-read string $local_name
 * @method static Builder|AdCategory newModelQuery()
 * @method static Builder|AdCategory newQuery()
 * @method static Builder|AdCategory query()
 * @method static Builder|AdCategory whereName($value)
 * @method static Builder|AdCategory whereCode($value)
 * @method static Builder|AdCategory whereActive($value)
 * @method static Builder|AdCategory whereLft($value)
 * @method static Builder|AdCategory whereModel($value)
 * @method static Builder|AdCategory whereParentId($value)
 * @method static Builder|AdCategory whereRgt($value)
 * @mixin Eloquent
 * @property-read Collection|AdCategory[] $children
 * @property-read AdCategory|null $parent
 * @method static Builder|AdCategory d()
 * @method static Builder|AdCategory whereAdCategoryId($value)
 */
class AdCategory extends Model implements HasLocalName
{
    use NodeTrait, NestedTrait;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $primaryKey = 'ad_category_id';

    /**
     * @return string
     */
    public function getLocalNameAttribute() {
        return __("ad-category.values.{$this->code}");
    }

    protected $fillable = ['name', 'model', 'code', 'active'];

    public static function boot()
    {
        parent::boot();

        static::saving(function (AdCategory $model) {
            $model->code = $model->code ?? Str::slug($model->name);
        });
    }
}
