<?php

namespace App\Models\Interfaces;

interface HasJobProfessionalArea {

    public function jobProfessionalAreas();

}