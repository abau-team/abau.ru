<?php

namespace App\Models\Interfaces;

interface HasJobEducations {

    public function jobEducations();

}