<?php

namespace App\Models\Interfaces;

use App\Models\Ad;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HasAdResume {

    /**
     * @return BelongsTo|Ad|null
     */
    public function resume();

}