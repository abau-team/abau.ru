<?php

namespace App\Models\Interfaces;

use App\Models\Ad;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HasAdVacancy {

    /**
     * @return BelongsTo|Ad|null
     */
    public function vacancy();

}