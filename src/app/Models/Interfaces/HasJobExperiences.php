<?php

namespace App\Models\Interfaces;

interface HasJobExperiences {

    public function jobExperiences();

}