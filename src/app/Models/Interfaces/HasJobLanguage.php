<?php

namespace App\Models\Interfaces;

use App\Models\JobLanguage;
use Illuminate\Database\Eloquent\Relations\HasMany;

interface HasJobLanguage {

    /**
     * @return HasMany|JobLanguage[]|null
     */
    public function jobLanguages();

}