<?php

namespace App\Models\Interfaces;

interface HasJobSkills {

    public function jobSkills();

}