<?php

namespace App\Models\Interfaces;

use App\Models\JobDriver;
use Illuminate\Database\Eloquent\Relations\HasMany;

interface HasDriveLicensesAttribute {

    /**
     * @return HasMany|JobDriver|null
     */
    public function driveLicenses();

}