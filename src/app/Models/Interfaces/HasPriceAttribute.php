<?php

namespace App\Models\Interfaces;

interface HasPriceAttribute {

    public function getPriceAttribute();

}