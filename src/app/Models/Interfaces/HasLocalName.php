<?php

namespace App\Models\Interfaces;

interface HasLocalName {

    public function getLocalNameAttribute();

}