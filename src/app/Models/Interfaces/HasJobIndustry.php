<?php

namespace App\Models\Interfaces;

interface HasJobIndustry {

    public function jobIndustries();

}